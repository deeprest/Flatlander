http://free-loops.com/7346-fire-crackle.html

**SLEEP MECHANIC IS NOT ENJOYABLE**
+ player sleep in bed
+ player sleep on ground
- nonplayer sleep in bed
- sleep with shield: unequip all items? add permanent 'hand' item
- sleep timer on HUD
- switch to sleeping character
- switch from sleeping character
- when player sleeps on ground, followers do too (anyone stand watch?)
- sleep face
- global time
- ground decals
+ mod affinity when witness killing of enemy team member
**low priority**
- trigger to transition to persistent worlds. fade out/in.
- show character relationship when highlighted: parent, child, sibling (do not show cousins etc)
- stamina icon should blink when stamina is low

# TODO

- tutorial world, informational signs explain how to play.
- gossip statements may not be useful. Maybe generic promotion/badmouth icons for conversations?
- unarmed punch attack
- exclamation symbol above head when fleeing.
- char info UI symbols instead text on HUD

* Create companions to protect you from monsters.
* Family tree, global hierarchy of all characters.
* Beehive behavior for bats or a new creature. Use affinity system so bats only attack if they see you harm them or their home. They would only attack you, the individual, and not your team. Newly spawned bats would not inherit negative affinity.
* HUNT. Hunt deer. Hunt with bow. Blood trails. cooking. eating.
* Tame animals by feeding them.
* BUILD. home base pops up like magic carpet.
* WAR. Two forts. Quota spawns. Follow according to Rank. Patrols. Timed incursions.
* DRAGON. kill monsters to increase rank. use rank to get followers. get enough followers to kill the beast.

* Flatcraft (see Flatcraft.md)

# serializedobject id problem:
  1. High Count: every serializedcomponent on every object, every launch increments the global id.
  2. Unused IDs for dead/insignificant: only store certain characters, not bats etc.
  3. How to know when IDs are no longer needed and can be removed; when to remove them.
- symbolic character IDs for generic/plentiful enemies.
- consolidate all existing ids (in all scenes!!!) by remap... when global index reaches a limit?
- ref-count ids and remove from list when zero.
component ids
character ids
event ids

# the verb problem
Get
Pickup
Hold
Grab
Carry
Haul
Transport
Obtain
Acquire
Collect
Possess
Gather
Harvest

inventory items: pickup/acquire, equip
in-world objects: carry/hold, drop

Pickup: move to object and acquire (put in inventory)
Gather: carry objects to a destination


## NOTE
* Replacement name list: the World script has a list of string pairs used to re-assign old object names.
* SerializedOject will rename itself on Awake() to remove "(Clone)" and "(1)" suffixes.

# spawn
- generate new level
- deserialized
- spawn point
- create new character (without spawn point)
- construction
- compound object replace (new level or construction)
- dragndrop in editor
## spawn variants
- limited
- by name, replacement check
- remove colliding objects
* fix the name

Name Generator Issue:
The source names Fabian, Maggie and Margot could potentially generate the name Faggot. Rather than have an incomplete internal blacklist, put the control in the end-user's hands by allowing them to edit an external blacklist. "Simply enter all of the words that you find offensive into the list, and they will be filtered out in the game".
