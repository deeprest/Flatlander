﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Speech
{
  public string tag;
  [TextArea( 2, 5 )]
  public string text;
  public float duration;
  public AudioClip clip
  { 
    get{
      if( clips.Length > 0 )
        return clips[ 0 ];
      else
        return null;
    }
  }
  public AudioClip[] clips;
}
  
/*[System.Serializable]
public class Identity
{
  public string name;
  public bool female;
  public Jabber jabber;
  public List<Speech> speeches;
  public List<Sprite> sprites;
}*/

[System.Serializable]
public class Team
{
  public string name = "None";
  public Tag Tag = Tag.None;
  public Color Color = Color.gray;
  public Material Material;
  public List<Character.Interest> Interests;
}



[System.Serializable]
public class ObjectReplacement
{
  public string oldKey;
  public string newKey;
}

[CreateAssetMenu]
public class GameData : ScriptableObject 
{
  // necessary for characters to spawn their own prefab without Instantiating themselves.
  public List<ObjectReplacement> objectReplacementList;
  public Dictionary<string,string> replacements = new Dictionary<string,string>();
   
  [Header("Character")]
  public GameObject CharacterPrefab;
  public List<Vocation> Vocation;
  public List<Identity> HumanIdentity;
  public List<Identity> AnimalIdentity;
  public List<Team> Team;
  //public MarchingSquare MarchingSquareBuildingSet;
  public List<DecalSet> Decals;

  [Header("Layers")]
  public string[] LowPrioritySensorLayers = new string[] {
    "Deadbody"
  };

  public string[] TopPrioritySensorLayers = new string[] {
    "Character",
    "Notable",
    "Interact",
    "Carry",
    "Weapon"
  };

  public string[] InteractLayers = new string[] {
    "Interact",
    "Notable",
    "Character",
    "Carry",
    "Deadbody"
  };

  public string[] IncludeLayers = new string[] {
    "Default",
    "CameraHide",
    "Character",
    "Carry",
    "Interact",
    "Notable"
  };


  public void Initialize()
  {
    // init object name-replacements
    replacements.Clear();
    foreach( var r in objectReplacementList )
      replacements.Add( r.oldKey, r.newKey );
  }

  public Identity FindIdentity( string name )
  {
    Identity idn = null;
    if( name == "human" )
      idn = HumanIdentity[ Random.Range( 0, HumanIdentity.Count ) ];
    if( name == "animal" )
      idn = AnimalIdentity[ Random.Range( 0, AnimalIdentity.Count ) ];
    if( idn==null )
      idn = HumanIdentity.Find( x => x.name == name );
    if( idn==null )
      idn = AnimalIdentity.Find( x => x.name == name );
    return idn;
  }

  public Team FindTeam( string name )
  {
    return Team.Find( x => x.name == name );
  }

  public Team FindTeam( Tag tag )
  {
    return Team.Find( x => x.Tag == tag );
  }
}



