﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiageticUI : MonoBehaviour, IAction
{
  public GameObject InitiallySelected;
  public Transform CameraLerpTarget;
  public Transform IndicatorTransform;
  public UnityEngine.UI.GraphicRaycaster raycaster;

  public void OnAction( Character instigator, string action = "default" )
  {
    instigator.StartDiageticUI( this );
  }

  public void GetActionContext( ref ActionData actionData )
  {
    if( IndicatorTransform != null )
    {
      actionData.indicator.rotation = Quaternion.LookRotation( IndicatorTransform.forward, IndicatorTransform.up );
      actionData.indicator.position = IndicatorTransform.position;
    }
    
  }
}