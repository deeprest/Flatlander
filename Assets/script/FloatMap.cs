﻿using UnityEngine;


public class FloatMap
{
  public int width;
  public int height;
  float[] oldBuffer;
  public float[] buffer;
  public Color ColorA = Color.black;
  public Color ColorARangeEnd = Color.black;
  public Color ColorB = Color.white;
  public Color ColorBRangeEnd = Color.white;
  Texture2D texture;

  public FloatMap( int w, int h )
  {
    width = w;
    height = h;
  }

  public FloatMap( Texture2D texture ): this( texture.width, texture.height )
  {
    this.texture = texture; 
  }

  public void FillRandom( int seed = 1234 )
  {
    Random.InitState( seed );
    oldBuffer = new float[width * height];
    buffer = new float[width*height];
    for( int x = 0; x < width; x++ )
    {
      for( int y = 0; y < height; y++ )
      {
        buffer[ x + width * y ] = Random.value;
      }
    }
  }

  public void FillPerlin( Vector2 origin, float scale )
  {
    oldBuffer = new float[width * height];
    buffer = new float[width*height];
    for( int x = 0; x < width; x++ )
    {
      for( int y = 0; y < height; y++ )
      {
        buffer[ x+width*y ] = Mathf.PerlinNoise( origin.x + (float)x / ( (float)width ) * scale, origin.y + (float)y / ( (float)height ) * scale );
      }
    }
  }

  public void Conway( int iterations )
  {
    int[] offset = new int[8] {
      -1,
      1,
      width - 1,
      width,
      width + 1,
      -width - 1,
      -width,
      -width + 1
    }; 
    for( int iteration = 0; iteration < iterations; iteration++ )
    {
      buffer.CopyTo( oldBuffer, 0 );
      for( int x = 0; x < width; x++ )
      {
        for( int y = 0; y < height; y++ )
        {
          if( x == 0 || y == 0 || x == width - 1 || y == height - 1 )
            continue;
          int index = x + width * y;
          int n = 0;
          for( int i = 0; i < 8; i++ )
          {
            if( oldBuffer[ x + y * width + offset[ i ] ] > 0.5f )
              n++;
          }
          if( oldBuffer[ index ] > 0.5f )
          {
            if( n < 2 || n > 3 )
              buffer[ index ] = 0f;
          }
          else
          {
            if( n == 3 )
              buffer[ index ] = 1f;
          }
        }
      }
    }
    Render();
  }
    

  public void Render(float floor=0f, float ceil=2f)
  {
    if( texture != null )
    {
      float value;
      for( int x = 0; x < width; x++ )
      {
        for( int y = 0; y < height; y++ )
        {
          value = buffer[ x + width * y ];
          if( value >= floor && value <= ceil )
          {
            float alpha = ( value - floor ) / ( ceil - floor );
            texture.SetPixel( x, y, Color.Lerp( ColorA, ColorARangeEnd, alpha ) );
          }
        }
      }
      texture.Apply();
    }
  }

}