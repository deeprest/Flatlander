﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using LitJson;

public static class PixelBit
{
  public const byte None = 0;
  public const byte Door = 32;
  public const byte Wall = 64;
  public const byte Building = 128;
}

[RequireComponent( typeof( MeshRenderer ) )]
public class Zone : MonoBehaviour
{

  string DataFilePath { get { return Application.persistentDataPath + "/" + name + "/" + "scene.json"; } }

  string DataFileDirectory { get { return Application.persistentDataPath + "/" + name; } }

  public static int BlockSize = 128;
  public static int GroundTextureSize = 1024;
  const string MainTextureName = "_MainTex";

  public Texture2D GroundDiffuse;

  string groundDiffusePath { get { return Application.persistentDataPath + "/" + name + "/" + "ground.png"; } }

  Texture2D diffuseTexture;
  const int diffuseIndex = 0;

  public Texture2D GroundOverlay;

  string groundOverlayPath { get { return Application.persistentDataPath + "/" + name + "/" + "ground-dirt.png"; } }

  Texture2D overlayTexture;
  const int overlayIndex = 1;
  bool overlayTextureDirtyFlag = false;

  public Color burnColor;
  public float BurnAlphaThreshold = 0.3f;

  public Vector3 basecampSafeHalfExtents = Vector3.one;

  GameObject BoxCollidersObject;





  UnityEngine.AI.NavMeshSurface NavSurface;

  Vector3 CharacterLookAngles;
  public bool IsSnowLevel;


  public void Initialize( string zoneName )
  {
    name = zoneName;
    Global.Instance.AddZone( this );

    BuildNavMesh();
    CreateBoundaryColliders();
    InitializeImages();
    InitializeStructure();
  }

  void Update()
  {
    if( overlayTextureDirtyFlag )
    {
      overlayTextureDirtyFlag = false;
      overlayTexture.Apply();
    }

    foreach( var p in GenerationParams.points )
    {
      Debug.DrawLine( p.point, p.point + Vector3.up * 10, p.def.lineColor );
    }
  }

  void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    Global.Instance.RemoveZone( this );

    MeshRenderer mr = GetComponent<MeshRenderer>();
    if( Application.isEditor )
    {
      if( mr != null )
      {
        Material.DestroyImmediate( mr.materials[diffuseIndex] );
        Material.DestroyImmediate( mr.materials[overlayIndex] );
      }
    }
    else
    {
      if( mr != null )
      {
        Material.Destroy( mr.materials[diffuseIndex] );
        Material.Destroy( mr.materials[overlayIndex] );
      }
    }
  }

  public void BuildNavMesh()
  {
    NavSurface = GetComponent<NavMeshSurface>();
    NavSurface.BuildNavMesh();
  }

  public void CreateBoundaryColliders()
  {
    if( BoxCollidersObject != null )
    {
      if( Application.isEditor )
        GameObject.DestroyImmediate( BoxCollidersObject );
      else
        GameObject.Destroy( BoxCollidersObject );
    }
    const float BoxColliderSize = 0.5f;
    BoxCollidersObject = new GameObject( "BoxCollidersObject" );
    BoxCollidersObject.transform.parent = transform;
    BoxCollider south = BoxCollidersObject.AddComponent<BoxCollider>();
    south.center = new Vector3( (BlockSize) * 0.5f, 0.5f, -BoxColliderSize * 0.5f );
    south.size = new Vector3( (BlockSize) + BoxColliderSize * 2f, 1, BoxColliderSize );
    BoxCollider north = BoxCollidersObject.AddComponent<BoxCollider>();
    north.center = new Vector3( (BlockSize) * 0.5f, 0.5f, (BlockSize) + BoxColliderSize * 0.5f );
    north.size = new Vector3( (BlockSize) + BoxColliderSize * 2f, 1, BoxColliderSize );
    BoxCollider west = BoxCollidersObject.AddComponent<BoxCollider>();
    west.center = new Vector3( -BoxColliderSize * 0.5f, 0.5f, (BlockSize) * 0.5f );
    west.size = new Vector3( BoxColliderSize, 1, (BlockSize) + BoxColliderSize * 2f );
    BoxCollider east = BoxCollidersObject.AddComponent<BoxCollider>();
    east.center = new Vector3( (BlockSize) + BoxColliderSize * 0.5f, 0.5f, (BlockSize) * 0.5f );
    east.size = new Vector3( BoxColliderSize, 1, (BlockSize) + BoxColliderSize * 2f );
  }

  #region Structure

  string StructurePath { get { return Application.persistentDataPath + "/" + name + "/" + "structure.png"; } }
  Texture2D StructureTexture;
  Dictionary<int, List<GameObject>> built = new Dictionary<int, List<GameObject>>();
  Color32[] structureData;

  public void InitializeStructure()
  {
    msd = new List<MarchingSquareData>();
    msd.Add( building );
    msd.Add( wall );
    //foreach( var md in msd )
    //md.ms.Awake();
    StructureTexture = new Texture2D( BlockSize, BlockSize );
    structureData = new Color32[BlockSize * BlockSize];
  }

  public void DeserializeStructure()
  {
    if( File.Exists( StructurePath ) )
    {
      byte[] bytes = File.ReadAllBytes( StructurePath );
      if( bytes.Length > 0 )
      {
        StructureTexture.filterMode = FilterMode.Point;
        StructureTexture.LoadImage( bytes );
        structureData = StructureTexture.GetPixels32();
      }
    }
    else
    {
      Color32 black = new Color32( 0, 0, 0, 255 );
      for( int i = 0; i < BlockSize * BlockSize; i++ )
        structureData[i] = black;
    }
    UpdateStructure( 0, 0, BlockSize, BlockSize );
  }

  public void SetStructureData( int x, int y, byte value )
  {
    x = System.Math.Max( 0, System.Math.Min( x, BlockSize - 1 ) );
    y = System.Math.Max( 0, System.Math.Min( y, BlockSize - 1 ) );
    structureData[x + y * BlockSize].r = value;
  }

  public void SetStructureBitOn( int x, int y, byte value )
  {
    x = System.Math.Max( 0, System.Math.Min( x, BlockSize - 1 ) );
    y = System.Math.Max( 0, System.Math.Min( y, BlockSize - 1 ) );
    structureData[x + y * BlockSize].r |= value;
  }


  [System.Serializable]
  public class MarchingSquareData
  {
    public byte bit = 0;
    public MarchingSquare ms;
    // transient
    public int[,] indexBuffer = new int[BlockSize, BlockSize];
  }

  public MarchingSquareData building;
  public MarchingSquareData wall;
  public MarchingSquareData door;

  List<MarchingSquareData> msd;


  public void UpdateStructure( int ox, int oy, int w, int h )
  {
    ox = Mathf.Clamp( ox, 0, BlockSize );
    oy = Mathf.Clamp( oy, 0, BlockSize );
    if( ox > 0 )
    {
      ox -= 1;
      w += 1;
    }
    if( oy > 0 )
    {
      oy -= 1;
      h += 1;
    }
    // first pass
    for( int y = oy; y < oy + h && y < BlockSize - 1; y++ )
    {
      for( int x = ox; x < ox + w && x < BlockSize - 1; x++ )
      {
        int key = x + y * BlockSize;
        if( built.ContainsKey( key ) && built[key] != null )
        {
          for( int i = 0; i < built[key].Count; i++ )
            GameObject.Destroy( built[key][i] );
          built[key].Clear();
        }
        foreach( var ms in msd )
        {
          // getpixels returns: 
          // 23
          // 01
          // marching squares bit values: 
          // 24
          // 18
          int index = 0;
          if( (structureData[key].r & ms.bit) > 0 )
            index += 1;
          if( (structureData[(x + 1) + y * BlockSize].r & ms.bit) > 0 )
            index += 8;
          if( (structureData[x + (y + 1) * BlockSize].r & ms.bit) > 0 )
            index += 2;
          if( (structureData[(x + 1) + (y + 1) * BlockSize].r & ms.bit) > 0 )
            index += 4;
          ms.indexBuffer[x, y] = index;
        }
      }
    }

    // second pass: neighbors
    /* foreach( var ms in msd )
    {
      for( int y = 1; y < BlockSize - 1; y++ )
      {
        for( int x = 1; x < BlockSize - 1; x++ )
        {
          //int index = x + y * BlockSize;
          int[] n = new int[9];
          n[ 0 ] = ms.indexBuffer[ x - 1, y - 1 ];
          n[ 1 ] = ms.indexBuffer[ x , y - 1 ];
          n[ 2 ] = ms.indexBuffer[ x + 1, y - 1 ];
          n[ 3 ] = ms.indexBuffer[ x - 1, y ];
          n[ 4 ] = ms.indexBuffer[ x, y ];
          n[ 5 ] = ms.indexBuffer[ x + 1, y ];
          n[ 6 ] = ms.indexBuffer[ x - 1, y + 1 ];
          n[ 7 ] = ms.indexBuffer[ x, y + 1 ];
          n[ 8 ] = ms.indexBuffer[ x + 1, y + 1 ];
            
        }
      }
    }*/


    for( int y = oy; y < oy + h && y < BlockSize - 1; y++ )
    {
      for( int x = ox; x < ox + w && x < BlockSize - 1; x++ )
      {
        int key = x + y * BlockSize;

        MarchingSquareCell cell = building.ms.Cells[building.indexBuffer[x, y]];
        if( cell != null )
        {
          int doorIndex = 0;
          if( (structureData[key].r & PixelBit.Door) > 0 )
            doorIndex += 1;
          if( (structureData[(x + 1) + y * BlockSize].r & PixelBit.Door) > 0 )
            doorIndex += 8;
          if( (structureData[x + (y + 1) * BlockSize].r & PixelBit.Door) > 0 )
            doorIndex += 2;
          if( (structureData[(x + 1) + (y + 1) * BlockSize].r & PixelBit.Door) > 0 )
            doorIndex += 4;

          if( doorIndex > 0 && cell.door != null )
            SingleCell( key, x, y, cell.door, cell.top );
          else
            SingleCell( key, x, y, cell.bottom, cell.top );
        }

        cell = wall.ms.Cells[wall.indexBuffer[x, y]];
        if( cell != null )
          SingleCell( key, x, y, cell.bottom, cell.top );


        //        foreach( var ms in msd )
        //        {
        //          int buildingIndex = ms.indexBuffer[ x, y ];
        //          SingleCell( key, x, y, ms.ms.Cells[ buildingIndex ].bottom, ms.ms.Cells[ buildingIndex ].top );
        //        }
      }
    }
  }

  void SingleCell( int key, int x, int y, GameObject bottom, GameObject top )
  {
    //        if( cell.bottom.Length > 0 )
    //          prefab = cell.bottom[ Random.Range( 0, cell.bottom.Length - 1 ) ];
    if( bottom != null )
    {
      Vector3 pos = new Vector3( x + 1.0f, 0, y + 1.0f );
      GameObject go = GameObject.Instantiate( bottom, pos, Quaternion.identity, null );
      if( !built.ContainsKey( key ) )
        built[key] = new List<GameObject>();
      built[key].Add( go );
      //            if( cell.top.Length > 0 )
      //              topPrefab = cell.top[ Random.Range( 0, cell.top.Length - 1 ) ];
      if( top != null )
      {
        built[key].Add( GameObject.Instantiate( top, pos, Quaternion.identity, null ) );
      }

    }
  }

  #endregion

  #region Images

  public void InitializeImages()
  {
    MeshRenderer mr = GetComponent<MeshRenderer>();
    diffuseTexture = (Texture2D)Object.Instantiate( GroundDiffuse );
    mr.materials[diffuseIndex].SetTexture( MainTextureName, diffuseTexture );
    overlayTexture = (Texture2D)Object.Instantiate( GroundOverlay );
    mr.materials[overlayIndex].SetTexture( MainTextureName, overlayTexture );
  }

  public void DeserializeImages()
  {
    if( !Application.isPlaying )
      return;

    if( File.Exists( groundDiffusePath ) )
    {
      byte[] bytes = File.ReadAllBytes( groundDiffusePath );
      if( bytes.Length > 0 )
      {
        diffuseTexture.filterMode = FilterMode.Point;
        diffuseTexture.LoadImage( bytes );
      }
    }

    if( File.Exists( groundOverlayPath ) )
    {
      byte[] bytes = File.ReadAllBytes( groundOverlayPath );
      if( bytes.Length > 0 )
      {
        overlayTexture.filterMode = FilterMode.Point;
        overlayTexture.LoadImage( bytes );
      }
    }
  }

  public void ClearGroundDirtImage()
  {
    File.Delete( groundOverlayPath );
    ClearGround();
    //mr.materials[dirtIndex].SetTexture( MainTextureName, (Texture2D)Object.Instantiate( mr.sharedMaterials[dirtIndex].GetTexture(MainTextureName) ) );
  }

  void ClearGround()
  {
    Color clear = new Color( 0, 0, 0, 0 );
    Texture dirtTex = overlayTexture;
    if( dirtTex != null )
    {
      Texture2D tex = (Texture2D)dirtTex;
      for( int x = 0; x < GroundTextureSize; x++ )
      {
        for( int y = 0; y < GroundTextureSize; y++ )
        {
          tex.SetPixel( x, y, clear );
        }
      }
      tex.Apply();
    }
  }

  public void PaintGround( Color color, float amount )
  {
    Texture dirtTex = overlayTexture;
    if( dirtTex != null )
    {
      Texture2D tex = (Texture2D)dirtTex;
      for( int x = 0; x < GroundTextureSize; x++ )
      {
        for( int y = 0; y < GroundTextureSize; y++ )
        {
          tex.SetPixel( x, y, Color.Lerp( tex.GetPixel( x, y ), color, amount ) );
        }
      }
      tex.Apply();
    }
  }


  public void BurnTheDirt()
  {
    for( int x = 0; x < GroundTextureSize; x++ )
    {
      for( int y = 0; y < GroundTextureSize; y++ )
      {
        Color overlayColor = overlayTexture.GetPixel( x, y );
        if( overlayColor.a > BurnAlphaThreshold )
        {
          diffuseTexture.SetPixel( x, y, burnColor );
          Color c = overlayColor;
          c.a = BurnAlphaThreshold;
          overlayTexture.SetPixel( x, y, c );
        }
      }
    }
    diffuseTexture.Apply();
    overlayTexture.Apply();
  }

  public void PaintDecalTile( Vector3 position, Sprite diffuseDecal )
  {
    /*
    Color clear = new Color( 0, 0, 0, 0 );
    const float fudge = 0.001f;
    RaycastHit hitInfo = new RaycastHit();
    if( Physics.Raycast( position, Vector3.down, out hitInfo, 2f, LayerMask.GetMask(new string[]{"Ground"}) ) )
    {
      MeshRenderer mr = hitInfo.transform.GetComponent<MeshRenderer>();
      Texture diffuse = mr.material.GetTexture("_MainTex");
      Texture2D diffuseTex = (Texture2D)diffuse;
      Color[] diffuseColors = diffuseDecal.texture.GetPixels();
      int cx = (int)( (hitInfo.textureCoord.x+fudge) * (float)diffuseTex.width );
      int cy = (int)( (hitInfo.textureCoord.y+fudge) * (float)diffuseTex.height );
      int decalSize = GroundTextureSize / Zone.BlockSize;
      int half = decalSize/2;
      for( int x = 0; x < decalSize; x++ )
      {
        for( int y = 0; y < decalSize; y++ )
        {
          Color diff = diffuseColors[ (int)(diffuseDecal.rect.x)+x + ((int)(diffuseDecal.rect.y)+y)*diffuseDecal.texture.width ];
          //diff.a = 1f;
          diffuseTex.SetPixel( cx-half+x, cy-half+y, diff );
          overlayTexture.SetPixel( cx - half + x, cy - half + y, clear );
        }
      }
      diffuseTex.Apply();
      overlayTexture.Apply();
    }
    */

    RaycastHit hitInfo = new RaycastHit();
    if( Physics.Raycast( position, Vector3.down, out hitInfo, 2f, LayerMask.GetMask( new string[] { "Ground" } ) ) )
    {
      //MeshRenderer mr = hitInfo.transform.GetComponent<MeshRenderer>();
      int pixelsPerUnit = GroundTextureSize / Zone.BlockSize;
      Vector2Int pos = new Vector2Int( Mathf.FloorToInt( position.x ), Mathf.FloorToInt( position.z ) );

      Color[] decalColors = diffuseDecal.texture.GetPixels( (int)diffuseDecal.rect.x, (int)diffuseDecal.rect.y, (int)diffuseDecal.rect.width, (int)diffuseDecal.rect.height );
      //Texture2D diffuseTex = (Texture2D)mr.material.GetTexture( "_MainTex" );
      diffuseTexture.SetPixels( pos.x * pixelsPerUnit, pos.y * pixelsPerUnit, pixelsPerUnit, pixelsPerUnit, decalColors );
      diffuseTexture.Apply();

      Color[] clearDecal = new Color[pixelsPerUnit * pixelsPerUnit];
      for( int i = 0; i < pixelsPerUnit * pixelsPerUnit; i++ )
        clearDecal[i] = Color.clear;
      overlayTexture.SetPixels( pos.x * pixelsPerUnit, pos.y * pixelsPerUnit, pixelsPerUnit, pixelsPerUnit, clearDecal );
      overlayTexture.Apply();
    }
  }

  // TODO finish this sometime
  /*public void PaintDecal( Vector3 position, Sprite diffuseDecal )
  {
    Color clear = new Color( 0, 0, 0, 0 );
    const float fudge = 0.001f;
    RaycastHit hitInfo = new RaycastHit();
    if( Physics.Raycast( position, Vector3.down, out hitInfo, 2f, LayerMask.GetMask(new string[]{"Ground"}) ) )
    {
      MeshRenderer mr = hitInfo.transform.GetComponent<MeshRenderer>();
      Texture diffuse = mr.material.GetTexture("_MainTex");
      Texture2D diffuseTex = (Texture2D)diffuse;
      Color[] diffuseColors = diffuseDecal.texture.GetPixels();
      int cx = (int)( (hitInfo.textureCoord.x+fudge) * (float)diffuseTex.width );
      int cy = (int)( (hitInfo.textureCoord.y+fudge) * (float)diffuseTex.height );
      //      int decalSize = Block.GroundTextureSize / Zone.BlockSize;
      int decalSize = (int)( diffuseDecal.pixelsPerUnit * diffuseDecal.bounds.size.x );
      int half = decalSize/2;
      for( int x = 0; x < decalSize; x++ )
      {
        for( int y = 0; y < decalSize; y++ )
        {
          Color diff = diffuseColors[ (int)(diffuseDecal.rect.x)+x + ((int)(diffuseDecal.rect.y)+y)*diffuseDecal.texture.width ];
          //diff.a = 1f;
          diffuseTex.SetPixel( cx-half+x, cy-half+y, diff );
          dirtTexture.SetPixel( cx - half + x, cy - half + y, clear );
        }
      }
      diffuseTex.Apply();
      dirtTexture.Apply();
    }
  }*/

  public enum PaintType
  {
    Blend,
    Overwrite
  }

  public void PaintSingleGroundPixel( Vector3 position, Color color, PaintType paintType = PaintType.Blend )
  {
    RaycastHit hitInfo = new RaycastHit();
    if( Physics.Raycast( position, Vector3.down, out hitInfo, 5f, LayerMask.GetMask( new string[] { "Ground" } ) ) )
    {
      Vector2 pixelCoord = new Vector2( hitInfo.textureCoord.x * overlayTexture.width, hitInfo.textureCoord.y * overlayTexture.height );
      Color dst = overlayTexture.GetPixel( (int)pixelCoord.x, (int)pixelCoord.y );
      Color pixelColor = color;
      if( paintType == PaintType.Blend )
        pixelColor = Color.Lerp( dst, color, color.a );
      overlayTexture.SetPixel( (int)pixelCoord.x, (int)pixelCoord.y, pixelColor );
      overlayTextureDirtyFlag = true;

    }
  }
  /*
  public void PaintSingleHeightPixel( Vector3 position, float value )
  {
    RaycastHit hitInfo = new RaycastHit();
    if( Physics.Raycast( position, Vector3.down, out hitInfo, 5f, LayerMask.GetMask( new string[]{"Ground"} ) ) )
    {
      Vector2 pixelCoord = new Vector2( hitInfo.textureCoord.x * heightTexture.width, hitInfo.textureCoord.y * heightTexture.height );  
//      Color dst = heightTexture.GetPixel( (int)pixelCoord.x, (int)pixelCoord.y );

//      heightTexture.SetPixel( (int)pixelCoord.x-1, (int)pixelCoord.y, new Color(0.8f,0.5f,1,1) );
      heightTexture.SetPixel( (int)pixelCoord.x, (int)pixelCoord.y, new Color(value,value,value) );
//      heightTexture.SetPixel( (int)pixelCoord.x+1, (int)pixelCoord.y, new Color(-0.8f,0.5f,1,1) );

//      heightTexture.SetPixel( (int)pixelCoord.x-1, (int)pixelCoord.y+1, new Color(0.8f,0.8f,1,1) );
//      heightTexture.SetPixel( (int)pixelCoord.x, (int)pixelCoord.y+1, new Color(0.5f,0.8f,1,1) );
//      heightTexture.SetPixel( (int)pixelCoord.x+1, (int)pixelCoord.y+1, new Color(-0.8f,0.8f,1,1) );
//
//      heightTexture.SetPixel( (int)pixelCoord.x-1, (int)pixelCoord.y-1, new Color(0.8f,-0.8f,1,1) );
//      heightTexture.SetPixel( (int)pixelCoord.x, (int)pixelCoord.y-1, new Color(0.5f,-0.8f,1,1) );
//      heightTexture.SetPixel( (int)pixelCoord.x+1, (int)pixelCoord.y-1, new Color(-0.8f,-0.8f,1,1) );

      Texture2D overlayHM = (Texture2D)mr.materials[ overlayIndex ].GetTexture( HeightTextureName );
      overlayHM.SetPixel( (int)pixelCoord.x, (int)pixelCoord.y, new Color(value,value,value,1) );
      overlayHM.Apply();
    }
    heightT
    exture.Apply();
  }
*/

  #endregion

  #region Generation

  [Header( "Generation" )]


  public GeneratedLayerParameters GenerationParams;

  public void Generate()
  {
    Generate( Random.Range( 0, 256 ) );
  }

  public void Generate( int seed )
  {
    Vector2 perlinOrigin = Vector2.one * 64f + Random.insideUnitCircle * 64f;
    GenerationParams.points.Clear();
    foreach( var p in GenerationParams.Layers )
      GenerateLayer( perlinOrigin, GenerationParams.PerlinScale, p.perlinLow, p.perlinHigh, p.gradient, p.obj, p.randomOffset );

    float minDistance = 10f;
    // filter points of interest
    List<PointOfInterest> good = new List<PointOfInterest>();
    foreach( var a in GenerationParams.points )
    {
      bool okay = true;
      if( Mathf.Abs( (float)Zone.BlockSize - a.point.x ) < minDistance || Mathf.Abs( -a.point.x ) < minDistance ||
          Mathf.Abs( (float)Zone.BlockSize - a.point.z ) < minDistance || Mathf.Abs( -a.point.z ) < minDistance )
      {
        okay = false;
      }
      else
      {
        foreach( var b in good )
        {
          if( a == b )
            continue;
          if( Vector3.Distance( a.point, b.point ) < minDistance )
          {
            okay = false;
            break;
          }
        }
      }
      if( okay )
        good.Add( a );
    }
    GenerationParams.points.Clear();
    GenerationParams.points = good;

    Vector3 pointBlue;
    Vector3 pointRed;
    if( GenerationParams.points.Count < 2 )
    {
      pointBlue = new Vector3( minDistance, 0, minDistance );
      pointRed = new Vector3( (float)Zone.BlockSize - minDistance, 0, (float)Zone.BlockSize - minDistance );
    }
    else
    {
      pointBlue = GenerationParams.points[0].point;
      GenerationParams.points.RemoveAt( 0 );
      pointRed = GenerationParams.points[GenerationParams.points.Count - 1].point;
      GenerationParams.points.RemoveAt( GenerationParams.points.Count - 1 );
    }

    if( GenerationParams.blueBaseCamp != null )
      BuildBasecamp( GenerationParams.blueBaseCamp, Tag.Blue, pointBlue, Quaternion.identity );
    if( GenerationParams.redBaseCamp != null )
      BuildBasecamp( GenerationParams.redBaseCamp, Tag.Red, pointRed, Quaternion.identity );

    // spawn objects of interest
    foreach( var p in GenerationParams.points )
    {
      if( p.def.obj != null )
        Global.Instance.Spawn( p.def.obj, p.point, Quaternion.identity );
    }
  }

  void BuildBasecamp( GameObject prefab, Tag team, Vector3 position, Quaternion rotation )
  {
    int layerMask = ~LayerMask.GetMask( new string[] { "Ground" } );

    /*Bounds bounds = new Bounds( Vector3.zero, Vector3.one );
      Collider[] mrs = blueBaseCamp.GetComponentsInChildren<Collider>();
      Vector3 prefabOffset = blueBaseCamp.transform.position;
      foreach( Collider mr in mrs )
      {
        Bounds b = new Bounds( mr.bounds.center - prefabOffset, mr.bounds.size );
        bounds.Encapsulate( b );
      }
      */

    Collider[] cls = Physics.OverlapBox( position, basecampSafeHalfExtents, rotation, layerMask, QueryTriggerInteraction.Collide );
    foreach( var cld in cls )
    {
      if( Application.isEditor && !Application.isPlaying )
        GameObject.DestroyImmediate( cld.gameObject );
      else
        GameObject.Destroy( cld.gameObject );
    }
    GameObject teambase = Global.Instance.Spawn( prefab, position, rotation, null, false, true );
    Global.Instance.AssignTeam( teambase, team );

    // DEBUG
    /*GameObject box = new GameObject( "basecamp debug box" );
      box.transform.position = position;
      BoxCollider bc = box.AddComponent<BoxCollider>();
      bc.extents = basecampSafeHalfExtents;
      */

    {
      GameObject go = Global.Instance.Spawn( Global.Instance.avatarPrefab, position, rotation, null, false, false );
      Character male = go.GetComponent<Character>();
      male.IdentityName = "stan";
      SerializedComponent[] scs = go.GetComponentsInChildren<SerializedComponent>();
      foreach( var sc in scs )
        sc.AfterDeserialize();
      Global.Instance.AssignTeam( go, team );
    }
    {
      GameObject go = Global.Instance.Spawn( Global.Instance.avatarPrefab, position, rotation, null, false, false );
      Character female = go.GetComponent<Character>();
      female.IdentityName = "francine";
      SerializedComponent[] scs = go.GetComponentsInChildren<SerializedComponent>();
      foreach( var sc in scs )
        sc.AfterDeserialize();
      Global.Instance.AssignTeam( go, team );
    }
  }

  public void GenerateLayer( Vector2 perlinOrigin, float perlinScale, float perlinLow, float perlinHigh,
                             Gradient gradient, GameObject[] obj, bool randomOffset = false )
  {
    // ground color
    FloatMap cwy = new FloatMap( diffuseTexture );
    cwy.ColorA = gradient.Evaluate( 0 );
    cwy.ColorARangeEnd = gradient.Evaluate( 1 );
    cwy.FillPerlin( perlinOrigin, perlinScale );
    cwy.Render( perlinLow, perlinHigh );

    //    if( obj.Length > 0 )
    {
      // spawn objects
      FloatMap cwy2 = new FloatMap( Zone.BlockSize, Zone.BlockSize );
      cwy2.FillPerlin( perlinOrigin, perlinScale );
      float rot = 0;
      float value = 0;
      for( int x = 0; x < cwy2.width; x++ )
      {
        for( int y = 0; y < cwy2.height; y++ )
        {
          value = cwy2.buffer[x + cwy2.width * y];
          if( value > perlinLow && value < perlinHigh )
          {
            Vector3 pos = new Vector3( 0.5f + x, 0, 0.5f + y );
            if( Global.Instance.SpawnCheck( pos, 0.4f ) )
            {
              if( randomOffset )
              {
                Vector2 offset = Random.insideUnitCircle * 0.5f;
                pos += new Vector3( offset.x, 0, offset.y );
              }
              if( obj.Length > 0 )
              {
                GameObject prefab = obj[Random.Range( 0, obj.Length )];
                if( prefab == null )
                {
                  continue;
                }
                GameObject go = Global.Instance.Spawn( prefab, pos, Quaternion.Euler( 0, rot, 0 ), null, false, true );
                go.transform.localScale = new Vector3( 1, 0.5f + Random.value, 1 );
                rot += 30f;
              }
            }
          }
          foreach( var pp in GenerationParams.PerlinPoints )
          {
            if( value > pp.threshold - pp.range * 0.5f && value < pp.threshold + pp.range * 0.5f )
            {
              PointOfInterest p = new PointOfInterest();
              p.def = pp;
              p.point = new Vector3( 0.5f + x, 0, 0.5f + y );
              GenerationParams.points.Add( p );
            }
          }
        }
      }
    }
  }

  #endregion


  public IEnumerator Construct()
  {
    if( !Directory.Exists( Application.persistentDataPath + "/" + name ) )
    {
      // unzip level from assets
      string zipPath = Application.temporaryCachePath + "/" + name + ".zip";
      TextAsset zipfile = Resources.Load( "zone/" + name ) as TextAsset;
      if( zipfile != null )
      {
        File.WriteAllBytes( zipPath, zipfile.bytes );
        Debug.Log( "Unzipping level: " + zipPath );
        ZipUtil.Unzip( zipPath, Application.persistentDataPath + "/" + name );
      }
      else
      {
        Debug.LogWarning( "no level directory or zip file in build: " + name );
      }
    }

    string levelFilePath = Application.persistentDataPath + "/" + name + "/" + "scene.json";
    if( File.Exists( levelFilePath ) )
    {
      string json = File.ReadAllText( levelFilePath );
      if( json.Length == 0 )
      {
        Debug.LogError( "level file empty: " + name );
        yield break;
      }

      JsonReader reader = new JsonReader( json );
      JsonData data = JsonMapper.ToObject( reader );
      if( data.Keys.Contains( "level" ) )
      {
        JsonData levelData = data["level"];
        if( levelData.Keys.Contains( "snow" ) )
          IsSnowLevel = levelData["snow"].GetBoolean();
        if( levelData.Keys.Contains( "camera" ) )
        {
          JsonData cam = levelData["camera"];
          CharacterLookAngles = JsonUtil.ReadVector( Vector3.zero, cam, "CharacterLookAngles" );
          Global.Instance.cameraController.lookInput = CharacterLookAngles;
          //          cameraController.FreePosition = JsonUtil.ReadVector( Vector3.zero, cam, "freepos" );
          //          cameraController.FreeLookAngles = JsonUtil.ReadVector( Vector3.zero, cam, "FreeLookAngles" );
        }
      }

      DeserializeImages();
      DeserializeStructure();

      JsonData obs = data["objects"];
      int total = obs.Count;
      int i = 0;
      float mark = Time.realtimeSinceStartup;
      foreach( JsonData c in obs )
      {
        SerializedObject.Deserialize( c );
        if( Time.realtimeSinceStartup - mark > Global.Instance.ProgressUpdateInterval )
        {
          mark = Time.unscaledTime;
          Global.Instance.ProgressBar.value = (float)(i++) / (float)total;
          yield return null;
        }
      }

      // serialized objects may be created in this step, so create a copy of the list to iterate over.
      SerializedObject[] sobs = new SerializedObject[SerializedObject.serializedObjects.Values.Count];
      SerializedObject.serializedObjects.Values.CopyTo( sobs, 0 );
      foreach( var so in sobs )
        so.AfterDeserialize();

    }

    //    if( IsSnowLevel )
    //    {
    //      PaintGround( Color.white, 0.4f );
    //    }
    //    else
    //    {
    BurnTheDirt();
    PaintGround( Color.clear, 0.4f );
    //    }

#if ENDLESS_WORLD
    cameraController.SetOffsets( WorldOffset );
#endif

    yield return null;
  }

  public void Destruct()
  {
    /*Global.Instance.StartCoroutine( SceneManager.UnloadSceneAsync( name ) ); 

    AsyncOperation aso = SceneManager.UnloadSceneAsync( name );
    //Resources.UnloadUnusedAssets();

    Timer UnloadTimer = new Timer();
    UnloadTimer.Start( 10f, delegate(Timer obj )
    {
      Debug.Log( "waiting for " + name + " to unload. " + obj.ProgressSeconds );
      if( aso.isDone )
      {
        Debug.Log("unload timer done");
        obj.Stop( false );
      }
    }, null );
    */

    /*if( Application.isEditor )
    {
      #if BOX_COLLIDERS
      if( BoxCollidersObject != null )
        GameObject.DestroyImmediate( BoxCollidersObject );
      #endif
      GameObject[] gos = gameObject.scene.GetRootGameObjects();
      for( int i = 0; i < gos.Length; i++ )
        if( gos[ i ] != gameObject )
          GameObject.DestroyImmediate( gos[ i ] );
    }
    else
    {
      #if BOX_COLLIDERS
      if( BoxCollidersObject != null )
        GameObject.Destroy( BoxCollidersObject );
      #endif
      GameObject[] gos = gameObject.scene.GetRootGameObjects();
      for( int i = 0; i < gos.Length; i++ )
        if( gos[ i ] != gameObject )
          GameObject.Destroy( gos[ i ] );
    }*/
  }

  public void Serialize()
  {
    if( File.Exists( DataFilePath ) )
      File.Copy( DataFilePath, DataFilePath + "-backup", true );

    //GameObject[] gos = gameObject.scene.GetRootGameObjects();
    List<SerializedObject> sos = new List<SerializedObject>();
    foreach( var so in SerializedObject.serializedObjects )
    {
      //if( so ==null )
      // write to dead list
      if( so.Value != null && so.Value.gameObject.scene == gameObject.scene )
        sos.Add( so.Value );
    }

    foreach( var so in sos )
    {
      so.BeforeSerialize();
    }

    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;

    writer.WriteObjectStart();

    writer.WritePropertyName( "level" );
    writer.WriteObjectStart();
    writer.WritePropertyName( "snow" );
    writer.Write( IsSnowLevel );
    writer.WritePropertyName( "camera" );
    writer.WriteObjectStart();
    // globals. this is ugly.
    writer.WritePropertyName( "freepos" );
    JsonUtil.WriteVector( writer, Global.Instance.cameraController.FreePosition );
    writer.WritePropertyName( "FreeLookAngles" );
    JsonUtil.WriteVector( writer, Global.Instance.cameraController.FreeLookAngles );
    writer.WritePropertyName( "CharacterLookAngles" );
    JsonUtil.WriteVector( writer, Global.Instance.cameraController.lookInput );

    writer.WriteObjectEnd();
    writer.WriteObjectEnd();

    writer.WritePropertyName( "objects" );
    writer.WriteArrayStart();
    foreach( var so in sos )
    {
      if( so.serialize )
      {
        writer.WriteObjectStart();
        so.Serialize( writer );
        writer.WriteObjectEnd();
      }
    }
    writer.WriteArrayEnd();

    writer.WriteObjectEnd();

    if( !Directory.Exists( DataFileDirectory ) )
      Directory.CreateDirectory( DataFileDirectory );
    File.WriteAllText( DataFilePath, writer.ToString() );

    // serialize images
    File.WriteAllBytes( groundDiffusePath, diffuseTexture.EncodeToPNG() );
    File.WriteAllBytes( groundOverlayPath, overlayTexture.EncodeToPNG() );

    StructureTexture.SetPixels32( structureData );
    File.WriteAllBytes( StructurePath, StructureTexture.EncodeToPNG() );
  }

}

