﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ColorizeUI : MonoBehaviour 
{
  public Color color;
  Color _color;
  Graphic[] graphic;

  public bool OnlyIfEnabled = true;
  public bool AffectImages = true;
  public bool AffectText = true;

  [Header("Images")]
  public bool AffectImageProperties = false;
  public bool SpriteFillCenter = false;

#if UNITY_EDITOR
	void OnEnable () {
		graphic = GetComponentsInChildren<Graphic>();
	}

  public void DoTheThing()
  {
    _color = color;
    foreach( var gra in graphic )
    {
      if( !OnlyIfEnabled || gra.enabled )
      {
        if( ( AffectImages && gra.GetType().IsAssignableFrom( typeof(Image) ) ) || ( AffectText && gra.GetType().IsAssignableFrom( typeof(Text) ) ) )
        {
          gra.color = color;
        }

        if( AffectImages && gra.GetType().IsAssignableFrom( typeof(Image) ) && AffectImageProperties )
        {
          (gra as Image).fillCenter = SpriteFillCenter;
        }
      }
    }
  } 
	void Update()
  {
    if( color != _color )
    {
      DoTheThing();
    }
	}
#endif

}
