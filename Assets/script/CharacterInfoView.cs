﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInfoView : MonoBehaviour 
{
  public Character Character;

  public Text InfoName;

  public Text InfoHealth;
  public Image HealthProgress;
  public Image HealthIcon;

  public Image StaminaProgress;

  public GameObject HiddenIcon;

  public Text InfoAge;
  public Text InfoVocation;
  public Text InfoGender;

  public Text InfoPregnantStatus;
  public GameObject PregnantParent;
  public Image PregnantProgress;
  public Image PregnantIcon;


  public GameObject InfoAffinityTemplate;
  public bool ShowAffinity = true;

  public void ShowInfo( Character cha )
  {
    Character = cha;

    gameObject.SetActive( true );
    InfoName.text = cha.CharacterName;

    InfoHealth.text = "Health: "+cha.Health;
    HealthProgress.fillAmount = cha.HealthNormalized;
    HealthIcon.rectTransform.localScale = Vector3.one * cha.HealthNormalized;

    StaminaProgress.fillAmount = cha.Stamina / cha.StaminaFull;

    HiddenIcon.SetActive( cha.IsHidden );

    InfoAge.text = cha.GetAgeLabel();
    InfoVocation.text = cha.VocationName;
    InfoGender.gameObject.SetActive( cha.identity != null );
    if( cha.identity != null )
      InfoGender.text = (cha.identity.female ? "Female" : "Male");

    InfoPregnantStatus.gameObject.SetActive( cha.IsPregnant );
    PregnantParent.gameObject.SetActive( cha.IsPregnant );
    if( cha.IsPregnant )
    {
      PregnantProgress.fillAmount = cha.PregnantTimer.ProgressNormalized;
      PregnantIcon.rectTransform.localScale = Vector3.one * (0.5f + 0.5f *  cha.PregnantTimer.ProgressNormalized);
    }

    if( ShowAffinity )
    {
      InfoAffinityTemplate.transform.parent.gameObject.SetActive( true );
      for( int i = 0; i < InfoAffinityTemplate.transform.parent.childCount; i++ )
      {
        Transform t = InfoAffinityTemplate.transform.parent.GetChild( i );
        if( t != InfoAffinityTemplate.transform )
          Destroy( t.gameObject );
      }
      InfoAffinityTemplate.SetActive( false );
      foreach( var pair in cha.Affinity )
      {
        // if a character does not have a name then ignore
        //SerializedComponent sc = SerializedObject.ResolveComponentFromId( pair.Value.CharacterId );
        CharacterInfo info = null;
        if( Global.Instance.CharacterInfoLookup.ContainsKey( pair.Value.CharacterId ) )
          info = Global.Instance.CharacterInfoLookup[ pair.Value.CharacterId ];
        if(info!=null)
        {
          if( info.name.Length > 0 )
          {
            GameObject go = GameObject.Instantiate( InfoAffinityTemplate, InfoAffinityTemplate.transform.parent );
            go.SetActive( true );
            Text txt = go.GetComponentInChildren<Text>();
            txt.text = info.name + ": " + pair.Value.Value.ToString(); 
            if( Global.Instance.playerCharacter!=null && info.id == Global.Instance.playerCharacter.id )
              txt.color = new Color32( 248, 207, 98, 255 );
            else
            if( !info.alive )
              txt.color = Color.grey;
            else
              txt.color = Color.white;
          }
        }
      }
    }
    else
    {
      InfoAffinityTemplate.transform.parent.gameObject.SetActive( false );
    }
  }
}
