﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PrefabReference : MonoBehaviour 
{

  public GameObject Prefab;

  public GameObject PreviewPrefab;
  public GameObject Preview;

}