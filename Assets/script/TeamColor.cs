﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamColor : SerializedComponent, ITeam
{
  Team Team;
  [Serialize] public string TeamName;
  public override void BeforeSerialize()
  {
  if( Team==null )
    Debug.Log("team is null for teamcolor");
  else
    TeamName = Team.name;
  }

  public override void AfterDeserialize()
  {
    Team team = Global.Instance.gameData.FindTeam( TeamName );
    if( team!=null )
      SetTeam( team );
  }

  public void SetTeam( Team team )
  {
    Team = team;
    TeamName = team.name;

    MeshRenderer[] mrs = GetComponentsInChildren<MeshRenderer>();
    foreach( var mr in mrs )
    {
      foreach( var mat in mr.materials )
      {
        if( mat.HasProperty("_IndexColor") )
        {
          mat.SetColor( "_IndexColor", team.Color );
        }
      }
    }
  }
}
