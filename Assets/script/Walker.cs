﻿using UnityEngine;
using System.Collections;

public class Walker : MonoBehaviour
{
  public Transform Target;
  [Range( 0.01f, 20f )]
  public float Rate = 4f;
  [Range( 0.01f, 0.3f )]
  public float MaxRange = 0.1f;
  float Range;
  float TargetRange;
  float rampRate = 1;
  public float rampUpRate = 1;
  public float rampDownRate = 0.2f;
  public float ResetThreshold = 0.01f;

  public enum MoveType
  {
    Bounce = 0,
    Wiggle
  }

  public MoveType moveType = MoveType.Bounce;
  public Vector3 Offset{ get; set; }
  [Header( "Optional" )]
  public Billboard AlignLocalToBillboard;

  float timeAccum = 0;
  public Vector3 StartPosition;
  float timeTarget = 0;

  public void On()
  {
    if( Target == null )
      Target = transform;
    TargetRange = MaxRange;
    rampRate = rampUpRate;
    timeTarget = Mathf.Infinity;
  }

  public void Off()
  {
    TargetRange = 0;
    rampRate = rampDownRate;
  }

  void Update()
  {
    if( Target == null )
    {
      enabled = false;
      return;
    }
    timeAccum = Mathf.MoveTowards( timeAccum, timeTarget, Time.deltaTime );
    Range = Mathf.MoveTowards( Range, TargetRange, Time.deltaTime * rampRate );
    if( Range < ResetThreshold )
      timeAccum = 0;

    switch( moveType )
    {
      case MoveType.Bounce:
        Target.localPosition = StartPosition + Offset + ( Vector3.up * ( Mathf.Abs( Mathf.Sin( Rate * timeAccum * Mathf.PI ) ) ) * Range );
        break;
      case MoveType.Wiggle:
        {
          Vector3 right = Vector3.right;
          if( AlignLocalToBillboard != null )
            right = AlignLocalToBillboard.transform.right;
          Target.localPosition = StartPosition + Offset + right * Mathf.Sin( timeAccum ) * Range;
        }
        break;
    }
  }
}
