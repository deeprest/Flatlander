﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(SpriteMeshQuad) )]
public class SpriteMeshQuadEditor : Editor
{
  public override void OnInspectorGUI()
  {
    SpriteMeshQuad smq = target as SpriteMeshQuad;
    int index = EditorGUI.IntField( EditorGUILayout.GetControlRect(), "index", smq.index );
    if( index != smq.index )
    {
      smq.index = index;
      smq.UpdateMesh();
    }
//    if( GUI.Button( EditorGUILayout.GetControlRect(), "update" ) )
//      smq.UpdateMesh();
    DrawDefaultInspector();
  }
}
#endif

[RequireComponent( typeof(MeshRenderer) )]
public class SpriteMeshQuad : SerializedComponent
{
  public MeshRenderer mr;
  public MeshFilter mf;
  public int cellWidth = 1;
  public int cellHeight = 1;
  public int columns = 1;
  public int rows = 1;
  [Serialize]
  public int index = 0;
  Vector2[] uvs = new Vector2[4];

  public override void AfterDeserialize()
  {
    UpdateMesh();
  }

  public void UpdateMesh()
  {
    int x = index % rows;
    int y = index / columns;
    Rect frame = new Rect( (float)x * (float)cellWidth, (float)y*(float)cellHeight, (float)cellWidth, (float)cellHeight );

    float w = mr.sharedMaterial.mainTexture.width;
    float h = mr.sharedMaterial.mainTexture.height;

    uvs[ 0 ].x = frame.x / w;
    uvs[ 0 ].y = ( h - frame.y ) / h;
    uvs[ 1 ].x = ( frame.x + frame.width ) / w;
    uvs[ 1 ].y = ( h - frame.y ) / h;
    uvs[ 2 ].x = frame.x / w;
    uvs[ 2 ].y = ( ( h - frame.y ) - frame.height ) / h;
    uvs[ 3 ].x = ( frame.x + frame.width ) / w;
    uvs[ 3 ].y = ( ( h - frame.y ) - frame.height ) / h;

    if( Application.isPlaying )
    {
      mf.mesh.uv = uvs;

    }
    else
    {
      mf.sharedMesh.uv = uvs;
    }

  }

}
