﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionLibrary : MonoBehaviour 
{
  public GameObject BuildItemTemplate;
  Transform mount;
  public Text BuildObjectName;
  public Material SelectedMaterial;
  public Material RegularMaterial;
  public int SelectedIndex = 0;
  Vector3 offset = Vector3.zero;
  public Vector3 selectedOffset;
  public float size = 0.1f;
  public float radius = 0.5f;
  Transform Selected;
  public float rot = 360f;

  public void Initialize()
  {
    // clear
    Transform p = BuildItemTemplate.transform.parent;
    for( int i = 0; i < p.childCount; i++ )
    {
      Transform c = p.GetChild( i );
      if( c != BuildItemTemplate.transform )
        GameObject.Destroy( c.gameObject );
    }

    mount = BuildItemTemplate.transform.parent;
    BuildItemTemplate.SetActive( false );
    foreach( var bp in Global.Instance.BuildPrefabs )
    {
      GameObject go = GameObject.Instantiate<GameObject>( BuildItemTemplate, mount );
      go.name = bp.name;
      go.SetActive( true );

      Vector3 rootOffset = bp.transform.position;
      Matrix4x4 offset = Matrix4x4.identity;
      offset.SetColumn( 3, new Vector4( -rootOffset.x, -rootOffset.y, -rootOffset.z, 1 ) );

      MeshFilter[] meshFilters = bp.GetComponentsInChildren<MeshFilter>();
      CombineInstance[] combine = new CombineInstance[meshFilters.Length];
      for( int i = 0; i < meshFilters.Length; i++ )
      {
        combine[ i ].mesh = meshFilters[ i ].sharedMesh;
        combine[i].transform = offset * meshFilters[i].transform.localToWorldMatrix;
      }
      Mesh mesh = new Mesh();
      go.GetComponent<MeshFilter>().mesh = mesh;
      mesh.CombineMeshes(combine,true,true);
      go.GetComponent<MeshRenderer>().sharedMaterial = RegularMaterial;
    }
    // remove for indexing
    GameObject.Destroy( BuildItemTemplate );
    Select( 0 );
  }

  public void Show()
  {
    gameObject.SetActive( true );
    BuildObjectName.gameObject.SetActive( true );
  }

  public void Hide()
  {
    gameObject.SetActive( false );
    BuildObjectName.gameObject.SetActive( false );
    Global.Instance.CurrentBuildDistance = 0f;
  }

  public void Select( int i )
  {
    mount.GetChild( SelectedIndex ).GetComponentInChildren<MeshRenderer>().sharedMaterial = RegularMaterial;
    mount.GetChild( SelectedIndex ).transform.localPosition = Vector3.zero;

    SelectedIndex = i;
    Selected = mount.GetChild( SelectedIndex );
    Selected.GetComponentInChildren<MeshRenderer>().sharedMaterial = SelectedMaterial;
    BuildObjectName.text = Selected.gameObject.name;

    //Selected.localRotation = Quaternion.identity;
    Vector3 extents = Selected.GetComponent<MeshFilter>().sharedMesh.bounds.extents;
    extents.y = 0;
    Global.Instance.CurrentBuildDistance = extents.magnitude;
  }

  public float MinSize = 1f;
  public float Power = 1f;
  public float XPower = 1f;

  public void ManualUpdate()
  {
    if( Selected != null )
    {
      Selected.Rotate( Vector3.up * rot * Time.deltaTime );
      Selected.localPosition = selectedOffset;
    }
    transform.rotation = Global.Instance.cameraController.transform.rotation;
    transform.position = Global.Instance.cameraController.transform.position + (mount.rotation * offset);
    int count = mount.childCount;
    float unit = (Mathf.PI*2.0f) / (float)count;
    for( int i = 0; i < count; i++ )
    {
      Transform trf = mount.GetChild( (SelectedIndex+i)%count );
      float sin = Mathf.Sin(((float)i)*unit);
      float squeeze = sin * (2f-Mathf.Abs(sin));
      trf.localPosition = new Vector3( squeeze * radius, 0, Mathf.Cos(((float)i)*unit)*radius );
      float mag = Mathf.Max( MinSize, trf.gameObject.GetComponent<MeshFilter>().sharedMesh.bounds.extents.magnitude );
      trf.localScale = Vector3.one * size * (1f / Mathf.Pow(mag,Power));
    }
  }
}
