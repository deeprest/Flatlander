﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sign : SerializedComponent 
{
  public Text Text;
  public RectTransform Panel;
  [TextArea(3,12)]
  [Serialize] public string text;
  [Serialize] public int lineSpacing;
  [Serialize] public int fontSize;
  [Serialize] public int width = 50;
  [Serialize] public int height = 40;

  public override void AfterDeserialize()
  {
    Text.text = text;
    Text.lineSpacing = lineSpacing;
    Text.fontSize = fontSize;
    Panel.sizeDelta = new Vector2( width, height );
  }

  void OnValidate()
  {
    Text.text = text;
    Text.lineSpacing = lineSpacing;
    Text.fontSize = fontSize;
    Panel.sizeDelta = new Vector2( width, height );
  }
}
