﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Billboard))]
public class Deadbody : SerializedComponent, ILimit, IAction
{
  public bool IsUnderLimit(){ return Limit.IsUnderLimit(); }
  public static Limit<Deadbody> Limit = new Limit<Deadbody>();

  public static int UpdateEveryNthFrame = 5;
  public static int UpdateFrameCounter = 0;
  public static int UpdateID = 0;

  const float DecomposeUpdateInterval = 1;
  const float BrownUntilTime = 120; //120;
  const float DestroyAtTime = 240; //240;
  float StartTime =0;

  public Billboard bb;
  Texture2D tex;
  Color[] pixels;
  static float[] randomClear;
  public Color brown = new Color( 0.5f, 0.45f, 0.4f, 1 );

  [Serialize] public string SpriteName;
  [Serialize] public float Scale;
  [Serialize] public int AgeSeconds;
  [Serialize] public string TeamName;
  [Serialize] public string CharacterName;
  [Serialize]  public int Kills = 0;

  public override void BeforeSerialize()
  {
    AgeSeconds = Mathf.FloorToInt( Time.time - StartTime );
  }

  public override void AfterDeserialize()
  {
    if( TeamName.Length > 0 )
    {
      Team a = Global.Instance.gameData.FindTeam( TeamName );
      if( a != null )
        GetComponent<MeshRenderer>().material.SetColor( "_IndexColor", a.Color );
    }
   
    for( int i = 0; i < Global.Instance.AllSprites.Length; i++ )
    {
      if( Global.Instance.AllSprites[ i ].name == SpriteName )
      {
        AssignSprite( Global.Instance.AllSprites[ i ], Scale );
        break;
      }
    }

    StartTime = Time.time - AgeSeconds;
    UpdateBody();
  }

  void Awake()
  {
    if( !Limit.OnCreate( this ) )
      return;

    tex = (Texture2D)(Texture2D)Object.Instantiate( bb.mr.sharedMaterial.mainTexture );
    bb.mr.material.SetTexture( "_MainTex", tex );
    pixels = tex.GetPixels(); 

    StartTime = Time.time;
    randomClear = new float[ pixels.Length ];
    // assumes an alpha cutoff of 0.5 
    for( int i = 0; i < pixels.Length; i++ )
      randomClear[ i ] = 0.5f + Random.value*0.5f;
  }
    
  public void AssignSprite( Sprite sprite, float scale )
  {
    SpriteName = sprite.name;
    bb.sprite = sprite;
    bb.UpdateMesh();
    Scale = scale;
    transform.localScale = Vector3.one * Scale;
    //Texture2D tex = new Texture2D( sprite.rect.width, sprite.rect.height );
      
  }
    
  public void UpdateBody()
  {
    float DecomposeTime = Time.time - StartTime;

    Color src = Color.clear;
    Color dst = Color.clear;
    if( DecomposeTime < BrownUntilTime )
    {
      float alpha = Mathf.Clamp01( DecomposeTime / BrownUntilTime );
      bb.mr.material.SetColor("_BlendColor", brown );
      bb.mr.material.SetFloat("_BlendAmount", alpha );
    }
    else
    if( DecomposeTime < DestroyAtTime )
    {
      float alpha = Mathf.Clamp01( ( DecomposeTime - BrownUntilTime ) / ( DestroyAtTime - BrownUntilTime ) );
      for( int x = 0; x < tex.width; x++ )
      {
        for( int y = 0; y < tex.height; y++ )
        {
          src = pixels[ x + y * tex.width ];
          if( src.a > 0 )
          {
            dst = Color.Lerp( brown, Color.clear, alpha * randomClear[ x + y * tex.width ] );
            pixels[ x + y * tex.width ] = dst;
          }
        }
      }
      tex.SetPixels( pixels );
      tex.Apply();
    }
    else
      GameObject.Destroy( gameObject );
  
  }

  void OnDestroy()
  {
    Texture2D.Destroy( tex );
    Limit.OnDestroy( this );
  }

  public void GetActionContext( ref ActionData actionData )
  {
    Color c = new Color( .7f,.7f,.7f );
    actionData.instigator.ShowPositionalText( transform.position, CharacterName, 0.1f, c );
    actionData.actions.Add("bury");
  }



  public void OnAction( Character instigator, string action = "default" )
  {
    if( action == "bury" )
    {
      instigator.HideContextMenu();
      GameObject.Destroy( gameObject );
      //spawn gravestone
      Vector3 pos = transform.position;
      pos.y = 0;
      Vector3 look = instigator.transform.position - transform.position;
      look.y = 0;
      GameObject go = Global.Instance.Spawn( "Gravestone", pos, Quaternion.LookRotation( look ), null );
      if( go != null )
      {
        string message = CharacterName + "\n\nKills " + Kills;
        go.GetComponentInChildren<Gravestone>().SetMessage( message );
      }

      //CharacterEvent evt = new CharacterEvent(CharacterEventEnum.Burial,transform.position, Time.time, null, instigator );
      //instigator.BroadcastEvent( evt, Global.Instance.GlobalSightDistance );
    }
  }

 
}
