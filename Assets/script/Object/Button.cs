﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Button : SerializedComponent, IAction
{
  public TextMesh textMesh;
  public string Text;

  public int GenericIntParam;
  public float GenericFloatParam;
  public UnityEvent onAction;

  Character instigator;

  void Awake()
  {
    if( textMesh != null )
      textMesh.text = Text;
  }

  public void OnAction( Character instigator, string action = "default" )
  {
    this.instigator = instigator;
    if( onAction != null )
      onAction.Invoke();
  }

  public void GetActionContext( ref ActionData actionData )
  {
    actionData.indicator.position = transform.position;
    actionData.indicator.rotation = transform.rotation;
    actionData.indicator.localScale = transform.localScale;
    if( textMesh != null )
      textMesh.text = Text;
  }

  // temporary
  public void DiluteWorld()
  {
    //Global.Instance.DiluteWorld( Color.clear, 0.3f );
  }

  public void LoadScene( string sceneName )
  {
    Global.Instance.LoadLevel( sceneName );
  }

  public void SpawnRandom( GameObject prefab )
  {
    Global.Instance.SpawnRandom( prefab, GenericIntParam );
  }

  public void SpawnNearby( GameObject prefab )
  {
    Vector2 direction = transform.forward;
    if( instigator != null )
      direction = instigator.transform.position - transform.position;
    Vector3 delta = direction.normalized * Mathf.Max(0.001f,GenericFloatParam);
    delta.y = 1;
    Vector3 pos = transform.position + delta;
    GameObject go = GameObject.Instantiate( prefab, pos, Quaternion.identity, null );
    go.name = prefab.name;

    // call this manually because the character needs to initialize
    //Character character = go.GetComponent<Character>();
//    if( character != null )
//      character.AfterDeserialize();
    

    CarryObject swag = go.GetComponent<CarryObject>();
    if( swag != null )
    {
      go.transform.rotation = Quaternion.LookRotation(delta, Vector3.up) * Quaternion.LookRotation( swag.GroundForward, swag.GroundUp );
    }
  }

  public void PlayAnimation( AnimationClip clip )
  {
    Animation anim = GetComponent<Animation>();
    anim.clip = clip;
    anim.Stop();
    anim.Play();
  }
}

