﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : SerializedComponent, IAction
{
  [Serialize] public bool isLampOn = true;
  public Light myLight;
  public Transform InteractTransform;

  void Awake()
  {
    if( myLight==null )
      myLight = GetComponent<Light>();
    myLight.enabled = isLampOn;
  }

  public void ToggleEnabled()
  {
    isLampOn = !isLampOn;
    myLight.enabled = isLampOn;
  }


  public override void AfterDeserialize()
  {
    myLight.enabled = isLampOn;
  }

  public void OnAction( Character instigator, string action = "default" )
  {
    ToggleEnabled();
  }

  public void GetActionContext( ref ActionData actionData )
  {
    actionData.indicator.position = InteractTransform.position;
    actionData.indicator.rotation = InteractTransform.rotation;
  }
}
