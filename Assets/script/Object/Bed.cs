﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof(CarryObject) )]
public class Bed : SerializedComponent, IAction, IOwnable
{
  public Destination destination;
  //  public TextMesh textMesh;
  public Transform sleepTarget;
  public Transform wakeTarget;
  public Tag[] BedForTags;

  public bool IsOwned()
  {
    return destination.ClaimedBy != null;
  }

  public void ClearOwner()
  {
    destination.ClearOwner();
  }

  public void AssignOwner( Character owner )
  {
    destination.AssignOwner( owner );
  }


  public override void BeforeSerialize()
  {

  }

  public override void AfterDeserialize()
  {
//    if( textMesh != null )
//    {
//      if( destination.ClaimedBy == null )
//        textMesh.text = "(unowned)";
//      else
//        textMesh.text = "owned by " + destination.ClaimedBy.CharacterName;
//    }
  }

  public void OnAction( Character instigator, string action = "default" )
  {
    instigator.HideContextMenu();
    if( action == "unclaim" )
    {
      ClearOwner();
    }
    if( action == "claim" )
    {
      if( destination.ClaimedBy == null )
      {
        Global.Instance.AssignTeam( gameObject, instigator.Team );

        // bed
        if( instigator.Bed != null )
          instigator.Bed.ClearOwner();
        instigator.Bed = this;
        AssignOwner( instigator );

        // home
        if( instigator.Home != null )
          instigator.Home.ClearOwner();
        instigator.Home = destination;
        destination.AssignOwner( instigator );

      }
      else
      {
        Global.Instance.Speak( instigator, "This is owned by " + destination.ClaimedBy.CharacterName, 3, 2 );
      }
    }
    if( action == "carry" )
    {
      instigator.HoldObject( transform );
    }
    if( action == "sleep" )
    {
      instigator.HideContextMenu();
      instigator.GoToBed( this );
    }
  }




  public void GetActionContext( ref ActionData actionData )
  {
    Vector3 pos = transform.position;
    pos.y = Global.Instance.GlobalSpriteOnGroundY;
    actionData.indicator.position = pos;

    if( destination.ClaimedBy == actionData.instigator )
    {
      actionData.actions.Add( "unclaim" );
      actionData.actions.Add( "sleep" );
    }
    else
      actionData.actions.Add( "claim" );

  }
}

