﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Food : SerializedComponent, IAction, ILimit
{
  public bool IsUnderLimit(){ return Limit.IsUnderLimit(); }
  public static Limit<Food> Limit = new Limit<Food>();

  public AudioClip ConsumeSound;

  void Awake()
  {
    Limit.OnCreate( this );
  }

  void OnDestroy()
  {
    Limit.OnDestroy( this );
  }

  public void OnAction( Character instigator, string action = "default" )
  {
    if( action == "eat" )
    {
      instigator.HideContextMenu();
      instigator.Health += 3;
      if( ConsumeSound != null )
        instigator.audioSource.PlayOneShot( ConsumeSound );
      GameObject.Destroy( gameObject );
    }

  }

  public void GetActionContext( ref ActionData actionData )
  {
//    position.y = World.Instance.GlobalSpriteOnGroundY;
  }
}
