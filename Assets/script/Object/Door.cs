﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[SelectionBase]
public class Door : SerializedComponent, ITeam
{
  public bool isOpen = false;
  public Collider doorCollider;
  public MeshRenderer doorRenderer;
  public AudioClip clipOpen;
  public AudioClip clipClose;
  public AudioSource audioSource;
  List<Collider> colliders = new List<Collider>();
  public List<Tag> openForTags = new List<Tag>();
  public NavMeshObstacle obstacle;

  Team Team;
  [Serialize] public string TeamName;

  public override void BeforeSerialize()
  {
    if( Team!=null )
      TeamName = Team.name;
  }

  public override void AfterDeserialize()
  {
    Team team = Global.Instance.gameData.FindTeam( TeamName );
    if( team!=null )
      SetTeam( team );
  }

  void Awake()
  {
    //Team = World.Instance.gameData.FindTeam( Tag.Neutral );
    if( obstacle!=null )
      obstacle.carving = !isOpen;
  }

  void OnTriggerEnter( Collider other )
  {
    if( other.isTrigger )
      return;
    Tags tags = other.gameObject.GetComponent<Tags>();
    if( tags == null )
      return;
    if( openForTags.Count==0 || tags.HasAnyTag( openForTags.ToArray() ) )
    {
      colliders.Add( other );
      Open();
    }
  }

  void OnTriggerExit( Collider other )
  {
    if( other.isTrigger )
      return;
    
    colliders.Remove( other );
    if( colliders.Count == 0 )
      Close();
  }

  void Open()
  {
    if( obstacle!=null )
      obstacle.carving = false;
    doorCollider.enabled = false;
    doorRenderer.enabled = false;
    if( !isOpen )
    {
      if( audioSource != null && clipOpen != null )
        audioSource.PlayOneShot( clipOpen );
    }
    isOpen = true;
  }

  void Close()
  {
    if( obstacle!=null )
      obstacle.carving = true;
    doorCollider.enabled = true;
    doorRenderer.enabled = true;
    if( isOpen )
    {
      if( audioSource != null && clipClose != null )
        audioSource.PlayOneShot( clipClose );
    }
    isOpen = false;
  }
    
  public void SetTeam( Team team )
  {
    Team = team;
    TeamName = team.name;
    openForTags.Clear();
    openForTags.Add( team.Tag );

    MeshRenderer[] mrs = GetComponentsInChildren<MeshRenderer>();
    foreach( var mr in mrs )
    {
      foreach( var mat in mr.materials )
      {
        if( mat.HasProperty("_IndexColor") )
        {
          mat.SetColor( "_IndexColor", team.Color );
        }
      }
    }
  }
}
