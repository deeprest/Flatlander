﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Campfire : MonoBehaviour {

  public Light Light;
  public Transform sprite;
  AudioSource source;
  public AudioClip calm;
  public AudioClip rage;
  public Damage fireDamage;

  public float lightIntensityLow = 3;
  public float lightIntensityHigh = 10;

  float rageStart;
  public float rageDuration = 10;
  public float rageLow = 10;
  public float rageHigh = 20;


  void Start()
  {
    source = GetComponent<AudioSource>();
    source.clip = calm;
    source.loop = true;
    source.Play();
    UpdateRage();
  }

  void Update()
  {
    if( Time.time - rageStart < rageDuration )
    {
      UpdateRage();
    }
  }

  void UpdateRage()
  {
    float alpha = 1f - Mathf.Clamp01( ( Time.time - rageStart ) / rageDuration );
    sprite.localScale = Vector3.one * ( rageLow + alpha * ( rageHigh - rageLow ) );
    Light.intensity = lightIntensityLow + ( lightIntensityHigh - lightIntensityLow ) * alpha;
  }

  void OnTriggerStay(Collider col )
  {
    IDamage otherChar = col.gameObject.GetComponent<IDamage>();
    if( otherChar != null )
    {
      otherChar.TakeDamage( fireDamage );
    }
  }

  void OnTriggerEnter( Collider col )
  {
    Tags tags = col.gameObject.GetComponent<Tags>();
    if( tags == null )
      return;

    if( tags.HasTag( Tag.Food ) )
    {
      CarryObject swag = col.gameObject.GetComponent<CarryObject>();
      if( swag != null )
      {
        CharacterEvent evt = new CharacterEvent( CharacterEventEnum.FoodAdded, transform.position, Time.time, swag.HeldByCharacter, swag.HeldByCharacter );
        swag.HeldByCharacter.BroadcastEvent( evt, Global.Instance.GlobalSightDistance );
      }
    }
    if( tags.HasTag( Tag.Combustible ) )
    {
      rageStart = Time.time;
      GameObject.Destroy( col.gameObject );
      Global.Instance.AudioOneShot( rage, transform.position );
    }

  }
    
}
