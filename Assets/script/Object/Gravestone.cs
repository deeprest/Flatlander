﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gravestone : SerializedComponent 
{
  public Text Text;
  [Serialize] public string Message;

  public override void AfterDeserialize()
  {
    SetMessage( Message );
  }
  public void SetMessage( string msg )
  {
    Text.text = msg;
    Message = msg;
  }
}
