﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bush : MonoBehaviour 
{
  public AudioClip[] Rustle;

  public void PlayRandomRustle()
  {
    Global.Instance.AudioOneShot( Rustle[ Random.Range( 0, Rustle.Length ) ], transform.position  );
  }
}
