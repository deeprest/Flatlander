﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour, IDamage
{
  public bool CanBump = true;
  public float BumpAmount = 10f; 

  public bool Reflect = false;
  public float ReflectIntensity = 1;
  public float offset = 0;

  public void TakeDamage( Damage d )
  {
//    print( "Shield damage taken: " + d.Amount );
  }

  void OnCollisionEnter( Collision other )
  {
    if( CanBump )
    {
      Character cha = other.transform.GetComponent<Character>();
      if( cha != null )
      {
        cha.body.AddForce( transform.forward * BumpAmount, ForceMode.Impulse );
      }
    }
    if( Reflect )
    {
      print( "SHIELD reflects "+other.gameObject.name );
      Rigidbody body = other.transform.GetComponent<Rigidbody>();
      if( body != null )
      {
        Vector3 away = -body.transform.forward;
        body.position = other.contacts[ 0 ].point + away * offset;
        body.rotation = Quaternion.LookRotation( away );
//        body.AddForce( away * ReflectIntensity, ForceMode.VelocityChange );
        body.velocity = -body.velocity * ReflectIntensity;
      }
//        
    }
  }
}
