﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
[CreateAssetMenu]
public class Identity : ScriptableObject
{
  new public string name;
  public bool female;
  public Jabber jabber;
  public List<Speech> speeches;
  public List<Sprite> sprites;
}
