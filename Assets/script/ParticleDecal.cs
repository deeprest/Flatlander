﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDecal : MonoBehaviour 
{
  public ParticleSystem ps;
  public Color paintColor;
  List<ParticleCollisionEvent> events = new List<ParticleCollisionEvent>();

  void Awake()
  {
    if( ps == null )
      ps = gameObject.GetComponent<ParticleSystem>();
  }
    
  void OnParticleCollision(GameObject other)
  {
    Zone zone = Global.Instance.GetZone( gameObject );

    int count = ps.GetCollisionEvents( other, events );
    if( count > 0 )
    {
      foreach( var evt in events )
      {
        zone.PaintSingleGroundPixel( evt.intersection, paintColor );
      }
    }
  }
    
}
