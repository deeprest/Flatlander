﻿using UnityEngine;
using System.Collections;

public class Rise : MonoBehaviour
{
  public float duration = 5f;
  float start=0;
  // Use this for initialization
  void Start()
  {
    transform.localScale = new Vector3(1,.2f,1);
    start = Time.time;
  }
	
  // Update is called once per frame
  void Update()
  {
    transform.localScale = new Vector3(1,Mathf.Clamp( (Time.time - start)/duration, 0.0f, 1.0f ),1);
    if( Time.time - start > duration )
      Destroy( this );
  }
}

