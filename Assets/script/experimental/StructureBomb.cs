﻿using UnityEngine;
using System.Collections;

public class StructureBomb : MonoBehaviour
{
  public int bombSize = 3;

  public void Boom()
  {
    Zone zone = Global.Instance.GetZone( gameObject );
    Vector2Int coord = new Vector2Int( Mathf.FloorToInt( transform.position.x ), Mathf.FloorToInt( transform.position.z ) );
    int xstart = coord.x - ( bombSize / 2 );
    int ystart = coord.y - ( bombSize / 2 );
    for( int x =  xstart; x < xstart+bombSize; x++ )
    {
      for( int y = ystart; y < ystart+bombSize; y++ )
      {
        zone.SetStructureData( x, y, PixelBit.Building );
      }
    }
    zone.UpdateStructure( xstart, ystart, bombSize, bombSize );
    
    Destroy( this.gameObject );
  }

}

