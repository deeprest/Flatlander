﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendShape : MonoBehaviour 
{

  [Range(0,1)]
  public float alpha = 0;
  float cachedAlpha=0;
  public int shapeIndex = 0;
  int cachedShapeIndex=-1;

  Mesh m;
  // convert from Blender mesh space to Unity
  Vector3 convert = Vector3.zero;
  Vector3[] baseVerts;
  Vector3[] v,n,t;
  Vector3[] final;

	void Start () 
  {
    m = GetComponent<MeshFilter>().mesh;
    baseVerts = new Vector3[m.vertexCount];
    m.vertices.CopyTo( baseVerts, 0 );

    final = new Vector3[ m.vertexCount ];
    v = new Vector3[ m.vertexCount ];
    n = new Vector3[ m.vertexCount ];
    t = new Vector3[ m.vertexCount ];

    /*Debug.Log( m.blendShapeCount );
    for( int i = 0; i < m.blendShapeCount; i++ )
    {
      Debug.Log( "name "+m.GetBlendShapeName( i ) +" framecount "+m.GetBlendShapeFrameCount( i ) );
      for( int f = 0; f < m.GetBlendShapeFrameCount( i ); f++ )
        Debug.Log( "weight"+f+ " " + m.GetBlendShapeFrameWeight( i, f ) );
    }*/
	}
	


	void Update () 
  {
    if( shapeIndex != cachedShapeIndex )
    {
      if( shapeIndex >= m.blendShapeCount )
        shapeIndex = m.blendShapeCount - 1;
      if( shapeIndex < 0 )
        shapeIndex = 0;
      cachedShapeIndex = shapeIndex;
      m.GetBlendShapeFrameVertices( shapeIndex, 0, v, n, t );
      // force change to cachedAlpha
      cachedAlpha = -1;
    }
    if( alpha != cachedAlpha )
    {
      cachedAlpha = alpha;
      for( int i = 0; i < m.vertexCount; i++ )
      {
        convert.x = v[ i ].x;
        convert.y = v[ i ].z;
        convert.z = -v[ i ].y;
        final[ i ] = baseVerts[ i ] + convert * alpha;
      }
      m.vertices = final;

    }
	}
}
