﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public struct PlayerData
{
  public string name;
  public int kills;
}

public class Web : MonoBehaviour
{
  void Start()
  {
    StartCoroutine(Test());
  }

  IEnumerator Test()
  {
    yield return WebQuery( "localhost:8080/?command=getplayerdata&id=bob", delegate(string json )
    {
      Debug.Log(json);
      //PlayerData data = JsonUtility.FromJson<PlayerData>( json );
      //World.Instance.pc.character.nametag.text = data.name + data.kills;
    } );
      
    yield return WebQuery( "localhost:8080/?command=getlevel&id=test", delegate(string json )
    {
      Debug.Log(json);
    } );

    yield return null;
  }

  IEnumerator WebQuery( string queryString, System.Action<string> callback )
  {
    using (UnityWebRequest www = UnityWebRequest.Get(queryString))
    {
      UnityWebRequestAsyncOperation req = www.SendWebRequest();
      while( !req.isDone )
      {
        Debug.Log( (req.progress*100f) + "% "+www.downloadProgress );
        yield return null;
      }
      if (www.isNetworkError || www.isHttpError)
      {
        Debug.Log(www.error);
      }
      else
      {
        callback.Invoke( www.downloadHandler.text );
      }
    }
  }

}
