﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor( typeof(Destination) )]
public class DestinationEditor : Editor
{
  void OnSceneGUI()
  {
    Destination d = target as Destination;
    //Handles.CircleHandleCap (0, d.transform.position, Quaternion.LookRotation(Vector3.up), d.arrivalRadius, EventType.Repaint);
    Handles.color = d.EditorColor;
    Handles.DrawSolidDisc( d.transform.position, Vector3.up, d.ArrivalRadius );
    Handles.color = new Color( 1, 1, 1, 0.5f );
    Handles.CircleHandleCap( 0, d.transform.position, Quaternion.LookRotation( Vector3.up ), d.ArrivalRadius, EventType.Repaint );
  
  }
}

[CustomEditor( typeof(Global) )]
public class GlobalEditor : Editor
{
  Global zone;
  public GameObject spawnPrefab;
  public int spawnCount;
  string levelName;

  public override void OnInspectorGUI()
  {
    zone = ( target as Global );
    levelName = EditorGUILayout.TextField( levelName );
    /*if( GUI.Button( EditorGUILayout.GetControlRect(), "Construct" ) )
      zone.StartCoroutine( zone.Construct( levelName ) );
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Destruct" ) )
      zone.Destruct();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Serialize" ) )
      zone.Serialize();*/
    spawnPrefab = (GameObject)EditorGUILayout.ObjectField( spawnPrefab, typeof(GameObject), false );
    spawnCount = EditorGUILayout.IntField( spawnCount );
    if( spawnPrefab != null )
    {
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Spawn Random" ) )
        zone.SpawnRandom( spawnPrefab, spawnCount );
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Spawn Conway" ) )
        zone.SpawnConway( spawnPrefab, spawnCount );
    }
    DrawDefaultInspector();
  }
}

[CustomEditor( typeof(Zone) )]
public class ZoneEditor : Editor
{
  Zone zone;

  static float amount = 0.3f;
  static Color color = Color.white;
  static int iterations = 0;

  public override void OnInspectorGUI()
  {
    zone = ( target as Zone );
    amount = EditorGUILayout.FloatField( amount );
    color = EditorGUILayout.ColorField("Color", color );
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Paint" ) )
    {
      zone.PaintGround( color, amount );
    }
      
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Clear Ground Dirt Image" ) )
    {
      zone.ClearGroundDirtImage();
    }
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Burn the Dirt" ) )
    {
      zone.BurnTheDirt();
    }

    iterations = EditorGUILayout.IntField( iterations );


    if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate" ) )
      zone.Generate();

    DrawDefaultInspector();
  }
}
 

[CustomEditor( typeof(Ball) )]
[CanEditMultipleObjects()]
public class BallSceneEditor : Editor
{
  void OnSceneGUI()
  {
    Ball obj = target as Ball;
    if( obj == null )
      return;

    if( obj.LockToGround )
    {
      SphereCollider sphere = obj.GetComponent<SphereCollider>();
      if( sphere != null )
      {
        Vector3 pos = obj.transform.position;
        pos.y = sphere.radius;
        obj.transform.position = pos;
      }

    }
  }
}

[CanEditMultipleObjects]
[CustomEditor( typeof(SpriteAnimation) )]
public class SpriteAnimationEditor : Editor
{
  string seq = "";

  public override void OnInspectorGUI()
  {
    SpriteAnimation sa = target as SpriteAnimation;
    seq = GUI.TextField( EditorGUILayout.GetControlRect(), seq );
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Play" ) )
      sa.Play( seq, true );
    DrawDefaultInspector();
  }
  /*
  static void OnSelectionChange()
  {
    Debug.Log( "Selection change" );
    if( Selection.activeGameObject != null )
    {
      SpriteAnimation sa = Selection.activeGameObject.GetComponent<SpriteAnimation>();
      if( sa != null )
      {
        selectedSpriteAnimation = sa;
      }
    }

    if( selectedSpriteAnimation != null && selectedSpriteAnimation!=sa )
    {
      Mesh.DestroyImmediate( selectedSpriteAnimation.mf.sharedMesh );
      selectedSpriteAnimation.mf.sharedMesh = selectedSpriteAnimation.editorMesh;
      selectedSpriteAnimation = null;
    }
    if( Selection.activeGameObject != null )
    {
      Debug.Log( "assign editor mesh" );
//        Mesh mesh = selectedSpriteAnimation.mf.sharedMesh;
      selectedSpriteAnimation.editorMesh = selectedSpriteAnimation.mf.sharedMesh;
//        Mesh mesh2 = Mesh.Instantiate(mesh);
//        selectedSpriteAnimation.mf.sharedMesh = mesh2;
      selectedSpriteAnimation.mf.sharedMesh = Mesh.Instantiate( selectedSpriteAnimation.mf.sharedMesh );

    }
  }
  */
}
/*
public static class SelectionHandler
{
  static SpriteAnimation selectedSpriteAnimation;
  static Mesh sharedMesh;

  [MenuItem ("GameObject/mehSelectionHandler ^o")]
  static void mehSelectionHandler()
  {
    Debug.Log( "mehSelectionHandler" );
    Selection.selectionChanged += OnSelectionChange;
  }

  static void OnSelectionChange()
  {
    Debug.Log( "Selection change" );
    if( selectedSpriteAnimation != null )
    {
      Mesh.DestroyImmediate( selectedSpriteAnimation.mf.sharedMesh );
      selectedSpriteAnimation.mf.sharedMesh = sharedMesh;
      sharedMesh = null;
      selectedSpriteAnimation = null;
    }
    if( Selection.activeGameObject != null )
    {
      SpriteAnimation sa = Selection.activeGameObject.GetComponent<SpriteAnimation>();
      if( sa != null )
      {
        selectedSpriteAnimation = sa;

        Mesh mesh = selectedSpriteAnimation.mf.sharedMesh;
        sharedMesh = selectedSpriteAnimation.mf.sharedMesh;
        Mesh mesh2 = Mesh.Instantiate(mesh);
        selectedSpriteAnimation.mf.sharedMesh = mesh2;
      }
    }
  }
}
*/
public class InsertParent : ScriptableObject
{
  [MenuItem ("GameObject/Create Parent of Selected")]
  static void MenuInsertParent()
  {
    Transform[] transforms = Selection.GetTransforms(SelectionMode.TopLevel |
      SelectionMode.OnlyUserModifiable);

    GameObject newParent = new GameObject("_Parent");
    Transform newParentTransform = newParent.transform;

    if(transforms.Length == 1)
    {
      Transform originalParent = transforms[0].parent;
      transforms[0].parent = newParentTransform;
      if(originalParent)
        newParentTransform.parent = originalParent;
    }
    else
    {
      foreach(Transform transform in transforms)
        transform.parent = newParentTransform;
    }
  }
}

/*
[InitializeOnLoadAttribute]
[CanEditMultipleObjects]
[CustomEditor( typeof(PrefabReference) )]
public class PrefabReferenceEditor : Editor
{
  static PrefabReferenceEditor()
  {
    EditorApplication.playModeStateChanged += LogPlayModeState;
  }

  private static void LogPlayModeState(PlayModeStateChange state)
  {
    Debug.Log(state);
    if( state == PlayModeStateChange.EnteredEditMode )
    {
      PrefabReference[] prs = FindObjectsOfType<PrefabReference>();
      foreach( var pr in prs )
      {
        if( pr.Preview == null )
        {
          pr.Preview = GameObject.Instantiate( pr.Prefab, Vector3.zero, Quaternion.identity, pr.transform );
        }
      }
    }
    if( state == PlayModeStateChange.ExitingEditMode )
    {
      PrefabReference[] prs = FindObjectsOfType<PrefabReference>();
      foreach( var pr in prs )
      {
        if( pr.Preview != null )
          GameObject.DestroyImmediate( pr.Preview );
      }
    }
  }

  SerializedProperty prefabProp;
  SerializedProperty previewPrefabProp;
  SerializedProperty previewProp;

  void OnEnable () 
  {
    prefabProp = serializedObject.FindProperty ("Prefab");
    previewPrefabProp = serializedObject.FindProperty ("PreviewPrefab");
    previewProp = serializedObject.FindProperty ("Preview");
  }

  public override void OnInspectorGUI()
  {
    serializedObject.Update();

//    DrawDefaultInspector ();

    EditorGUILayout.PropertyField (prefabProp, new GUIContent ("Prefab"));

    serializedObject.ApplyModifiedProperties();


    bool createPreview = false;
    if( prefabProp.objectReferenceInstanceIDValue != previewPrefabProp.objectReferenceInstanceIDValue )
    {
      createPreview = true;
      Debug.Log( serializedObject.targetObject.name + " value changed" );
      previewPrefabProp.objectReferenceValue = prefabProp.objectReferenceValue;
    }
    if( previewProp.objectReferenceValue == null )
      createPreview = true;

    if( createPreview )
    {
      GameObject.DestroyImmediate( previewProp.objectReferenceValue );
//        PrefabReference pr = serializedObject.targetObject as PrefabReference;
//        previewProp.objectReferenceValue = GameObject.Instantiate( prefabProp.objectReferenceValue, pr.gameObject.transform.position, pr.gameObject.transform.rotation, pr.gameObject.transform );
    }
  }

}
*/

[CanEditMultipleObjects]
[CustomEditor( typeof(SerializedObject) )]
public class SerializedObjectEditor : Editor
{

  public override void OnInspectorGUI()
  {
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Validate" ) )
    {
      foreach( var obj in targets )
      {
        ( obj as SerializedObject ).OnValidate();
      }
    }
    DrawDefaultInspector();
  }

}
  

[CustomEditor( typeof(ColorizeUI) )]
public class ColorizeUIEditor : Editor
{

  public override void OnInspectorGUI()
  {
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Colorize!" ) )
    {
      (target as ColorizeUI).DoTheThing();
    }
    DrawDefaultInspector();
  }

}


