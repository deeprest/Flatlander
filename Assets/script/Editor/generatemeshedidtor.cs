#if false
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;


// Generate Mesh From Texture or Sprite
public class GenerateMeshEditor : EditorWindow
{
  bool createObject = true;
  bool createPrefab = false;
  bool createMaterial = true;
  GameObject prefab = null;
  Material material = null;
  // TODO: create individual materials ( or share )
  float alphaThreshold = 0.1f;
  float scale = 0.03f;
  float rotateZ = 0;
  float scaleZ = 1;
  bool centerMesh = true;
  string outputdir = "";

  [MenuItem( "Window/Generate Mesh" )]
  static void Init()
  {
    // Get existing open window or if none, make a new one:
    GenerateMeshEditor window = (GenerateMeshEditor)EditorWindow.GetWindow( typeof(GenerateMeshEditor) );
    window.Show();
  }

  void OnGUI()
  {
    outputdir = EditorGUILayout.TextField( "output dir", outputdir );
    prefab = (GameObject)EditorGUILayout.ObjectField( "prefab", prefab, typeof(GameObject), false );
    material = (Material)EditorGUILayout.ObjectField( "material", material, typeof(Material), false );
    createPrefab = EditorGUILayout.Toggle( "create prefabs", createPrefab );
    createObject = EditorGUILayout.Toggle( "create object in scene", createObject );
    alphaThreshold = EditorGUILayout.FloatField( "alpha threshold", alphaThreshold );
    scale = EditorGUILayout.FloatField( "scale", scale );
    rotateZ = EditorGUILayout.FloatField( "rotateZ", rotateZ );
    scaleZ = EditorGUILayout.FloatField( "scaleZ", scaleZ );
    centerMesh = EditorGUILayout.Toggle( "centerMesh", centerMesh );

    if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate Mesh From Texture or Sprite" ) )
    {
      foreach( var obj in Selection.objects )
        GenerateMeshFromTextureOrSprite( obj );
    }

  }

  void GenerateMeshFromTextureOrSprite( Object obj )
  {
    string resultName = "noname";
    RectInt r = new RectInt();
    Mesh mesh = new Mesh();

    Texture2D texture = obj as Texture2D;
    if( texture != null )
    {
      r = new RectInt( 0, 0, texture.width, texture.height );
      resultName = texture.name;
    }

    Sprite sprite = obj as Sprite;
    if( sprite != null )
    {
      r = new RectInt( (int)sprite.rect.x, (int)sprite.rect.y, (int)sprite.rect.width, (int)sprite.rect.height );
      texture = sprite.texture;
      resultName = sprite.name;
    }

    if( texture == null && sprite == null )
    {
      Debug.LogError( "must select textures or sprites in project view first" );
      return;
    } 

    Color[] colors = texture.GetPixels();
    List<int> triangles = new List<int>();
    List<Vector3> vertices = new List<Vector3>();
    List<Vector3> uvs = new List<Vector3>();
    List<Vector3> normals = new List<Vector3>();
    int triangleIndex = 0;
    int colorsLength = colors.Length;
    int index;
    float textureWidth = (float)texture.width;
    float textureHeight = (float)texture.height;



    for( int x = r.x; x < r.x + r.width; x++ )
    {
      for( int y = r.y; y < r.y + r.height; y++ )
      {
        index = x + y * texture.width;

        Color pixel = colors[ index ];
        if( pixel.a < alphaThreshold )
          continue;

        Color left = Color.clear;
        Color right = Color.clear;
        Color bottom = Color.clear;
        Color top = Color.clear;

        if( x > r.x )
          left = colors[ x - 1 + y * texture.width ];
        if( x < r.x + r.width - 1 )
          right = colors[ x + 1 + y * texture.width ];
        if( y > r.y )
          bottom = colors[ x + ( y - 1 ) * texture.width ];
        if( y < r.y + r.height - 1 )
          top = colors[ x + ( y + 1 ) * texture.width ];

        // front face
        {
          triangles.Add( triangleIndex + 0 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 2 );
          triangleIndex += 4;

          Vector3 v0 = new Vector3( (float)x, (float)y, 0 );
          vertices.Add( v0 );
          normals.Add( Vector3.back );
          uvs.Add( new Vector3( v0.x / textureWidth, v0.y / textureHeight, 0 ) );

          Vector3 v1 = new Vector3( (float)( x ), (float)( y + 1 ), 0 );
          vertices.Add( v1 );
          normals.Add( Vector3.back );
          uvs.Add( new Vector3( v1.x / textureWidth, v1.y / textureHeight, 0 ) );

          Vector3 v2 = new Vector3( (float)( x + 1 ), (float)( y + 1 ), 0 );
          vertices.Add( v2 );
          normals.Add( Vector3.back );
          uvs.Add( new Vector3( v2.x / textureWidth, v2.y / textureHeight, 0 ) );

          Vector3 v3 = new Vector3( (float)( x + 1 ), (float)( y ), 0 );
          vertices.Add( v3 );
          normals.Add( Vector3.back );
          uvs.Add( new Vector3( v3.x / textureWidth, v3.y / textureHeight, 0 ) );
        }

        // back face
        {
          triangles.Add( triangleIndex + 0 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 2 );
          triangleIndex += 4;

          Vector3 v0 = new Vector3( (float)( x + 1 ), (float)y, 1 );
          vertices.Add( v0 );
          normals.Add( Vector3.forward );
          uvs.Add( new Vector3( v0.x / textureWidth, v0.y / textureHeight, 0 ) );

          Vector3 v1 = new Vector3( (float)( x + 1 ), (float)( y + 1 ), 1 );
          vertices.Add( v1 );
          normals.Add( Vector3.forward );
          uvs.Add( new Vector3( v1.x / textureWidth, v1.y / textureHeight, 0 ) );

          Vector3 v2 = new Vector3( (float)( x ), (float)( y + 1 ), 1 );
          vertices.Add( v2 );
          normals.Add( Vector3.forward );
          uvs.Add( new Vector3( v2.x / textureWidth, v2.y / textureHeight, 0 ) );

          Vector3 v3 = new Vector3( (float)( x ), (float)( y ), 1 );
          vertices.Add( v3 );
          normals.Add( Vector3.forward );
          uvs.Add( new Vector3( v3.x / textureWidth, v3.y / textureHeight, 0 ) );
        }

        if( left.a < alphaThreshold )
        {
          triangles.Add( triangleIndex + 0 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 2 );
          triangleIndex += 4;

          Vector3 v0 = new Vector3( (float)x, (float)y, 1 );
          vertices.Add( v0 );
          normals.Add( Vector3.left );
          uvs.Add( new Vector3( (v0.x+0.5f) / textureWidth, (v0.y+0.5f) / textureHeight, 0 ) );

          Vector3 v1 = new Vector3( (float)( x ), (float)( y + 1 ), 1 );
          vertices.Add( v1 );
          normals.Add( Vector3.left );
          uvs.Add( new Vector3( (v0.x+0.5f) / textureWidth, (v0.y+0.5f) / textureHeight, 0 ) );

          Vector3 v2 = new Vector3( (float)( x ), (float)( y + 1 ), 0 );
          vertices.Add( v2 );
          normals.Add( Vector3.left );
          uvs.Add( new Vector3( (v0.x+0.5f) / textureWidth, (v0.y+0.5f) / textureHeight, 0 ) );

          Vector3 v3 = new Vector3( (float)( x ), (float)( y ), 0 );
          vertices.Add( v3 );
          normals.Add( Vector3.left );
          uvs.Add( new Vector3( (v0.x+0.5f) / textureWidth, (v0.y+0.5f) / textureHeight, 0 ) );
        }

        if( bottom.a < alphaThreshold )
        {
          triangles.Add( triangleIndex + 0 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 2 );
          triangleIndex += 4;

          Vector3 v0 = new Vector3( (float)x, (float)y, 1 );
          vertices.Add( v0 );
          normals.Add( Vector3.down );
          uvs.Add( new Vector3( v0.x / textureWidth, v0.y / textureHeight, 0 ) );

          Vector3 v1 = new Vector3( (float)( x ), (float)( y ), 0 );
          vertices.Add( v1 );
          normals.Add( Vector3.down );
          uvs.Add( new Vector3( v1.x / textureWidth, v1.y / textureHeight, 0 ) );

          Vector3 v2 = new Vector3( (float)( x + 1 ), (float)( y ), 0 );
          vertices.Add( v2 );
          normals.Add( Vector3.down );
          uvs.Add( new Vector3( v2.x / textureWidth, v2.y / textureHeight, 0 ) );

          Vector3 v3 = new Vector3( (float)( x + 1 ), (float)( y ), 1 );
          vertices.Add( v3 );
          normals.Add( Vector3.down );
          uvs.Add( new Vector3( v3.x / textureWidth, v3.y / textureHeight, 0 ) );
        }

        if( right.a < alphaThreshold )
        {
          triangles.Add( triangleIndex + 0 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 2 );
          triangleIndex += 4;

          Vector3 v0 = new Vector3( (float)( x + 1 ), (float)y, 0 );
          vertices.Add( v0 );
          normals.Add( Vector3.right );
          uvs.Add( new Vector3( (v0.x-1) / textureWidth, v0.y / textureHeight, 0 ) );

          Vector3 v1 = new Vector3( (float)( x + 1 ), (float)( y + 1 ), 0 );
          vertices.Add( v1 );
          normals.Add( Vector3.right );
          uvs.Add( new Vector3( (v0.x-1) / textureWidth, v0.y / textureHeight, 0 ) );

          Vector3 v2 = new Vector3( (float)( x + 1 ), (float)( y + 1 ), 1 );
          vertices.Add( v2 );
          normals.Add( Vector3.right );
          uvs.Add( new Vector3( (v0.x-1) / textureWidth, v0.y / textureHeight, 0 ) );

          Vector3 v3 = new Vector3( (float)( x + 1 ), (float)( y ), 1 );
          vertices.Add( v3 );
          normals.Add( Vector3.right );
          uvs.Add( new Vector3( (v0.x-1) / textureWidth, v0.y / textureHeight, 0 ) );
        }

        if( top.a < alphaThreshold )
        {
          triangles.Add( triangleIndex + 0 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 3 );
          triangles.Add( triangleIndex + 1 );
          triangles.Add( triangleIndex + 2 );
          triangleIndex += 4;

          Vector3 v0 = new Vector3( (float)( x ), (float)( y + 1 ), 0 );
          vertices.Add( v0 );
          normals.Add( Vector3.up );
          uvs.Add( new Vector3( v0.x / textureWidth, (v0.y-0.5f) / textureHeight, 0 ) );

          Vector3 v1 = new Vector3( (float)( x ), (float)( y + 1 ), 1 );
          vertices.Add( v1 );
          normals.Add( Vector3.up );
          uvs.Add( new Vector3( v0.x / textureWidth, (v0.y-0.5f) / textureHeight, 0 ) );

          Vector3 v2 = new Vector3( (float)( x + 1 ), (float)( y + 1 ), 1 );
          vertices.Add( v2 );
          normals.Add( Vector3.up );
          uvs.Add( new Vector3( v0.x / textureWidth, (v0.y-0.5f) / textureHeight, 0 ) );

          Vector3 v3 = new Vector3( (float)( x + 1 ), (float)( y + 1 ), 0 );
          vertices.Add( v3 );
          normals.Add( Vector3.up );
          uvs.Add( new Vector3( v0.x / textureWidth, (v0.y-0.5f) / textureHeight, 0 ) );
        }

      }
    }
    Vector3 offset = new Vector3( (float)-r.x, (float)-r.y, -0.5f );
    if( centerMesh )
      offset += new Vector3( r.width * -0.5f, r.height * -0.5f, 0f );
    for( int i = 0; i < vertices.Count; i++ )
    {
      vertices[ i ] += offset;
      Vector3 rawr = vertices[ i ] * scale;
      rawr.Scale( new Vector3(1,1,scaleZ) );
      vertices[ i ] = rawr;
      if( rotateZ != 0 )
        vertices[ i ] = Quaternion.Euler( 0, 0, rotateZ ) * vertices[ i ];
    }

    mesh.SetVertices( vertices );
    mesh.SetNormals( normals );
    mesh.SetUVs( 0, uvs );
    mesh.SetTriangles( triangles, 0 );
    mesh.UploadMeshData( true );



    string outdir = "Assets/" + outputdir;
    if( outdir.Length == 0 )
      outdir = Path.GetDirectoryName( AssetDatabase.GetAssetPath( texture ) );
    if( !Directory.Exists( outputdir ) )
      Directory.CreateDirectory( outdir );
        
    AssetDatabase.CreateAsset( mesh, outdir + "/" + resultName + ".asset" );

    Material mat = material;
    if( createMaterial && mat == null )
    {
      mat = new Material( Shader.Find( "Standard" ) );
      mat.mainTexture = texture;
      mat.SetFloat( "_SmoothnessTextureChannel", 0f );
      mat.SetFloat( "_Metallic", 0.5f );
      AssetDatabase.CreateAsset( mat, outdir + "/" + resultName + ".mat" );
    }

    GameObject go = null;
    if( prefab != null )
    {
      go = GameObject.Instantiate( prefab );
      go.name = resultName;
    }
    else
    if( createObject || createPrefab )
      go = new GameObject( resultName );
    else
      return;
    
    MeshFilter mf = go.GetComponent<MeshFilter>();
    if( mf == null )
      mf = go.AddComponent<MeshFilter>();
    mf.sharedMesh = mesh;
    MeshRenderer mr = go.GetComponent<MeshRenderer>();
    if( mr == null )
      mr = go.AddComponent<MeshRenderer>();
    mr.sharedMaterial = mat;

    if( createPrefab )
      PrefabUtility.CreatePrefab( outdir + "/" + resultName + ".prefab", go );
    if( !createObject )
      DestroyImmediate( go );
  }
}
#endif