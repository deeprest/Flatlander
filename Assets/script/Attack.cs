﻿using UnityEngine;
using System.Collections;

[SelectionBase]
public class Attack : MonoBehaviour 
{
  public enum Type
  {
    Projectile,
    Melee,
    Spawner
  }
  public Type type = Type.Projectile;
  public bool IgnoreInstigatorCollision = true;
  public float AttackDistance = 1f;
  public float StartDistance = 1;
  public bool RequireClearance = false;
  public bool AttackClosestWithinRadius = false;
  public float Timeout = 1;
  public float Force = 1;
  public ForceMode ForceMod = ForceMode.VelocityChange;
  public bool SetDrag = true;
  public float Drag = 1;
  public float Interval = 1;
  public Billboard bb;
  public float billboardSpinSpeed = 0;
  public GameObject[] SpawnerPrefabs;

  // components
  public Transform instigator;
  public AudioClip LaunchSound;
  public AnimSequence symbol;

  void Awake()
  {
    if( bb != null )
    {
      transform.rotation = bb.transform.rotation;
    }
  }



  public void FireWeapon( GameObject go, Vector3 direction, AudioSource audioSource, Transform instigator )
  {
    Attack wpn = go.GetComponent<Attack>();
    if( wpn!=null )
      wpn.instigator = instigator;

    if( IgnoreInstigatorCollision )
      Physics.IgnoreCollision( instigator.GetComponent<Collider>(), go.GetComponent<Collider>() );

    Rigidbody rb = go.GetComponent<Rigidbody>();
    if( rb != null )
    {
      rb.AddForce( direction * Force, ForceMod );
      if( SetDrag )
        rb.drag = Drag;
    }

    Timeout timeout = go.GetComponent<Timeout>();
    if( timeout != null )
    {
      timeout.duration = Timeout;
    }

    if( audioSource != null )
    {
      if( LaunchSound != null )
      {
        audioSource.PlayOneShot( LaunchSound );
      }
    }
  }
}
