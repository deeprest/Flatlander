﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class Destination : SerializedComponent, IAction, IOwnable
{
  [Serialize] public Character ClaimedBy;
  public TextMesh textMesh;


  public bool CommunalDestination = true;
  [Range(0.2f,10f)]
  [Serialize] public float ArrivalRadius = 0.5f;
  // a gather destination for objects with these tags
  public List<Tag> GatherDestinationTags;
  // can be a home for characters with these tags
  public Tag[] HomeForTags;

  public Color EditorColor = new Color(1,0,1,0.1f);

  public int GenericIntParam;
  public float GenericFloatParam;

  public override void BeforeSerialize()
  {

  }

  public override void AfterDeserialize()
  {
    if( textMesh != null )
    {
      if( ClaimedBy == null )
        textMesh.text = "(unowned)";
      else
        textMesh.text = "owned by "+ClaimedBy.CharacterName;
    }
  }

  public void OnAction( Character instigator, string action = "default" )
  {
    if( action == "claim" )
    {
      instigator.HideContextMenu();
      if( ClaimedBy == null )
      {
        Global.Instance.AssignTeam( gameObject, instigator.Team );
        AssignOwner( instigator );
      }
      else
      if( ClaimedBy == instigator )
      {
        Global.Instance.Speak( instigator, "This is mine already.", 3, 2 );
      }
      else
      {
        Global.Instance.Speak( instigator, "This is owned by " + ClaimedBy.CharacterName, 3, 2 );
      }
    }
  }

  public void GetActionContext( ref ActionData actionData )
  {
    Vector3 pos = transform.position;
    pos.y = Global.Instance.GlobalSpriteOnGroundY;
    actionData.indicator.position = pos;

    if( !CommunalDestination )
      actionData.actions.Add( "claim" );
  }


  public bool IsOwned()
  {
    return !CommunalDestination && ClaimedBy!=null;
  }

  public void ClearOwner()
  {
    if( ClaimedBy!=null )
      ClaimedBy.Home = null;
     ClaimedBy = null;
    if( textMesh != null ) 
      textMesh.text = "unowned";
  }
  public void AssignOwner( Character owner )
  {
    if( CommunalDestination )
      return;
    ClaimedBy = owner;
    if( textMesh != null )
      textMesh.text = ClaimedBy.CharacterName + "'s\n bed";
  }
}


