﻿using UnityEngine;
using System.Collections;

public class ColorPulse : MonoBehaviour 
{
	public Renderer rndr;
  public Color PulseColor = Color.red;
	public float duration = 1f;
  public float flashDuration = 0.05f;
  bool toggle;
  float startTime = 0;
  float lastToggleTime;

	void OnEnable()
	{
    if( rndr == null )
      rndr = GetComponent<Renderer>();
    
    toggle = false;
    startTime = Time.time;
    lastToggleTime = startTime;
	}

	void OnDisable()
	{
		rndr.material.color = Color.white;
	}


  void Update()
  {
    if( Time.time - startTime < duration )
    {
      if( Time.time - lastToggleTime > flashDuration )
      {
        lastToggleTime = Time.time;
        toggle = !toggle;
        if( toggle )
          rndr.material.color = PulseColor;
        else
          rndr.material.color = Color.white;
      }
    }
    else
    {
      enabled = false;
    }
	}
}
