﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShowItem
{
  public string id;
  public GameObject go;
}

public class Activator : MonoBehaviour
{
  public GameObject parent;
  public List<ShowItem> items;

  public void Activate( string id )
  {
    for( int i = 0; i < parent.transform.childCount; i++ )
      parent.transform.GetChild( i ).gameObject.SetActive( false );
    ShowItem si = items.Find( x => x.id == id );
    if( si != null )
      si.go.SetActive( true );
  }

}
