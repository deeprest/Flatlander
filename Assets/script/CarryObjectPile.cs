﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(CarryObjectPile) )]
public class CarryObjectPileEditor : Editor
{
  float cubeSize = 0.05f;
  CarryObjectPile cop;

  /*void Snap( ref Vector3 pos )
  {
    pos.x = Mathf.Floor( pos.x * ( 1.0f / cop.snap ) ) * cop.snap;
    pos.y = Mathf.Floor( pos.y * ( 1.0f / cop.snap ) ) * cop.snap;
    pos.z = Mathf.Floor( pos.z * ( 1.0f / cop.snap ) ) * cop.snap;
  }*/

  void OnSceneGUI()
  {
    cop = (CarryObjectPile)target;
    foreach( var mp in cop.mount )
    {
      Vector3 pos = cop.transform.localToWorldMatrix.MultiplyPoint( mp.pos );
      Quaternion rot = cop.transform.rotation * mp.rot;

      if( Event.current.type == EventType.Repaint )
        Handles.CubeHandleCap( cop.GetInstanceID(), pos, rot, cubeSize, EventType.Repaint );

      Quaternion newrot = Handles.RotationHandle( rot, pos );
      Vector3 newTargetPosition = Handles.PositionHandle( pos, rot );
      if( Vector3.Distance( pos, newTargetPosition ) > cop.snap || Quaternion.Angle(rot,newrot) > 1 )
      {
        mp.pos = cop.transform.worldToLocalMatrix.MultiplyPoint( newTargetPosition );
        mp.rot = Quaternion.Inverse( cop.transform.rotation ) * newrot;
        if( mp.go != null )
        {
          mp.go.transform.localPosition = mp.pos;
          mp.go.transform.localRotation = mp.rot;
        }
      }
    }
  }

  public override void OnInspectorGUI()
  {
    cop = (CarryObjectPile)target;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "toggle preview" ) )
    {
      cop.showPreview = !cop.showPreview;
      if( cop.showPreview )
      {
        for( int i = 0; i < cop.mount.Length; i++ )
        {
          MountPoint mt = cop.mount[ i ];
          if( mt.go==null )
            mt.go = GameObject.Instantiate( cop.templateObject, cop.transform.localToWorldMatrix.MultiplyPoint3x4( mt.pos ), cop.transform.rotation * mt.rot, cop.transform );
        }
      }
      else
      {
        for( int i = 0; i < cop.mount.Length; i++ )
        {
          if( cop.mount[ i ].go != null )
            DestroyImmediate( cop.mount[ i ].go );
        }
      }
    }
    if( GUI.Button( EditorGUILayout.GetControlRect(), "extract mount points from selected" ) )
    {
      foreach( var ob in Selection.gameObjects )
      {
        if( ob.transform.parent == cop.transform )
        {
          MountPoint mp = new MountPoint();
          mp.pos = ob.transform.localPosition;
          mp.rot = ob.transform.localRotation;
          ArrayUtility.Add( ref cop.mount, mp );
        }
      }
    }
    DrawDefaultInspector();
  }

}

#endif

// Supports a single prefab type, checks the incoming object's name for match with the prefab
[System.Serializable]
public class MountPoint
{
  public Vector3 pos = Vector3.zero;
  public Quaternion rot = Quaternion.identity;
  [System.NonSerialized]
  public GameObject go = null;
}

[SelectionBase]
[RequireComponent( typeof(Collider) )]
public class CarryObjectPile : SerializedComponent, IAction
{
  #if UNITY_EDITOR
  public float snap = 0.001f;
  public bool showPreview = false;
  #endif

  public CarryObject carry;
  public MountPoint[] mount;
  public GameObject templateObject;
  public GameObject spawnPrefab;
  public float spawnDistance = 0.5f;
  // prevent spawned object from triggering the collider immediately
  CarryObject last = null;
  [Header( "Serialized" )]
  [Serialize] public int count = 0;

  /*public override void BeforeSerialize()
  {
  }*/

  public override void AfterDeserialize()
  {
    for( int i = 0; i < count; i++ )
    {
      MountPoint mt = mount[ i ];
      mt.go = GameObject.Instantiate( templateObject, transform.localToWorldMatrix.MultiplyPoint3x4( mt.pos ), transform.rotation * mt.rot, transform );
    }
  }

  void OnCollisionEnter( Collision other )
  {
    // accept only the prefab
    if( other.gameObject.name != spawnPrefab.name )
      return;
    if( carry != null && carry.IsHeld )
      return;
    
    CarryObject co = other.gameObject.GetComponent<CarryObject>();
    if( co != null )
    {
      if( co == last )
      {
        last = null;
        return;
      }
      foreach( var mt in mount )
      {
        if( mt.go == null )
        {
          mt.go = GameObject.Instantiate( templateObject, transform.localToWorldMatrix.MultiplyPoint3x4( mt.pos ), transform.rotation * mt.rot, transform );
          count++;
          Destroy( co.gameObject );
          return;
        }
      }
    }
  }

  public void OnAction( Character instigator, string action = "default" )
  {
    if( action == "take one" )
    {
      // iterate backwards because it's a pile, we want the object on the top
      for( int i = 0; i < mount.Length; i++ )
      {
        MountPoint mt = mount[ mount.Length - 1 - i ];
        if( mt.go != null )
        {
          Destroy( mt.go );
          count--;

          Vector3 offset = ( instigator.moveTransform.position - transform.position ).normalized * spawnDistance;
          GameObject go = Global.Instance.Spawn( spawnPrefab, transform.position + offset, Quaternion.identity, null, true, true );

          last = go.GetComponent<CarryObject>();
          instigator.DropObject();
          instigator.HoldObject( go.transform );
          instigator.HideContextMenu();
          break;
        }
      }
    }
  }

  public void GetActionContext( ref ActionData actionData )
  {
    if( count > 0 )
      actionData.actions.Add( "take one" );
  }
}
