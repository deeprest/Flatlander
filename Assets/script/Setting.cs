﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class IntValue
{
  public string name;
  int _value;
  bool _valueInitial = true;
  public int Value
  {
    get{ return _value; }
    set
    {
      if( value != _value || _valueInitial )
      {
        _value = value;
        _valueInitial = false;
        onValueChanged( _value );
        // check for differing value to avoid infinite loop 
        updateView( _value );
      }
    }
  }

  public System.Action<int> updateView;
  public System.Action<int> onValueChanged;

  public Text labelText;
  public Text valueText;
  public Slider slider;

  public void Init()
  {
    if( labelText != null && labelText.text=="<name>" )
      labelText.text = name;

    updateView = new System.Action<int>( delegate(int value )
    {
      if( slider != null )
        slider.value = (float)value;
      if( valueText != null )
        valueText.text = value.ToString();
    } );

    if( slider != null )
    {
      slider.onValueChanged.AddListener( new UnityEngine.Events.UnityAction<float>( delegate(float value )
      {
        Value = Mathf.FloorToInt(value);
      } ) );
    }
  }
}

[System.Serializable]
public class BoolValue
{
  public string name;
  bool _value;
  bool _valueInitial = true;
  public bool Value
  {
    get{ return _value; }
    set
    {
      if( value != _value || _valueInitial )
      {
        _value = value;
        _valueInitial = false;
        onValueChanged( _value );
        // check for differing value to avoid infinite loop 
        updateView( _value );
      }
    }
  }

  public System.Action<bool> updateView;
  public System.Action<bool> onValueChanged;

  public Text labelText;
  public Text valueText;
  public Toggle toggle;

  public void Init()
  {
    if( labelText != null && labelText.text=="<name>" )
      labelText.text = name;

    updateView = new System.Action<bool>( delegate(bool value )
    {
      if( toggle != null )
        toggle.isOn = value;
      if( valueText != null )
      {
        valueText.text = value.ToString();
      }
    } );

    if( toggle != null )
    {
      toggle.onValueChanged.AddListener( new UnityEngine.Events.UnityAction<bool>( delegate(bool value )
      {
        Value = value;
      } ) );
    }
  }
}

public class Setting : MonoBehaviour
{
  public bool isInteger = false;
  public IntValue intValue;
  public bool isBool = false;
  public BoolValue boolValue;
}
