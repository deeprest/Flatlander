﻿using UnityEngine;
using UnityEditor;
using System.Collections;


public enum WorldOrientation
{
	Yup,
	Zup
}


[InitializeOnLoad]
static class EditorCamera
{
	static bool enabled = false;
	public static bool Enabled
	{
		get{ return enabled; }
		set{
			enabled = value;
			if( enabled )
				SceneView.onSceneGUIDelegate += SceneGUI;
			else
				SceneView.onSceneGUIDelegate -= SceneGUI;
		}
	}

	public static WorldOrientation orientation = WorldOrientation.Yup;
	public static float FOV = 60f;
	public static float MouseSensitivity = 0.5f;
	public static float Near = 0.001f;
	public static float Far = 100f;

	static Vector3 forward = Vector3.forward;

	static EditorCamera()
	{
//		SceneView.onSceneGUIDelegate += SceneGUI;
	}

	static void SceneGUI( SceneView sv )
	{
		if( !enabled )
			return;
			
		Vector3 WorldUp = Vector3.up;
		if( orientation == WorldOrientation.Yup )
			WorldUp = new Vector3( 0,1,0 );
		if( orientation == WorldOrientation.Zup )
			WorldUp = new Vector3( 0, 0, 1 );

		if( !sv.camera.orthographic )
			sv.camera.projectionMatrix = Matrix4x4.Perspective( FOV, sv.camera.aspect, Near, Far );

		if( Event.current.isMouse )
		{
			Vector2 mousePos = Event.current.mousePosition;
			if( Event.current.button == 1 )
			{
				if( Event.current.type == EventType.MouseDrag )
				{
					Transform cam = sv.camera.transform;
					Vector2 mouseDelta = Event.current.delta; 
					mouseDelta *= MouseSensitivity;
					forward = Quaternion.AngleAxis( mouseDelta.y, cam.right ) * Quaternion.AngleAxis( mouseDelta.x, WorldUp ) * forward;
					Vector3 up = WorldUp; //Vector3.Cross( forward.normalized, cam.right );
					/*
					if( up.z <= 0f ) // avoid jitter when hitting pitch extremes
					{
						if( forward.z >= 0f )  // up
						{
							up = Quaternion.AngleAxis( -mouseDelta.x, -WorldUp ) * Vector3.Cross( forward, cam.right );
							forward = WorldUp;
						}
						else
						{
							up = Quaternion.AngleAxis( mouseDelta.x, WorldUp ) * Vector3.Cross( forward, cam.right );
							forward = -WorldUp;
						}
					}
					else
					{
						up = WorldUp;
					}
					*/
					sv.LookAtDirect( sv.pivot, Quaternion.LookRotation( forward.normalized, up.normalized ) );
					Event.current.Use();
				}
			}
		}

	}
}


public class EditorCameraSettings : EditorWindow
{
	WorldOrientation wo = WorldOrientation.Yup;


	[MenuItem("Window/Editor Camera Settings")]
	static void Open()
	{
		EditorWindow.CreateInstance<EditorCameraSettings>().Show();
	}


	void OnGUI()
	{
		EditorCamera.Enabled = EditorGUILayout.Toggle( "Enable Settings:", EditorCamera.Enabled );
		EditorCamera.orientation = (WorldOrientation)EditorGUILayout.EnumPopup("World Orientation:", EditorCamera.orientation );

		EditorCamera.FOV = EditorGUILayout.FloatField( "FOV:", EditorCamera.FOV );
		EditorCamera.FOV = Mathf.Clamp( EditorCamera.FOV, 1f, 179f );

		EditorCamera.MouseSensitivity = EditorGUILayout.Slider( "MouseSensitivity:", EditorCamera.MouseSensitivity, 0.1f, 1f );

		if( GUI.changed )
		{
			
			SceneView.RepaintAll();
			Repaint();
		}
	}
}
