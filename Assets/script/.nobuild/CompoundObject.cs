﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

// NOTE do not use SerializedObject component on the same gameobject as CompoundObject. CompoundObject is meant to be used as a
// delivery mechanism for SerializedObjects, but not be serialized itself.

// NOTE when pressing "Update" in editor, you much deselect and re-select the asset in project view if you
// want to see an updated preview. 

//[CustomPreview(typeof(CompoundObject))]
[ExecuteInEditMode]
public class CompoundObject : MonoBehaviour 
{
  // set this in script defaults
  public GameData gameData;
  public bool ReplaceChildrenOnAwake = true;
  // Instantiate this objects children from Resource prefabs (by name).
  // This script should not be on a serialized object because of possible race condition
  // between Awake() and World.Awake() which loads resources that this script will use.
  void Awake()
  {
    if( ReplaceChildrenOnAwake )
      ReplaceChildren();
  }

  public void ReplaceChildren()
  {
    if( Application.isPlaying )
    {
      SerializedObject[] sos = GetComponentsInChildren<SerializedObject>();
      foreach( var so in sos )
      {
        string resourceName = so.name;
        GameObject go = Global.Instance.Spawn( resourceName, so.transform.position, so.transform.rotation, transform, true, false );
        if( go != null )
          GameObject.Destroy( so.gameObject );
      }
    }
    else
    {
      #if UNITY_EDITOR

      Undo.RecordObject( gameObject, "Update Compound Object" );

      if( PrefabUtility.GetPrefabType( gameObject ) == PrefabType.Prefab )
      {
        // Replacing assets requires destroying the old game object using GameObject.Destroy().
        // This is prohibited by the Unity editor. 
        // Instead of destroying each sub object, the prefab is replaced in it's entirety using PrefabUtility.
        // Instantiating the prefab here effectively calls the block below, to initialize.
        GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab( gameObject );
        PrefabUtility.ReplacePrefab( obj, gameObject );
        GameObject.DestroyImmediate( obj );
        // there is apparently no easy way to repaint the preview window (inspector panel).
      }
      else
      {
        if( gameData==null )
        {
          if( Application.isPlaying )
            gameData = Global.Instance.gameData;
          else
            gameData = Resources.FindObjectsOfTypeAll<GameData>()[0];
        }
        SerializedObject[] sos = GetComponentsInChildren<SerializedObject>();
        foreach( var so in sos )
        {
          if( so==null )
            continue;
          string resourceName = so.name;
          ObjectReplacement rep = gameData.objectReplacementList.Find( x=>x.oldKey==resourceName );
          if( rep!=null )
            resourceName = rep.newKey;

          GameObject resourcePrefab = (GameObject)Resources.Load( "serialized/"+resourceName );
          if( resourcePrefab != null )
          {
            GameObject go = GameObject.Instantiate( resourcePrefab, so.transform.position, so.transform.rotation, transform );
            go.name = resourceName;
          }
          else
          {
            Debug.LogError( "resource "+ resourceName +" not found", gameObject );
          }
          GameObject.DestroyImmediate( so.gameObject );
        }
      }

      #endif
    }
  }
}
  

