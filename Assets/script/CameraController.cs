﻿//#define ENDLESS_WORLD
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Profiling;
using System.Collections;
using System.Collections.Generic;

public class CameraSettings
{
  public float MinDistance = 1f;
}

public class CameraController : MonoBehaviour
{
  //  public bool useExtraCameras = true;
  public Image FullScreenQuad;
  public Camera cam;

  #if ENDLESS_WORLD
  public Camera[] extraCameras;
  public Camera skyCamera;
  public Vector3[] offset;
  #endif

 

  public void AssignSettings( CameraSettings s )
  {
    MinDistance = s.MinDistance;
  }

  public Transform LookTarget;
  public Vector3 FreePosition;
  public Vector3 FreeLookAngles;
  public LerpToTarget Lerp;
  public float Distance = 1.3f;
  public float MinDistance = 0.5f;
  public float RaycastStartOffset = 0f;
  public float cameraHeightMax = 20f;
  public float cameraHeightCeiling = 1f;
  public float cameraHeightMin = 4f;
  public float pitchAngleMin = 8f;
  public float pitchAngleMax = 80f;
  public float cameraLookSpeed = 2;
  public float cameraMoveSpeed = 1;
  public float dollyOutSpeed = 10;
  public float heightSpeed = 10f;
  float cameraHeightTarget;
  float lastCameraHeightUpdate;
  public float heightUpdateInterval = .1f;
  public float hitNormalLength = 0.1f;
  public float hitRadius = 0.1f;

  public bool Automated{ get; set; }

  public float normPitch;

  public bool LookControlEnabled = true;
  public Vector2 lookInput;
  public CursorLockMode cursorlockmode;
  public float hor;

  public AnimationCurve heightCeilingCurve;
  public AnimationCurve heightLookCurve;

  public bool EnableHide
  {
    set
    { 
      if( !value )
        UnhideRenderers();
      enableHide = value;
    }
  }

  public bool enableHide = true;
  public float cameraHideRendererExtent = 3;
  public float cameraHideRendererMinAngle = 80;
  public float cameraHideRendererMinHeight = 5;
  List<MeshRenderer> hidden = new List<MeshRenderer>();
  List<MeshRenderer> removeFromHidden = new List<MeshRenderer>();
  Collider[] cameraHideColliders = new Collider[200];



  void Awake()
  {
    FullScreenQuad.color = Color.clear;
    cameraHeightTarget = cameraHeightMax;

    #if ENDLESS_WORLD
    if( Global.Instance.EndlessWorld )
      cam.clearFlags = CameraClearFlags.Depth;
    else
      cam.clearFlags = CameraClearFlags.Skybox;

    skyCamera.gameObject.SetActive( World.Instance.EndlessWorld );
    foreach( var c in extraCameras )
      c.gameObject.SetActive( World.Instance.EndlessWorld );
    #endif

    FreePosition = new Vector3( 0.5f * Zone.BlockSize, 40, 0.5f * Zone.BlockSize );
    //lookInput = new Vector2( 0, 15 );
  }

  #if ENDLESS_WORLD
  public void SetOffsets( Vector2 wo )
  {
    offset = new Vector3[8];
    offset[ 0 ] = new Vector3( -wo.x, 0, 0 );
    offset[ 1 ] = new Vector3( wo.x, 0, 0 );
    offset[ 2 ] = new Vector3( 0, 0, -wo.y );
    offset[ 3 ] = new Vector3( 0, 0, wo.y );
    // indices 4 and above are corner cameras
    // this is only important because their camera depth must be lowest
    offset[ 4 ] = new Vector3( -wo.x, 0, -wo.y );
    offset[ 5 ] = new Vector3( -wo.x, 0, wo.y );
    offset[ 6 ] = new Vector3( wo.x, 0, wo.y );
    offset[ 7 ] = new Vector3( wo.x, 0, -wo.y );
  }


  List<int> DetermineCamerasToEnable( Vector3 pos )
  {
    // make this slightly larger than view distance to avoid popping
    float borderDistance = cam.farClipPlane + 10f;
    // this depends on the indices set for the camera offsets
    List<int> indices = new List<int>();
    if( Mathf.Abs( World.Instance.WorldOffset.x - pos.x ) < borderDistance ) indices.Add( 0 );
    if( Mathf.Abs( 0 - pos.x ) < borderDistance ) indices.Add( 1 );
    if( Mathf.Abs( World.Instance.WorldOffset.y - pos.z ) < borderDistance ) indices.Add( 2 );
    if( Mathf.Abs( 0 - pos.z ) < borderDistance ) indices.Add( 3 );
    // add corners 
    if( indices.Contains( 1 ) && indices.Contains( 3 ) ) indices.Add( 6 );
    if( indices.Contains( 1 ) && indices.Contains( 2 ) ) indices.Add( 7 );
    if( indices.Contains( 0 ) && indices.Contains( 3 ) ) indices.Add( 5 );
    if( indices.Contains( 0 ) && indices.Contains( 2 ) ) indices.Add( 4 );
    return indices;
  }
  #endif
    

  public void FreelookMode()
  {
    LookTarget = null;
    lookInput = new Vector2( FreeLookAngles.y, FreeLookAngles.x );
  }

  public void AssignLookTarget( Transform target )
  {
    LookTarget = target;
  }

  public void LerpTo( Transform target )
  {
    Lerp.targetTransform = target;
    Lerp.localOffset = Vector3.zero;
    Lerp.lerpType = LerpToTarget.LerpType.Curve;
    Lerp.duration = 1;
    Lerp.LerpRotation = true;
    Lerp.OnLerpEnd = null;
    Lerp.enabled = true;
  }

  public void LerpTo( Vector3 targetPositionWorld, Vector3 forward, Vector3 up, float duration = 1 )
  {
    Lerp.WorldTarget = true;
    Lerp.targetPositionWorld = targetPositionWorld;
    Lerp.targetRotationForward = forward;
    Lerp.targetRotationUp = up;
    Lerp.localOffset = Vector3.zero;
    Lerp.lerpType = LerpToTarget.LerpType.Curve;
    Lerp.duration = duration;
    Lerp.LerpRotation = true;
    Lerp.OnLerpEnd = null;
    Lerp.enabled = true;
  }

  // mitigate a Unity input bug
  public const int delayreset = 1;
  public int delay{ get; set; }
  const float killThreshold = 10;

  void Update()
  {
    Vector2 lookDelta = Vector2.zero;

    cursorlockmode = Cursor.lockState;
    //Debug.Log( Input.GetAxis( "LookHorizontal" ) );

    if( LookControlEnabled )
    {
      if( Cursor.lockState == CursorLockMode.Locked )
      {
        // NOTE This hack is necessary because Input.GetAxis() provides extreme values 
        // for the mouse immediately after the cursor is locked (a bug in the Unity input system). 
        // Input.ResetInputAxes() does nothing to help.
        float x = Input.GetAxis( "LookHorizontal" );
        float y = Input.GetAxis( "LookVertical" );
        if( delay > 0 )
        {
          if( Mathf.Abs( x ) > killThreshold || Mathf.Abs( y ) > killThreshold )
          {
            delay--;
            x = 0;
            y = 0;
          }
        }
        lookDelta.x = x * cameraLookSpeed * Time.deltaTime;
        lookDelta.y = y * cameraLookSpeed * Time.deltaTime;
      }
    }

    if( Automated )
    {
      // do nothing
    }
    else
    if( LookTarget == null )
    {
      // input
      lookInput += lookDelta;
      lookInput.y = Mathf.Clamp( lookInput.y, -90f, 90f );
        lookInput.x = Util.NormalizeAngle( lookInput.x );
      // rotation
      Vector3 eulerAngles = Vector3.zero;
      eulerAngles.y = lookInput.x;
      eulerAngles.x = lookInput.y;
      FreeLookAngles = eulerAngles;
      transform.rotation = Quaternion.Euler( eulerAngles );
      // pos
      float speed = cameraMoveSpeed;
      if( Input.GetButton( "Sprint" ) )
        speed *= 4f;
      FreePosition += transform.forward * ( Input.GetAxis( "Vertical" ) * speed * Time.smoothDeltaTime );
      FreePosition += transform.right * ( Input.GetAxis( "Horizontal" ) * speed * Time.smoothDeltaTime );
      transform.position = FreePosition;
    }
    else
    {
      if( Time.time - lastCameraHeightUpdate > heightUpdateInterval )
      {
        lastCameraHeightUpdate = Time.time;
       
        bool found = false;
        RaycastHit hit = new RaycastHit();
        //Debug.DrawLine( lookTarget.position, lookTarget.position + Vector3.up * 20f );
        if( Physics.Raycast( LookTarget.position + ( Vector3.up * cameraHeightMax ), Vector3.down, out hit, cameraHeightMax, Physics.AllLayers/*LayerMask.NameToLayer( "CameraMesh" )*/ ) )
        {
          if( hit.transform.tag == "CameraTrigger" )
          {
            found = true;
            cameraHeightTarget = hit.point.y;
          }
        }
        if( !found )
        {
          cameraHeightTarget = cameraHeightMax;
        }
      }

      // limit the normalized input into height ceiling curve
      float maxDist = 10f;
      float speed = heightSpeed * heightCeilingCurve.Evaluate( Mathf.Min( maxDist, Mathf.Abs( cameraHeightCeiling - cameraHeightTarget ) ) / maxDist );
      cameraHeightCeiling = Mathf.MoveTowards( cameraHeightCeiling, cameraHeightTarget, speed * Time.deltaTime );

      lookInput += lookDelta;
      lookInput.y = Mathf.Clamp( lookInput.y, pitchAngleMin, pitchAngleMax );
      lookInput.x = Util.NormalizeAngle( lookInput.x );
      Vector3 eulerAngles = Vector3.zero;
      eulerAngles.y = lookInput.x;
      eulerAngles.x = lookInput.y;
      transform.rotation = Quaternion.Euler( eulerAngles );

      Vector3 lookPos = LookTarget.position;
      lookPos += new Vector3( 0, RaycastStartOffset, 0 );

      // use normalized pitch...
      float normPitch = ( eulerAngles.x - pitchAngleMin ) / ( pitchAngleMax - pitchAngleMin );
      Vector3 desiredPosition = lookPos - GetForwardDirection() * Distance * ( 1f - normPitch );
      // to determine normalized height, then evaluate with height curve
      desiredPosition.y = lookPos.y + cameraHeightMin + heightLookCurve.Evaluate( normPitch ) * ( cameraHeightCeiling - cameraHeightMin );
      Vector3 delta = desiredPosition - lookPos;
      float distance = delta.magnitude;

      if( enableHide )
      {
        UnhideRenderers();

        if( transform.rotation.eulerAngles.x > cameraHideRendererMinAngle || desiredPosition.y > cameraHideRendererMinHeight )
        { 
          //Camera.main.cullingMask &= ~LayerMask.GetMask( new string[]{"CameraHide"});
          int count = Physics.OverlapBoxNonAlloc( lookPos, Vector3.one * cameraHideRendererExtent, cameraHideColliders,
                        Quaternion.identity, LayerMask.GetMask( new string[]{ "CameraHide" } ),
                        QueryTriggerInteraction.Collide );
          if( count > 0 )
          {
            int i = 0;
            foreach( var c in cameraHideColliders )
            {
              if( i++ < count )
              {
                //              if( c.gameObject.layer == LayerMask.NameToLayer( "CameraHide" ) )
                {
                  MeshRenderer mr = c.gameObject.GetComponent<MeshRenderer>();
                  if( mr )
                  {
                    mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                    if( !hidden.Contains( mr ) )
                      hidden.Add( mr );
                  }
                }
              }
              else
              {
                break;
              }
            }
          }
        }
        else
        { 
          RaycastHit hit = new RaycastHit();
          if( Physics.SphereCast( lookPos, hitRadius, delta, out hit, Distance, 
                ~LayerMask.GetMask( new string[] {
              "Camera",
              "AudioTrigger",
              "CameraHide",
              "Player",
              "Ignore Raycast",
              "Character",
              "Carry",
              "Projectile",
              "Opaque"
            } ) ) )
          {
            Vector3 pos = hit.point + hit.normal * hitNormalLength;
            if( ( pos - lookPos ).magnitude < delta.magnitude )
            {
              // instant in
              distance = ( pos - lookPos ).magnitude;
            }
          }
          else
          {
            // fast out
            if( Vector3.Distance( transform.position, lookPos ) < delta.magnitude )
              distance = Mathf.MoveTowards( Vector3.Distance( transform.position, lookPos ), delta.magnitude, Time.deltaTime * dollyOutSpeed );
            distance = Mathf.Clamp( distance, MinDistance, Vector3.Distance( desiredPosition, lookPos ) );
          }
        }
      }

      transform.position = lookPos + ( delta.normalized * distance );
    }

    #if ENDLESS_WORLD
    if( World.Instance.EndlessWorld )
    {
      int index = 0;
      foreach( var ec in extraCameras )
      {
        ec.transform.position = transform.position + offset[ index ];
        ec.transform.rotation = transform.rotation; 
        index++;
      }
      List<int> cams = DetermineCamerasToEnable( transform.position );
      for( int i = 0; i < extraCameras.Length; i++ )
      {
        if( cams.Contains( i ) )
        {
          // indices 4 and above are corner cameras. the corner camera depth must be lowest
          if( i > 3 )
            extraCameras[ i ].depth = 0;
          extraCameras[ i ].gameObject.SetActive( true );
        }
        else
        {
          extraCameras[ i ].gameObject.SetActive( false );
        }
      }
    }
    #endif
  }

 
  void UnhideRenderers()
  {
    foreach( var mr in hidden )
    {
      if( mr == null )
        removeFromHidden.Add( mr );
      else
        mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
    }
    foreach( var mr in removeFromHidden )
    {
      hidden.Remove( mr );
    }
    removeFromHidden.Clear();
  }

  public Vector3 GetForwardDirection()
  {
    Vector3 forward = transform.forward;
    if( Vector3.Angle( forward, Vector3.down ) < 30.0f )
      forward = transform.up;
    forward.y = 0;
    forward.Normalize();
    return forward;
  }

  public void ScreenFadeColor( Color color )
  {
    FullScreenQuad.color = color;
  }

  public void SetFarPlane( float far )
  {
    cam.farClipPlane = far;
    #if ENDLESS_WORLD
    skyCamera.farClipPlane = far;
    foreach( var c in extraCameras )
    {
      c.farClipPlane = far;
    }
    #endif
  }

  public CameraShake shaker;

  public void Shake()
  {
    // restarts the shake if already active
    shaker.enabled = false;
    shaker.enabled = true;
  }
}
