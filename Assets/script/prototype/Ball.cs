﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
[RequireComponent( typeof(SphereCollider) )]
public class Ball : MonoBehaviour
{
  public bool LockToGround = true;
  SphereCollider sphere;
  public SphereCollider Sphere
  {
    get{
      if( sphere==null )
        sphere = GetComponent<SphereCollider>();
      return sphere;
    }
  }

  void Start()
  {
    Rigidbody body = GetComponent<Rigidbody>();
		if( body != null )
		{
      body.constraints |= RigidbodyConstraints.FreezePositionY;
//      body.freezeRotation = true;
		}
	}

	void Update()
	{
    if( LockToGround )
    {
      if( Sphere != null )
      {
        Vector3 pos = transform.position;
        pos.y = Sphere.radius;
        transform.position = pos;
      }

		}
  
	}

}

/*
#if UNITY_EDITOR
[CustomEditor( typeof(Ball) )]
[CanEditMultipleObjects()]
public class BallSceneEditor : Editor
{
	void OnSceneGUI()
	{
		Ball obj = target as Ball;
		if( obj == null )
			return;

		if( obj.LockToGroud )
		{
			SphereCollider sphere = obj.GetComponent<SphereCollider>();
			if( sphere != null )
			{
				Vector3 pos = obj.transform.position;
				pos.y = sphere.radius;
				obj.transform.position = pos;
			}

		}
	}
}

#endif
*/