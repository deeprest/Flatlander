﻿//#define ENDLESS_WORLD

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBoundMaster : MonoBehaviour 
{
  public bool useSurrogates = false;
  public GameObject surrogatePrefab;
  const int surrogateCount = 8;
  const float borderDistance = 10;
  WorldBoundSurrogate[] surrogate = new WorldBoundSurrogate[surrogateCount];
//  public System.Action OnSeamCrossed;

  void Start()
  {
    if( useSurrogates )
    {
      for( int i = 0; i < surrogateCount; i++ )
      {
        GameObject go = GameObject.Instantiate( surrogatePrefab, transform );
        WorldBoundSurrogate w = go.GetComponent<WorldBoundSurrogate>();
        surrogate[ i ] = w;
        //w.gameObject.tag = gameObject.tag;
        w.gameObject.layer = gameObject.layer;

        Tags myTags = GetComponent<Tags>();
        if( myTags!=null )
        {
          Tags surrogateTags = go.GetComponent<Tags>();
          if( surrogateTags!=null )
          {
            surrogateTags.tags = myTags.tags;
          }
        }
      }
    }
  }
    
  List<int> DetermineSurrogatesToEnable( Vector3 pos )
  {
    // this depends on the indices set for the camera offsets
    List<int> indices = new List<int>();
    if( Mathf.Abs( Global.Instance.WorldOffset.x - pos.x ) < borderDistance ) indices.Add( 0 );
    if( Mathf.Abs( 0 - pos.x ) < borderDistance ) indices.Add( 1 );
    if( Mathf.Abs( Global.Instance.WorldOffset.y - pos.z ) < borderDistance ) indices.Add( 2 );
    if( Mathf.Abs( 0 - pos.z ) < borderDistance ) indices.Add( 3 );
    // add corners 
    if( indices.Contains( 1 ) && indices.Contains( 3 ) ) indices.Add( 6 );
    if( indices.Contains( 1 ) && indices.Contains( 2 ) ) indices.Add( 7 );
    if( indices.Contains( 0 ) && indices.Contains( 3 ) ) indices.Add( 5 );
    if( indices.Contains( 0 ) && indices.Contains( 2 ) ) indices.Add( 4 );
    return indices;
  }
    
	public void Update () 
  {
    Vector3 pos = transform.position;

    pos.x = Mathf.Clamp( pos.x, 0, Global.Instance.WorldOffset.x );
    pos.z = Mathf.Clamp( pos.z, 0, Global.Instance.WorldOffset.y );

    // endless world
//    pos.x = Mathf.Repeat( pos.x, World.Instance.WorldOffset.x );
//    pos.z = Mathf.Repeat( pos.z, World.Instance.WorldOffset.y );

//    if( pos != transform.position )
//      OnSeamCrossed.Invoke();

    transform.position = pos;

    #if ENDLESS_WORLD
    if( useSurrogates )
    {
      int index = 0;
      foreach( var sg in surrogate )
      {
        sg.transform.position = transform.position + World.Instance.cameraController.offset[ index ];
        sg.transform.rotation = transform.rotation; 
        index++;
      }
    
      List<int> objs = DetermineSurrogatesToEnable( transform.position );
      for( int i = 0; i < surrogate.Length; i++ )
      {
        if( objs.Contains( i ) )
        {
          surrogate[ i ].gameObject.SetActive( true );
        }
        else
        {
          surrogate[ i ].gameObject.SetActive( false );
        }
      }
    }
    #endif
	}

  void OnDestroy()
  {
    if( !Global.IsQuiting )
    {
      if( useSurrogates )
      {
        foreach( var sg in surrogate )
          GameObject.Destroy( sg.gameObject );
      }
    }
  }

  // play sound closest to the active audio listener
//  public PlaySound()
}
