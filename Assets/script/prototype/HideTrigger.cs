﻿using UnityEngine;
using System.Collections;

public class HideTrigger : MonoBehaviour
{
  void OnTriggerEnter( Collider other )
  {
    Character c = other.GetComponent<Character>();
    if( c != null )
      c.IsHidden = true;
  }

  void OnTriggerExit( Collider other )
  {
    Character c = other.GetComponent<Character>();
    if( c != null )
      c.IsHidden = false;
  }
}

