﻿using UnityEngine;
using System.Collections;

public class Reflect : MonoBehaviour
{
  public Tag[] reflectTags;
  public ForceMode forceMode;
  public float Intensity = 2f;

  void OnCollisionEnter( Collision collision )
  {
    if( collision.rigidbody == null )
      return;

    if( reflectTags.Length == 0 )
    {
      collision.rigidbody.AddForce( -(collision.rigidbody.velocity).normalized * Intensity, forceMode );
    }
    else
    {
      Tags tags = collision.gameObject.GetComponent<Tags>();
      if( tags != null )
      {
        foreach( var tag in reflectTags )
        {
          if( tags.HasTag( tag ) )
          {
            collision.rigidbody.AddForce( -(collision.rigidbody.velocity).normalized * Intensity, forceMode );
            return;
          }	
        }
      }
    }
  }

}
