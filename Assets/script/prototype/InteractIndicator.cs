﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractIndicator : MonoBehaviour 
{
  public float speed = 90f;
	
  void OnEnable()
  {
//    Debug.Log( "ind enable" );
  }
  void OnDisable()
  {
//    Debug.Log( "ind dissable" );
  }
	void Update () 
  {
    transform.Rotate( Vector3.up, speed * Time.deltaTime, Space.Self );
	}
}
