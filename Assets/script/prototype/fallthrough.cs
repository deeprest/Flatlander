﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallthrough : MonoBehaviour {

  void OnTriggerEnter( Collider c )
  {
    Debug.LogWarning( "object fell through ground: " + c.gameObject.name );
    Vector3 pos = c.transform.position;
    pos.y = 1;
    c.transform.position = pos;
  }
}
