﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO use blend shapes
public class Destructible : SerializedComponent, IDamage
{
  [Serialize] 
  public float Health = 3;
  public float HealthRegen = 0;
  [Serialize] 
  public string meshName;

  public MeshFilter mf;
  public List<Mesh> meshes = new List<Mesh>();
  Transform instigator;

  public List<DamageType> DamageTypes = new List<DamageType>(){ DamageType.Generic };

  [System.Serializable]
  public class HealthThreshold
  {
    public float Health;
    //public DamageType DamageType;
    public UnityEngine.Events.UnityEvent WhenReached;
  }
  public HealthThreshold[] thresholds;
  public float SpawnObjectRadius = 0.5f;


  void Awake()
  {
    if( mf==null )
      mf = GetComponent<MeshFilter>();
  }

  string SanitizeName( string name )
  {
    return name.Split( new char[]{ ' ' } )[ 0 ];
  }

  Mesh FindMesh( string name )
  {
    string sanitizedName = SanitizeName( name );
    Mesh returnMesh = meshes.Find( x => x.name == sanitizedName );
    if( returnMesh == null )
      return meshes[ 0 ];
    return returnMesh;
  }

  public override void BeforeSerialize()
  {
    if( Application.isEditor && !Application.isPlaying )
      return;
    if( mf!=null )
      meshName = SanitizeName( mf.mesh.name );
  }

  public override void AfterDeserialize()
  {
    if( Application.isEditor && !Application.isPlaying )
      return;
    if( mf!=null && meshes.Count > 0 )
    {
      if( mf.mesh.name != SanitizeName( meshName ) )
      {
        mf.mesh = FindMesh( meshName );
      }
    }
  }

  public void TakeDamage( Damage d )
  {
    if( !DamageTypes.Contains( d.type ) )
      return;
    
    Health = Mathf.Max( Health - d.amount, 0 );
    float lowestHealth = int.MaxValue;
    HealthThreshold found = null;
    foreach( var th in thresholds )
    {
      //if( th.DamageType == d.type )
      {
        if( Health <= th.Health && th.Health < lowestHealth )
        {
          lowestHealth = th.Health;
          found = th;
        }
      }
    }
    instigator = d.instigator;
    if( found != null )
    {
      if( found.WhenReached != null )
        found.WhenReached.Invoke();
    }
  }
    

  public void ReplaceWithPrefab( GameObject prefab )
  {
    Debug.LogWarning( "ReplaceWithPrefab needs implementation" );
    //World.Instance.MarkNavMeshDirty();
  }

  public void ReplaceMesh( Mesh mesh )
  {
    mf.mesh = mesh; 
    meshName = SanitizeName( mesh.name );
  }
    

  public void SpawnEffect( GameObject prefab )
  {
    Quaternion rot = Quaternion.identity;
    Vector3 pos = transform.position;
    GameObject go = Instantiate( prefab, pos, rot );
    go.name = prefab.name;
  }
    
  public void SpawnNearby( GameObject prefab )
  {
    Vector3 delta = (instigator.position - transform.position).normalized * Mathf.Max(0.001f,SpawnObjectRadius);
    Vector3 pos = transform.position + delta;
    pos.y = 1;
    GameObject go = Global.Instance.Spawn( prefab, pos, Quaternion.identity, null );
    if( go != null )
    {
      CarryObject swag = go.GetComponent<CarryObject>();
      if( swag != null )
      {
        go.transform.rotation = Quaternion.LookRotation( delta, Vector3.up ) * Quaternion.LookRotation( swag.GroundForward, swag.GroundUp );
      }
    }
  }

  public void DestroyThisGameObject()
  {
    GameObject.Destroy( gameObject );
  }
}
