﻿using UnityEngine;
using System.Collections;

public class AudioTrigger : MonoBehaviour
{

  /*void OnCollisionEnter( Collision other )
  {
    if( other.gameObject == Global.Instance.cameraController.gameObject )
    {
      Global.Instance.GoInside();
    }
  }

  void OnCollisionExit( Collision other )
  {
    if( other.gameObject == Global.Instance.cameraController.gameObject )
    {
      Global.Instance.GoOutside();
    }
  }*/

  void OnTriggerEnter( Collider other )
  {
    if( other.gameObject == Global.Instance.cameraController.gameObject )
    {
      Global.Instance.AudioGoInside();
    }
  }

  void OnTriggerExit( Collider other )
  {
    if( other.gameObject == Global.Instance.cameraController.gameObject )
    {
      Global.Instance.AudioGoOutside();
    }
  }
}

