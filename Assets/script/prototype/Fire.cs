﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
  public GameObject FirePrefab;
  public ParticleSystem timeoutSystem;
  public float PropagateInterval = 3;
  public float PropagateRadius = 1;
  public float Timeout = 10;
  float timeMark;


  void OnEnable()
  {
    timeMark = Time.time;
  }


  void Update()
  {
    float duration = Time.time - timeMark;
    if( duration > PropagateInterval)
    {
      timeMark = Time.time;
      Damage dinfo = new Damage( transform, DamageType.Fire, 1 );
      Collider[] cds = Physics.OverlapBox( transform.position, new Vector3(PropagateRadius,2,PropagateRadius), Quaternion.identity, Physics.AllLayers, QueryTriggerInteraction.Collide );
      foreach( var cd in cds )
      {
        SerializedObject so = cd.transform.root.GetComponentInChildren<SerializedObject>();
        if( so == null || so.gameObject == gameObject )
          continue;
        
        IDamage[] dms = so.gameObject.GetComponentsInChildren<IDamage>();
        foreach( var dam in dms )
          dam.TakeDamage( dinfo );
        
        Tags[] tags = so.gameObject.GetComponentsInChildren<Tags>();
        foreach( var t in tags )
        {
          if( t.HasTag( Tag.Combustible ) )
          {
            Vector3 offset = Vector3.zero;
            BoxCollider cld = t.gameObject.GetComponent<BoxCollider>();
            if( cld != null )
              offset = t.transform.rotation * cld.center;
            GameObject.Instantiate( FirePrefab, cd.transform.position + offset, Quaternion.identity, so.gameObject.transform );
          }
        }
      } 
    }
    if( timeoutSystem != null && !timeoutSystem.isPlaying )
      GameObject.Destroy( gameObject );
  }

}