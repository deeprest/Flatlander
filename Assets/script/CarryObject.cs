﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LerpToTarget))]
public class CarryObject : MonoBehaviour, IAction, ILimit
{
  public bool IsUnderLimit(){ return Limit.IsUnderLimit(); }
  public static Limit<CarryObject> Limit = new Limit<CarryObject>();

  public InventoryItem Item;
  public LerpToTarget lerp;

  // prevent multiple characters from picking up the same item.
  public bool IsHeld = false;
  public Character HeldByCharacter = null;
  public bool PreserveConstraintsWhenDropped = true;
  public bool CreateRigidbodyWhenCarried = false;
  public RigidbodyConstraints cachedConstraints;

//  public Vector3 HeldForward = Vector3.forward;
//  public Vector3 HeldUp = Vector3.up;
  public Vector3 GroundForward = Vector3.forward;
  public Vector3 GroundUp = Vector3.up;


//  [Header("When Killed")]
//  [Tooltip("When this is set, destroy this gameobject and play pop effect when triggered")]
//  public ParticleSystem popEffect;
//  public SpriteRenderer spriteRenderer;


  public Vector3 EquippedOffset = Vector3.zero;
  public Vector3 EquippedForward = Vector3.forward;
  public Vector3 EquippedUp = Vector3.up;


  void Awake()
  {
    Limit.OnCreate( this );
  }

  void OnDestroy()
  {
    Limit.OnDestroy( this );
  }
  /*
  public void Kill()
  {
    float deathklok = 0f;
    if( popEffect != null )
    {
      popEffect.Play();
      deathklok = popEffect.main.duration;
    }

    GameObject.Destroy( gameObject, deathklok );

    if( spriteRenderer != null )
      spriteRenderer.enabled = false;
  }
*/
  public void OnAction( Character instigator, string action = "default" )
  {
    if( action == "carry" )
    {
      instigator.HideContextMenu();
      instigator.HoldObject( transform );
    }
    if( action == "keep" )
    {
      instigator.HideContextMenu();
      instigator.AcquireObject( gameObject );
    }
  }

  public void GetActionContext( ref ActionData actionData )
  {

  }
}
