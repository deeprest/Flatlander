﻿using UnityEngine;
using System.Collections;


public class InventoryItem : MonoBehaviour
{
  public string Type;
  public Vector3 InInventoryForward = Vector3.forward;
  public Vector3 InInventoryUp = Vector3.up;
  public CarryObject CarryObjectPrefab;
  public Attack Weapon;
  public Shield Shield;

}

