﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ContextAction
{
  public string id;
  public Sprite sprite;
}

public class ContextMenu : MonoBehaviour 
{
  public Transform Mount;
  public GameObject ActionPrefab;
  public List<ContextAction> Action;
  public RectTransform[] options;
  public float offset = 0.3f;
  public float spacing = 0.5f;

  public List<ContextAction> CurrentActions = new List<ContextAction>();

  Dictionary<string,string> labels = new Dictionary<string, string>();
  void Awake()
  {
    labels.Add( "mode build", "Build" );
    labels.Add( "build quit", "Quit Build Mode" );
    labels.Add( "build build", "Build" );
    labels.Add( "build modify", "Modify" );
    labels.Add( "build block", "Block" );

    labels.Add( "command", "Command" );
    labels.Add( "command follow", "Follow Me" );
    labels.Add( "command unfollow", "Dismiss" );
    labels.Add( "command team join", "Join Team" );

    labels.Add( "follow all", "Follow Me!" );
    labels.Add( "unfollow all", "Dismissed!" );

    labels.Add( "romance", "Romance" );
    labels.Add( "generate name", "Generate New Name" );

    labels.Add( "job soldier", "Soldier" );
    labels.Add( "job civilian", "Civilian" );
    labels.Add( "show more jobs", "More Options" );
  }

  public void Show( string[] actions )
  {
    gameObject.SetActive( true );
    for( int i = 0; i < Mount.childCount; i++ )
    {
      Transform tf = Mount.GetChild( i );
      if( tf.childCount > 0 )
        GameObject.Destroy( tf.GetChild( 0 ).gameObject );
    }
    CurrentActions.Clear();
    int idx = 0;
    foreach( string name in actions )
    {
      ContextAction action = Action.Find( x => x.id == name );
      if( action == null )
      {
        //Debug.LogWarning( "Action not found: " + name );
        action = new ContextAction();
        action.id = name;
      }

      CurrentActions.Add( action );
      RectTransform mount = options[ idx++ ];
      mount.gameObject.SetActive( true );
      mount.anchoredPosition = Vector2.zero;
      GameObject go = GameObject.Instantiate( ActionPrefab, mount );
      Text label = go.GetComponentInChildren<Text>();
      string text = action.id;
      if( labels.ContainsKey( action.id ) )
        text = labels[ action.id ];
      label.text = text;
      //Image image = go.GetComponentInChildren<Image>();
      //image.sprite = action.sprite;
    }
  }

  public void Hide()
  {
    gameObject.SetActive( false );
  }

}
