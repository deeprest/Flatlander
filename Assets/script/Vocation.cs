﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Vocation : ScriptableObject 
{
  public List<Character.CharacterStateParameters> StateParameters;
  public List<Character.Interest> Interests;
}