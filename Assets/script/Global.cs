﻿#define BOX_COLLIDERS

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
//using System.IO.Compression;
using Ionic.Zip;
using LitJson;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.Profiling;
using UnityEngine.Networking;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif


public partial class Global : MonoBehaviour //NetworkManager
{
  static Global instance;

  public static Global Instance
  {
    get
    {
      if( instance == null )
      {
        instance = FindObjectOfType<Global>();
      }
      return instance;
    }
  }

  public Vector2 WorldOffset
  {
    get
    {
      return new Vector2( (float)Zone.BlockSize, (float)Zone.BlockSize );
    }
  }

  string worldName;

  public string WorldName
  {
    get{ return worldName; }
    set
    { 
      worldName = value;
      txtLevelName.text = "Current World: "+value;
    }
  }

  public static string[] persistentFilenames = new string[] {
    "settings.json",
    "characters.json",
    "events.json"
  };

  string settingsPath { get { return Application.persistentDataPath + "/" + "settings.json"; } }

  string characterDataPath{ get { return Application.persistentDataPath + "/" + "characters.json"; } }

  string characterEventsPath{ get { return Application.persistentDataPath + "/" + "events.json"; } }

  [Header( "Global Settings" )]
  public string InitializeWorldName;
  // all visibility raycasts are done on the same plane for this project
  public float GlobalRaycastY = 0.1f;
  public float GlobalSpriteOnGroundY = 0.02f;

  public bool Footprints = true;
  public bool Bloodtrails = true;
  public int CharacterUpdateEveryNthFrame = 1;
  public int CharacterUpdateCallsPerUpdate = 1;
  int CharacterUpdateFrameCounter = 0;
  int CharacterUpdateID = 0;

  public float WallBounce = 0.3f;
  public float RepathInterval = 1;

  public bool GlobalSidestepping = true;
  public float SidestepDistance = 1f;
  public float SidestepRaycastDistance = 1f;
  public float SidestepInterval = 1f;
  public float SidestepIgnoreWithinDistanceToGoal = 0.5f;

  public int MaxFollowers = 9;
  public float FollowDistanceTooClose = 0.5f;
  public float FollowDistanceWalk = 1f;
  public float FollowDistanceRun = 2f;
  // affected by time of day?
  public float GlobalSightDistance = 8;

  public float BuildDistanceOffset = 1f;
  // actual build distance
  public float BuildDistance
  {
    get{ return BuildDistanceOffset + _currentBuildDistance; }
  }
  // current build object distance
  float _currentBuildDistance;

  public float CurrentBuildDistance
  {
    set{ _currentBuildDistance = value; }
  }
   

  // flag when application is shutting down
  public static bool IsQuiting = false;
  public static bool IsPaused = false;

  [Header( "Global" )]
  // If you do not want to save the level on exit, disable this before quiting
  public bool SerializeWorld = true;
  #if ENDLESS_WORLD
  public bool EndlessWorld = true;
#endif
  // turn this on to resume the game as the last saved character (editor only)
  public bool ResumeAsSavedPlayerCharacter = false;
  public bool BuildStructures = false;

  public CameraController cameraController;
  public GameObject UI;
  public GameObject MainMenu;
  public GameObject LoadingScreen;

  public GameData gameData;
  public GameObject avatarPrefab;
  public Tag PlayerTeamTag = Tag.Blue;

  int InitializePlayerId = 0;
  public Character playerCharacter;
  Vector2 CharacterLookAngles;
  public GameObject audioOneShotPrefab;
  public float ProgressUpdateInterval = 1;

  // serialized objects
  public Dictionary<string,GameObject> ResourceLookup = new Dictionary<string, GameObject>();
  // zones
  public GameObject ZonePrefab;
  public List<Zone> Zones = new List<Zone>();
  public Dictionary<string,Zone> ZoneLookup = new Dictionary<string, Zone>();


  [Header( "Time Of Day" )]
  public Light SunDirectional;
  public Gradient AmbientGradient;
  // 720 is noon
  public float CurrentTimeOfDay = 400f;
  // 24 hrs, in minutes
  public float LengthOfDay = 1440f;
  public float TimeMultiplier = 1f;
  public float UpdateTimeOfDayInterval = 1;
  float lastTimeOfDayUpdate;
  public AnimationCurve SightDistanceCurve;

  [Header( "Fog" )]
  public bool fogEnabled = true;
  public FogMode fogMode = FogMode.Exponential;
  public float fogDensity = 0.02f;
  public float fogStart = 30;
  public float fogEnd = 80;


  [Header( "misc" )]
  public NameGenerator namegen;
  public Color footprintColor = new Color( 0.2f, 0.2f, 0.2f, 0.5f );
  public Zone.PaintType footprintType = Zone.PaintType.Blend;
  public float CamoflaugeMoveThreshold = 0.1f;
  // fps / update counter
  float unscaledTime = 0;
  int frames = 0;
  int ups = 0;


  [Header( "Builder" )]
  public ConstructionLibrary ConstructionLibrary;
  // compound objects
  public List<GameObject> BuildPrefabs;
  // inventory items
  public List<GameObject> ItemPrefabs;
  public Material BuildMaterial;
  public Material DeleteMaterial;
  // decals for painting the ground
  public Sprite[] Decals;
  // sprite lookup for dead body prefab
  public Sprite[] AllSprites;
  public GameObject BlockSelector;


  [Header( "UI" )]
  List<string> WorldList = new List<string>();
  public GameObject LevelSelectionButtonTemplate;
  public Text txtLevelName;
  public Text txtCharacterName;

  bool isMenuShowing { get { return MainMenu.activeInHierarchy; } }

  public Slider ProgressBar;
  public Text Mode;
  public GameObject ControlInfo;
  public Text InteractCharacterName;
  public CharacterInfoView CurrentCharacterInfo;
  public CharacterInfoView SelectedCharacterInfo;
  public ContextMenu ContextMenu;

  public Image SelectedGroundDecal;

  [Header( "Speech" )]
  public Text SpeechName;
  public Text SpeechText;
  Character SpeechCharacter;
  int SpeechPriority = 0;
  public float SpeechRange = 8;
  Timer SpeechTimer = new Timer();

  [Header( "DEBUG" )]
  public Text UPS;
  public Text SOC;
  public Text DebugChar;
  public Text DebugDead;
  public Text DebugSwag;
  public Text DebugFood;

  public Dictionary<string,IntValue> IntSetting = new Dictionary<string, IntValue>();
  public Dictionary<string,BoolValue> BoolSetting = new Dictionary<string, BoolValue>();


  void Awake()
  {
    enabled = false;
    DontDestroyOnLoad( gameObject );
    StartCoroutine( Initialize() );
  }

  public void Serialize()
  {
    WriteSettings();
    WriteCharacterData();
    WriteCharacterEventRecord();

    foreach( var zone in Zones )
      zone.Serialize();

    SaveLevelScreen();
  }

  IEnumerator Initialize()
  {
    Pause();
    InitializeSettings();
    VerifyPersistentData();
    ReadSettings();
    yield return ShowLoadingScreenRoutine( true, "Loading... "+InitializeWorldName );

    ReadCharacterData();
    ReadEventRecord();

    namegen.DefaultTraining();

    // UI
    UI.SetActive( true );
    MainMenu.GetComponent<Activator>().Activate( "levelselect" );
    ContextMenu.Hide();
    InteractCharacterName.gameObject.SetActive( false );
    SpeechName.gameObject.SetActive( false );
    SpeechText.gameObject.SetActive( false );
    InitializeWorldList();

    // Load Resources
    GameObject[] res = Resources.LoadAll<GameObject>( "serialized" );
    foreach( GameObject go in res )
      ResourceLookup.Add( go.name, go );
//    List<GameObject> MarchingSquares = new List<GameObject>( Resources.LoadAll<GameObject>( "marchingsquares" ) );
//    foreach( GameObject go in MarchingSquares )
//      ResourceLookup.Add( go.name, go );
    BuildPrefabs = new List<GameObject>( Resources.LoadAll<GameObject>( "construction" ) );
    foreach( GameObject go in BuildPrefabs )
      ResourceLookup.Add( go.name, go );
    // add singles to build prefabs, after the compound objects in the "build" folder
    BuildPrefabs.AddRange( res );
    ItemPrefabs = new List<GameObject>( Resources.LoadAll<GameObject>( "item" ) );
    AllSprites = Resources.FindObjectsOfTypeAll<Sprite>();

    // Misc
    gameData.Initialize();
    ConstructionLibrary.Initialize();
    ConstructionLibrary.Hide();
    HideControlInfo();
    HideCharacterInfo();
    CurrentCharacterInfo.gameObject.SetActive( false );
    SelectedGroundDecal.transform.parent.gameObject.SetActive( false );


    if( InitializeWorldName == null || InitializeWorldName.Length == 0 )
    {
      ShowMenu( true );
      UpdateLightingSettings();
      UpdateTimeOfDay();
    }
    else
    {
      ShowMenu( false );

      yield return Construct( InitializeWorldName );
      Character playerCharacter = null;
      if( InitializePlayerId != 0 )
      {
        SerializedObject playerSO = SerializedObject.ResolveObjectFromId( InitializePlayerId );
        if( playerSO != null )
          playerCharacter = playerSO.GetComponent<Character>();
      }
      if( playerCharacter == null )
      {
        // Create the character only after the initial world construction.
        // This is here to allow the camera controller to initialize first.
        playerCharacter = FindExistingCharacter();
        if( playerCharacter == null )
        {
          playerCharacter = CreateNewCharacter();
        }
      }
      SetPlayerCharacter( playerCharacter );
      //cameraController.lookInput = CharacterLookAngles;
      cameraController.delay = CameraController.delayreset;
    }
      
    yield return HideLoadingScreenRoutine();
    Unpause();
    enabled = true;
  }

  void InitializeSettings()
  {
    Setting[] settings = MainMenu.GetComponentsInChildren<Setting>( true );
    foreach( var s in settings )
    {
      if( s.isInteger )
      {
        s.intValue.Init();
        IntSetting.Add( s.intValue.name, s.intValue );
      }
      if( s.isBool )
      {
        s.boolValue.Init();
        BoolSetting.Add( s.boolValue.name, s.boolValue );
      }
    }

    IntSetting[ "farplane" ].onValueChanged = delegate(int value )
    {
      cameraController.cam.farClipPlane = value;
    };
    IntSetting[ "lookSensitivity" ].onValueChanged = delegate(int value )
    {
      cameraController.cameraLookSpeed = value;
    };
    IntSetting[ "time" ].onValueChanged = delegate(int value )
    {
      CurrentTimeOfDay = value;
      UpdateTimeOfDay();
    };
    IntSetting[ "timeMultiplier" ].onValueChanged = delegate(int value )
    {
      TimeMultiplier = value;
    };

    IntSetting[ "shadowDistance" ].onValueChanged = delegate(int value )
    {
      QualitySettings.shadowDistance = value;
    };
    BoolSetting[ "shadowsEnabled" ].onValueChanged = delegate(bool value )
    {
      if( value )
        QualitySettings.shadows = ShadowQuality.HardOnly;
      else
        QualitySettings.shadows = ShadowQuality.Disable;

    };

    BoolSetting[ "footprintsEnabled" ].onValueChanged = delegate(bool value )
    {
      Footprints = value;
    };
    BoolSetting[ "bloodtrailsEnabled" ].onValueChanged = delegate(bool value )
    {
      Bloodtrails = value;
    };

    IntSetting[ "characterUpdateNth" ].onValueChanged = delegate(int value )
    {
      CharacterUpdateEveryNthFrame = Mathf.FloorToInt( value );
    };
    IntSetting[ "characterUpdatesPerFrame" ].onValueChanged = delegate(int value )
    {
      CharacterUpdateCallsPerUpdate = Mathf.FloorToInt( value );
    };

    IntSetting[ "characterLimit" ].onValueChanged = delegate(int value )
    {
      Character.Limit.Upper = Mathf.FloorToInt( value );
    };
    IntSetting[ "swagLimit" ].onValueChanged = delegate(int value )
    {
      CarryObject.Limit.Upper = Mathf.FloorToInt( value );
    };
    IntSetting[ "deadbodyLimit" ].onValueChanged = delegate(int value )
    {
      Deadbody.Limit.Upper = Mathf.FloorToInt( value );
    };
    IntSetting[ "foodLimit" ].onValueChanged = delegate(int value )
    {
      Food.Limit.Upper = Mathf.FloorToInt( value );
    };

    IntSetting[ "pixelLightCount" ].onValueChanged = delegate(int value )
    {
      QualitySettings.pixelLightCount = value;
    };

    IntSetting[ "perlinScale" ].onValueChanged = delegate(int value )
    {
      SpawnPerlinScale = value;
    };
  }

  void ClearWorldList()
  {
    WorldList.Clear();
    Transform p = LevelSelectionButtonTemplate.transform.parent;
    for( int i = 0; i < p.childCount; i++ )
    {
      Transform c = p.GetChild( i );
      if( c != LevelSelectionButtonTemplate.transform )
        GameObject.Destroy( c.gameObject );
    }
  }

  void AddToWorldList( string worldName )
  {
    GameObject go = GameObject.Instantiate<GameObject>( LevelSelectionButtonTemplate, LevelSelectionButtonTemplate.transform.parent );
    go.SetActive( true );
    Text txt = go.GetComponentInChildren<Text>();
    txt.text = worldName;
    // image preview
    string imgPath = Application.persistentDataPath + "/" + worldName + "/screen.png";
    if( File.Exists( imgPath ) )
    {
      RawImage img = go.GetComponentInChildren<RawImage>();
      byte[] bytes = File.ReadAllBytes( imgPath );
      if( bytes.Length > 0 )
      {
        Texture2D preview = new Texture2D( 320, 200 );
        preview.filterMode = FilterMode.Point;
        preview.LoadImage( bytes );
        img.texture = preview;
      }
    }
    UnityEngine.UI.Button btn = go.GetComponentInChildren<UnityEngine.UI.Button>();
    btn.onClick.AddListener( new UnityEngine.Events.UnityAction( delegate()
    {
      StartCoroutine( LevelTransition( worldName ) );
    } ) );
  }

  void RemoveFromWorldList( string name )
  {
    WorldList.Remove( name );
    Transform p = LevelSelectionButtonTemplate.transform.parent;
    for( int i = 0; i < p.childCount; i++ )
    {
      Transform c = p.GetChild( i );
      Text t = c.GetComponentInChildren<Text>();
      if( t.text == name )
      {
        GameObject.Destroy( c.gameObject );
        return;
      }
    }
  }

  void InitializeWorldList()
  { 
    LevelSelectionButtonTemplate.SetActive( false );
    ClearWorldList();
    // populate level thumbnails
    string[] dirs = Directory.GetDirectories( Application.persistentDataPath );
    foreach( var dir in dirs )
    {
      string basename = new DirectoryInfo( dir ).Name;
      if( basename != "Unity" )
        WorldList.Add( basename );
    }
    // include packed levels in resources
    TextAsset[] tas = Resources.LoadAll<TextAsset>( "zone/" );
    foreach( var ass in tas )
      if( !WorldList.Contains( ass.name ) )
        WorldList.Add( ass.name );

    foreach( var worldName in WorldList )
      AddToWorldList( worldName );
  }

  void ShowMenu( bool show )
  {
    if( show )
    {
      Pause();
      MainMenu.SetActive( true );
      Cursor.lockState = CursorLockMode.None;
      EnableRaycaster( true );
    }
    else
    {
      Unpause();
      MainMenu.SetActive( false );
      Cursor.lockState = CursorLockMode.Locked;
      EnableRaycaster( false );
      cameraController.delay = CameraController.delayreset;
      Input.ResetInputAxes();
    }
  }

  public void EnableRaycaster( bool enable = true )
  {
    UI.GetComponent<UnityEngine.UI.GraphicRaycaster>().enabled = enable;
  }

  void ShowLoadingScreen( bool show, string message = "Loading.." )
  {
    LoadingScreen.SetActive( true );
    Text txt = LoadingScreen.GetComponentInChildren<Text>();
    txt.text = message;
  }

  void HideLoadingScreen()
  {
    LoadingScreen.SetActive( false );
  }


  IEnumerator ShowLoadingScreenRoutine( bool show, string message = "Loading.." )
  {
    // This is a coroutine simply to wait a single frame after activating the loading screen.
    // Otherwise the screen will not show! 
    ShowLoadingScreen( show, message );
    yield return null;
  }

  IEnumerator HideLoadingScreenRoutine()
  {
    HideLoadingScreen();
    yield return null;
  }

  void OnApplicationQuit()
  {
    if( !IsQuiting )
    {
      Application.CancelQuit();
      IsQuiting = true;
      // cleanup player states
      if( playerCharacter != null )
      {
        playerCharacter.CleanupPlayerState();
      }
      if( SerializeWorld )
      {
        if( Application.isEditor )
          Serialize();
        else
          StartCoroutine( SerializeAndQuit() );
      }
    }
  }

  IEnumerator SerializeAndQuit()
  {
    yield return ShowLoadingScreenRoutine( true, "Saving... Please Wait" );
    Serialize();
    Application.Quit();
  }

  void OnApplicationFocus( bool hasFocus )
  {
    if( hasFocus )
    {
      if( isMenuShowing )
      {
        Cursor.lockState = CursorLockMode.None;
      }
      else
      {
        Cursor.lockState = CursorLockMode.Locked;
      }
    }
  }

  void Update()
  {
    UpdateDebugPanel();
    Timer.UpdateTimers();

    if( SpeechTimer.IsActive && playerCharacter != null && SpeechCharacter != null )
    {
      float DistanceSqr = Vector3.SqrMagnitude( SpeechCharacter.moveTransform.position - playerCharacter.moveTransform.position );
      if( DistanceSqr > SpeechRange * SpeechRange )
        SpeechTimer.Stop( true );
      else
        SpeechText.rectTransform.localScale = Vector3.one * ( 1f - 0.5f * Mathf.Clamp( Mathf.Sqrt( DistanceSqr ) / SpeechRange, 0, 1 ) );
    }

    if( playerCharacter != null &&
        playerCharacter.CurrentStateName == "Construction" &&
        playerCharacter.BuildMode != Character.Mode.Menu )
      Mode.text = playerCharacter.BuildMode.ToString();
    else
      Mode.text = "";

    if( Input.GetButtonDown( "CursorLockToggle" ) )
    {
      if( Cursor.lockState != CursorLockMode.None )
      {
        Cursor.lockState = CursorLockMode.None;
      }
      else
      {
        Cursor.lockState = CursorLockMode.Locked;
      }
    }
    #if UNITY_STANDALONE_LINUX
    if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown("q") ) 
    {
    	Application.Quit();
    	return;
    }
    #endif

    if( Input.GetButtonUp( "Help" ) )
    {
      ShowMenu( !isMenuShowing );
      #if UNITY_STANDALONE_LINUX
      // hack: there is a weird bug where the Canvas will not update when an
      // object with a canvas renderer is disabled
      Canvas canvas = FindObjectOfType<Canvas>();
      canvas.gameObject.SetActive( false );
      canvas.gameObject.SetActive( true );
      #endif
    }

    #if UNITY_EDITOR_LINUX
    if( Input.GetKeyDown(KeyCode.Escape) )
    Cursor.lockState = CursorLockMode.None;
    #else
    #endif

    if( IsPaused )
      return;


    if( Input.GetButtonDown( "Screenshot" ) )
    {
      string now = System.DateTime.Now.Year.ToString() +
                   System.DateTime.Now.Month.ToString( "D2" ) +
                   System.DateTime.Now.Day.ToString( "D2" ) + "." +
                   System.DateTime.Now.Minute.ToString( "D2" ) +
                   System.DateTime.Now.Second.ToString( "D2" );
      ScreenCapture.CaptureScreenshot( Application.persistentDataPath + "/" + now + ".png" );
    }
    
    if( Input.GetButtonUp( "SelectCycle" ) )
    {
      if( playerCharacter == null || (playerCharacter.CurrentState.Name != "Construction" && playerCharacter.CurrentState.Name != "Sleep" ) )
      {
        NextCharacter( 1 );
        if( playerCharacter == null )
          SetPlayerCharacter( CreateNewCharacter() );
      }
    }
  
    if( Input.GetButtonUp( "WatchCycle" ) )
    {
      if( playerCharacter != null )
        SetPlayerCharacter( null );
      WatchRandomCharacter();
    }
    if( Input.GetButtonUp( "Freecam" ) )
    {
      SetPlayerCharacter( null );
    }

    // ups / world update counter
    frames++;
    unscaledTime += Time.unscaledDeltaTime;
    while( unscaledTime > 1f )
    {
      unscaledTime -= 1f;
      ups = frames;
      frames = 0;
    }
      
    CurrentTimeOfDay += Time.deltaTime * TimeMultiplier;

    if( Time.time - lastTimeOfDayUpdate > UpdateTimeOfDayInterval )
    {
      lastTimeOfDayUpdate = Time.time;
      // this will call UpdateTimeOfDay()
      IntSetting[ "time" ].Value = Mathf.FloorToInt( CurrentTimeOfDay );
    }


    /* Moved to Zone.Update
    Profiler.BeginSample( "overlay texture" );
    if( overlayTextureDirtyFlag )
    {
      overlayTextureDirtyFlag = false;
      overlayTexture.Apply();
    }
    Profiler.EndSample();*/

    Profiler.BeginSample( "deadbody" );
    if( Deadbody.Limit.All.Count > 0 )
    {
      if( Deadbody.UpdateFrameCounter++ % Deadbody.UpdateEveryNthFrame == 0 )
      {
        if( Deadbody.UpdateID >= Deadbody.Limit.All.Count )
          Deadbody.UpdateID = 0;
        Deadbody.Limit.All[ Deadbody.UpdateID++ ].UpdateBody();
      }
    }
    Profiler.EndSample();

    Profiler.BeginSample( "Character Sensor Update" );
    if( Character.Limit.All.Count > 0 )
    {
      if( CharacterUpdateFrameCounter++ % CharacterUpdateEveryNthFrame == 0 )
      {
        for( int i = 0; i < CharacterUpdateCallsPerUpdate && i < Character.Limit.All.Count; i++ )
        {
          if( CharacterUpdateID >= Character.Limit.All.Count )
            CharacterUpdateID = 0;
          Character.Limit.All[ CharacterUpdateID ].UpdateSensor();
          CharacterUpdateID++;
        }
      }
    }
    Profiler.EndSample();

    Profiler.BeginSample( "CharacterUpdate" );
    for( int i = 0; i < Character.Limit.All.Count; i++ )
    {
      Character.Limit.All[ i ].CharacterUpdate();
    }
    Profiler.EndSample();

    /* Moved to Zone.Update
    foreach( var p in points )
    {
      Debug.DrawLine( p.point, p.point + Vector3.up * 10, p.def.lineColor );
    }*/

    if( ConstructionLibrary.gameObject.activeInHierarchy )
      ConstructionLibrary.ManualUpdate();
  }

  void FixedUpdate()
  {
    Profiler.BeginSample( "Character Fixed Update" );
    for( int i = 0; i < Character.Limit.All.Count; i++ )
    {
      Character.Limit.All[ i ].CharacterFixedUpdate();
    }
    Profiler.EndSample();
  }

  void UpdateLightingSettings()
  {
    RenderSettings.sun = SunDirectional;
    RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
    RenderSettings.fog = fogEnabled;
    RenderSettings.fogDensity = fogDensity;
    RenderSettings.fogStartDistance = fogStart;
    RenderSettings.fogEndDistance = fogEnd;
    RenderSettings.fogMode = fogMode;
  }

  public float ClearOverlayAmount = 0.2f;
  public float ClearOverlayShminterval = 360;
  float ClearOverlayLastShtime;

  public void UpdateTimeOfDay()
  {
    CurrentTimeOfDay = Mathf.Repeat( CurrentTimeOfDay, LengthOfDay );
    RenderSettings.ambientLight = AmbientGradient.Evaluate( CurrentTimeOfDay / LengthOfDay );
    cameraController.cam.backgroundColor = RenderSettings.ambientLight;
    RenderSettings.fogColor = AmbientGradient.Evaluate( CurrentTimeOfDay / LengthOfDay );
    RenderSettings.fogDensity = fogDensity;
    RenderSettings.fogStartDistance = fogStart;
    RenderSettings.fogEndDistance = fogEnd;
    RenderSettings.fogMode = fogMode;
    // directional light angle
    float TimeNormal = Mathf.Round( CurrentTimeOfDay ) / LengthOfDay;
    float rot = 270 - 360 * TimeNormal;
    SunDirectional.transform.rotation = Quaternion.Euler( new Vector3( rot, 90, 0 ) );
    if( TimeNormal > 0.25f && TimeNormal < 0.75f )
      SunDirectional.enabled = true;
    else
      SunDirectional.enabled = false;
    //GlobalSightDistance = SightDistanceCurve.Evaluate( CurrentTimeOfDay );
    if( CurrentTimeOfDay - ClearOverlayLastShtime > ClearOverlayShminterval )
    {
      ClearOverlayLastShtime = CurrentTimeOfDay;
      foreach( Zone zone in Zones )
        zone.PaintGround( Color.clear, ClearOverlayAmount );
    }
  }

  IEnumerator WaitThenDo( float duration, System.Action action )
  {
    yield return new WaitForSeconds( duration );
    action.Invoke();
  }

  public void LoadLevel( string sceneName )
  {
    StartCoroutine( LevelTransition( sceneName ) );
  }

  /*IEnumerator LoadLevel( string levelName )
  {
    yield return ShowLoadingScreen( true );
    Serialize();
    Destruct();
    Construct( levelName );
    RebuildLevelList();
    yield return ShowLoadingScreen( false );
  }*/

    
  IEnumerator LevelTransition( string levelName )
  {
    Pause();
    enabled = false;
    yield return ShowLoadingScreenRoutine( true, "Loading... "+levelName );

    /*// carry player character into the next level
    if( playerCharacter != null )
    {
      SerializedObject.RemoveSerializedObject( playerCharacter.gameObject.GetComponent<SerializedObject>() );
      playerCharacter.transform.parent = null;
      playerCharacter.gameObject.SetActive( false );
    }*/

    if( SerializeWorld )
      Serialize();
    Destruct();
    yield return null;
    yield return Construct( levelName );

    /*if( playerCharacter != null )
    {
      SerializedObject.AddSerializedObject( playerCharacter.gameObject.GetComponent<SerializedObject>() );
      playerCharacter.transform.position = FindSpawnPosition();
      playerCharacter.gameObject.SetActive( true );
    }*/
    PlayAsExistingCharacter();

    yield return HideLoadingScreenRoutine();
    Unpause();
    ShowMenu( false );
    enabled = true;
  }

  public void RespawnAvatar()
  {
    // wait for character to die
    StartCoroutine( WaitThenDo( 5f, PlayAsExistingCharacter ) );
  }

  void PlayAsExistingCharacter()
  {
    //findNewCharacterConsecutive++;
    Character respawnChar = FindExistingCharacter();
    if( respawnChar == null )
    {
      respawnChar = CreateNewCharacter();
    }
    cameraController.ScreenFadeColor( Color.clear );
    cameraController.LookTarget = respawnChar.transform;
    cameraController.transform.position = respawnChar.transform.position + Vector3.up * 5;
    cameraController.transform.rotation = Quaternion.Euler( 90, 0, 0 );
    SetPlayerCharacter( respawnChar );
    /*
    StartCoroutine( WaitThenDo( 1f, delegate(){
      if( respawnChar==null )
      {
        // if the controllable characters are continuously killed at spawn point, then go to home level as a failsafe.
        if( findNewCharacterConsecutive > 3 )
          LoadLevel( "Home" );
        PlayAsExistingCharacter();
      }
      else
      {
        SetPlayerCharacter( respawnChar );
        findNewCharacterConsecutive=0;
      }
    } ) );*/
  }

  Character FindExistingCharacter()
  {
    if( Character.Limit.All.Count > 0 )
    {
      // choose random character on player's team
      List<Character> blues = Character.Limit.All.FindAll( delegate(Character obj )
      {
        if( obj.Team != null )
          return obj.Team.Tag == PlayerTeamTag;
        return false;
      } );
      if( blues != null && blues.Count > 0 )
        return blues[ Random.Range( 0, blues.Count ) ];
    }
    return null;
  }

  Character CreateNewCharacter()
  {
    GameObject go = Spawn( avatarPrefab, FindSpawnPosition(), Quaternion.identity, null, false, true );
    go.name = avatarPrefab.name;
    AssignTeam( go, PlayerTeamTag );
    return go.GetComponent<Character>();
  }

  Vector3 FindSpawnPosition()
  {
    // last resort: spawn at origin
    Vector3 pos = Vector3.zero;
    List<SpawnPoint> sps = new List<SpawnPoint>( FindObjectsOfType<SpawnPoint>() );
    SpawnPoint point = sps.Find( delegate(SpawnPoint obj )
    {
      Tags tags = obj.GetComponent<Tags>();
      return tags.HasTag( PlayerTeamTag );
    } );
    if( point != null )
    {
      // first choice: spawn at spawn point
      pos = point.transform.position;
    }
    else
    {
      // second choice: spawn at object with respawn tag
      GameObject spawnPoint = GameObject.FindWithTag( "Respawn" );
      if( spawnPoint != null )
        pos = spawnPoint.transform.position;
    }
    return pos;
  }

  int CharacterSelectIndex = 0;

  public void NextCharacter( int increment )
  {
    List<Character> chars = Character.Limit.All;
    if( chars.Count > 0 )
    {
      List<Character> blues = chars.FindAll( delegate(Character x )
      {
        Tags tags = x.GetComponent<Tags>();
        bool valid = tags.HasTag( PlayerTeamTag );
        if( playerCharacter != null && x.id == playerCharacter.id )
          valid = false;
        return valid;
      } );
      if( blues != null && blues.Count > 0 )
      {
        CharacterSelectIndex += increment;
        while( CharacterSelectIndex < 0 )
          CharacterSelectIndex += blues.Count;
        int index = CharacterSelectIndex % blues.Count;
        SetPlayerCharacter( blues[ index ] );
      }
    }
  }

  void WatchRandomCharacter()
  {
    List<Character> chars = new List<Character>( GameObject.FindObjectsOfType<Character>() );
    if( chars.Count > 0 )
    {
      cameraController.LookTarget = chars[ Random.Range( 0, chars.Count ) ].transform;
      CameraSettings settings = new CameraSettings();
      settings.MinDistance = 1f;
      cameraController.AssignSettings( settings );
    }
  }

  public void SetPlayerCharacter( Character value )
  {
    // release previous chracter
    if( playerCharacter != null )
      playerCharacter.CleanupPlayerState();
    playerCharacter = value;

    if( playerCharacter == null )
    {
      // camera
      cameraController.FreelookMode();
      CameraSettings settings = new CameraSettings();
      settings.MinDistance = 2f;
      cameraController.AssignSettings( settings );
      // UI
      txtCharacterName.text = "freelook mode";
      CurrentCharacterInfo.gameObject.SetActive( false );
      SelectedCharacterInfo.gameObject.SetActive( false );
    }
    else
    {
      playerCharacter.PushState( "PlayerControlled" );
      // camera
      cameraController.AssignLookTarget( playerCharacter.transform );
      CameraSettings settings = new CameraSettings();
      settings.MinDistance = 0.5f;
      cameraController.AssignSettings( settings );
      // UI
      txtCharacterName.text = playerCharacter.CharacterName;
      CurrentCharacterInfo.gameObject.SetActive( true );
      CurrentCharacterInfo.ShowInfo( playerCharacter );
    }
    cameraController.ScreenFadeColor( Color.clear );
  }


  public IEnumerator Construct( string worldName )
  {
    Pause();
    // DO NOT serialize if the app quits while loading a zone
    bool cachedSerializeWorld = SerializeWorld;
    SerializeWorld = false;

    WorldName = worldName;
    Zone zone = null;

    /*AsyncOperation asyncLoad = SceneManager.LoadSceneAsync( WorldName, LoadSceneMode.Single );
    //Wait until the last operation fully loads to return anything
    while( !asyncLoad.isDone )
    {
      yield return null;
    }
        
    Scene activeScene = SceneManager.GetSceneByName( WorldName );
    if( activeScene.IsValid() )
    {
      foreach( var go in activeScene.GetRootGameObjects() )
      {
        zone = go.GetComponent<Zone>();
        if( zone != null )
          break;
      }
    }
    else
    {
      activeScene = SceneManager.CreateScene( WorldName );
      SceneManager.SetActiveScene( activeScene );
    }

    if( zone == null )
      zone = GameObject.Instantiate( ZonePrefab ).GetComponent<Zone>();
    */

    Scene activeScene = SceneManager.CreateScene( WorldName );
    // The active scene is the global lighting source, and destination for new gameobjects.
    SceneManager.SetActiveScene( activeScene );
    zone = GameObject.Instantiate( ZonePrefab ).GetComponent<Zone>();
    zone.Initialize( WorldName );


    SerializedObject.serializedObjects.Clear();
    SerializedObject.serializedComponents.Clear();

    yield return zone.Construct();
    UpdateLightingSettings();
    UpdateTimeOfDay();

    Unpause();
    SerializeWorld = cachedSerializeWorld;
    yield return null;
  }

  void VerifyPersistentData()
  {
    // ensure that all persistent data files exist. IF not, unpack from zip in build.
    bool unpack = false;
    foreach( var filename in persistentFilenames )
      if( !File.Exists( Application.persistentDataPath + "/" + filename ) )
        unpack = true;
    if( unpack )
    {
      string zipPath = Application.temporaryCachePath + "/persistent.zip";
      TextAsset zipfile = Resources.Load( "persistent" ) as TextAsset;
      if( zipfile != null )
      {
        File.WriteAllBytes( zipPath, zipfile.bytes );
        Debug.Log( "Unzipping persistent: " + zipPath );
        using( ZipFile zip = ZipFile.Read( zipPath ) )
        {
          zip.ExtractAll( Application.persistentDataPath, ExtractExistingFileAction.OverwriteSilently );
        }
      }
      else
      {
        Debug.LogWarning( "no level directory or zip file in build: " + name );
      }
    }
  }


  void ReadSettings()
  {
    JsonData json = null;
    string gameJson = File.ReadAllText( settingsPath );
    if( gameJson.Length > 0 )
    {
      JsonReader reader = new JsonReader( gameJson );
      json = JsonMapper.ToObject( reader );
    }

    if( ResumeAsSavedPlayerCharacter )
      InitializeWorldName = JsonUtil.Read<string>( InitializeWorldName, json, "scene" );

    IntSetting[ "farplane" ].Value = JsonUtil.Read<int>( 200, json, "settings", "farplane" );
    IntSetting[ "lookSensitivity" ].Value = JsonUtil.Read<int>( 100, json, "settings", "lookSensitivity" );
    IntSetting[ "time" ].Value = JsonUtil.Read<int>( 700, json, "settings", "time" );
    IntSetting[ "timeMultiplier" ].Value = JsonUtil.Read<int>( 1, json, "settings", "timeMultiplier" );
    BoolSetting[ "shadowsEnabled" ].Value = JsonUtil.Read<bool>( true, json, "settings", "shadowsEnabled" );
    IntSetting[ "shadowDistance" ].Value = JsonUtil.Read<int>( 20, json, "settings", "shadowDistance" );
    BoolSetting[ "footprintsEnabled" ].Value = JsonUtil.Read<bool>( true, json, "settings", "footprintsEnabled" );
    BoolSetting[ "bloodtrailsEnabled" ].Value = JsonUtil.Read<bool>( true, json, "settings", "bloodtrailsEnabled" );
    IntSetting[ "characterUpdateNth" ].Value = System.Math.Max( 1, JsonUtil.Read<int>( 1, json, "settings", "characterUpdateNth" ) );
    IntSetting[ "characterUpdatesPerFrame" ].Value = System.Math.Max( 1, JsonUtil.Read<int>( 50, json, "settings", "characterUpdatesPerFrame" ) );
    IntSetting[ "pixelLightCount" ].Value = JsonUtil.Read<int>( 4, json, "settings", "pixelLightCount" );
    IntSetting[ "perlinScale" ].Value = JsonUtil.Read<int>( 20, json, "settings", "perlinScale" );

    IntSetting[ "characterLimit" ].Value = JsonUtil.Read<int>( 200, json, "world", "limit", "character" );
    IntSetting[ "deadbodyLimit" ].Value = JsonUtil.Read<int>( 300, json, "world", "limit", "deadbody" );
    IntSetting[ "swagLimit" ].Value = JsonUtil.Read<int>( 500, json, "world", "limit", "Carry" );
    IntSetting[ "foodLimit" ].Value = JsonUtil.Read<int>( 200, json, "world", "limit", "food" );

    if( json != null )
    {
      if( ResumeAsSavedPlayerCharacter )
      {
        if( json.Keys.Contains( "player" ) )
        {
          JsonData playerData = JsonUtil.Read<JsonData>( new JsonData(), json, "player" );
          InitializePlayerId = JsonUtil.Read<int>( 0, playerData, "id" );
        }
      }
    }
  }

  void WriteSettings()
  {
    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;
    writer.WriteObjectStart(); // root

    writer.WritePropertyName( "scene" );
    writer.Write( WorldName );

    writer.WritePropertyName( "settings" );
    writer.WriteObjectStart();
    foreach( var pair in IntSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value );
    }
    foreach( var pair in BoolSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value );
    }
    writer.WriteObjectEnd();

    writer.WritePropertyName( "world" );
    writer.WriteObjectStart(); // world
    writer.WritePropertyName( "limit" );
    writer.WriteObjectStart();
    writer.WritePropertyName( "character" );
    writer.Write( Character.Limit.Upper );
    writer.WritePropertyName( "deadbody" );
    writer.Write( Deadbody.Limit.Upper );
    writer.WritePropertyName( "Carry" );
    writer.Write( CarryObject.Limit.Upper );
    writer.WritePropertyName( "food" );
    writer.Write( Food.Limit.Upper );
    writer.WriteObjectEnd();
    writer.WriteObjectEnd(); // world end

    if( playerCharacter != null )
    {
      SerializedObject so = playerCharacter.GetComponent<SerializedObject>();
      writer.WritePropertyName( "player" );
      writer.WriteObjectStart(); // player
      writer.WritePropertyName( "id" );
      writer.Write( so.id );
      writer.WriteObjectEnd(); // player end
    }

    writer.WriteObjectEnd(); // root end
    File.WriteAllText( settingsPath, writer.ToString() );
  }


  public Dictionary<int,CharacterInfo> CharacterInfoLookup = new Dictionary<int, CharacterInfo>();

  void WriteCharacterData()
  {
    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;
    // root
    writer.WriteObjectStart();

    writer.WritePropertyName( "GlobalInstanceID" );
    writer.Write( SerializedObject.GlobalInstanceID );

    writer.WritePropertyName( "character" );
    writer.WriteArrayStart();
    foreach( var pair in CharacterInfoLookup )
    {
      writer.WriteObjectStart();
      CharacterInfo info = pair.Value;
      writer.WritePropertyName( "id" );
      writer.Write( info.id );
      writer.WritePropertyName( "name" );
      writer.Write( info.name );
      writer.WritePropertyName( "alive" );
      writer.Write( info.alive );
      writer.WritePropertyName( "female" );
      writer.Write( info.female );
      writer.WriteObjectEnd();
    }
    writer.WriteArrayEnd();
    // root
    writer.WriteObjectEnd();
    File.WriteAllText( characterDataPath, writer.ToString() );
  }


  void ReadCharacterData()
  {
    if( !File.Exists( characterDataPath ) )
    {
      Debug.LogError( "character data does not exist" );
      return;
    }
    JsonReader reader = new JsonReader( File.ReadAllText( characterDataPath ) );
    JsonData json = JsonMapper.ToObject( reader );
  
    SerializedObject.GlobalInstanceID = JsonUtil.Read<int>( SerializedObject.GlobalInstanceID, json, "GlobalInstanceID" );

    JsonData cinfo = json[ "character" ];
    foreach( JsonData ob in cinfo )
    {
      CharacterInfo ci = new CharacterInfo();
      ci.id = (int)ob[ "id" ].GetNatural();
      ci.name = ob[ "name" ].GetString();
      ci.alive = JsonUtil.Read<bool>( true, ob, "alive" );
      ci.female = JsonUtil.Read<bool>( true, ob, "female" );
      CharacterInfoLookup[ ci.id ] = ci;
    }
  }

  public int GlobalEventID = 0;
  public Dictionary<int,CharacterEvent> CharacterEventRecord = new Dictionary<int, CharacterEvent>();

  void WriteCharacterEventRecord()
  {
    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;
    // root
    writer.WriteObjectStart();
    // global fields
    writer.WritePropertyName( "GlobalEventID" );
    writer.Write( GlobalEventID );
    // character events
    writer.WritePropertyName( "events" );
    writer.WriteArrayStart();
    foreach( var pair in CharacterEventRecord )
    {
      pair.Value.Serialize( writer );
    }
    writer.WriteArrayEnd();
    // root
    writer.WriteObjectEnd();
    File.WriteAllText( characterEventsPath, writer.ToString() );
  }


  void ReadEventRecord()
  {
    if( !File.Exists( characterEventsPath ) )
    {
      Debug.LogError( "events log does not exist" );
      return;
    }
    JsonReader reader = new JsonReader( File.ReadAllText( characterEventsPath ) );
    JsonData json = JsonMapper.ToObject( reader );

    GlobalEventID = JsonUtil.Read<int>( GlobalEventID, json, "GlobalEventID" );

    JsonData cinfo = json[ "events" ];
    foreach( JsonData ob in cinfo )
    {
      CharacterEvent evt = new CharacterEvent( ob );
      CharacterEventRecord[ evt.id ] = evt;
    }
  }


  public void DeleteAllPersistentGameData()
  {
    // DELETE ALL PERSISTENT GAME DATA
    Directory.Delete( Application.persistentDataPath, true );
    Directory.CreateDirectory( Application.persistentDataPath );
    Destruct();
    ClearWorldList();
    // must construct a level to avoid serializing previously deleted level when clicking level button
    StartCoroutine( Construct( "home" ) );
  }

  public void DeleteThisWorld()
  {
    Destruct();   
    RemoveFromWorldList( WorldName );
    if( Directory.Exists( Application.persistentDataPath + "/" + WorldName ) )
      Directory.Delete( Application.persistentDataPath + "/" + WorldName, true );
    // must construct a level to avoid serializing previously deleted level when clicking level button
    StartCoroutine( Construct( "home" ) );
  }

  public void Destruct()
  {   
    foreach( var zone in Zones )
      StartCoroutine( DestructZone( zone ) );
  }

  IEnumerator DestructZone( Zone zone )
  {
    AsyncOperation aso = SceneManager.UnloadSceneAsync( zone.name );
    while( !aso.isDone )
    {
      Debug.Log( "destruct zone " + zone.name + " progress " + aso.progress );
      yield return null;
    }
    Debug.Log( "destruct zone done" );
  }

  /*public void WriteScene()
  {
    foreach( var so in SerializedObject.serializedObjects )
    {
      so.Value.BeforeSerialize();
    }
      
    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;

    writer.WriteObjectStart();

    writer.WritePropertyName( "level" );
    writer.WriteObjectStart();
    writer.WritePropertyName( "snow" );
    writer.Write( IsSnowLevel );
    writer.WritePropertyName( "camera" );
    writer.WriteObjectStart();
    writer.WritePropertyName( "freepos" );
    JsonUtil.WriteVector( writer, cameraController.FreePosition );
    writer.WritePropertyName( "FreeLookAngles" );
    JsonUtil.WriteVector( writer, cameraController.FreeLookAngles );

    writer.WritePropertyName( "CharacterLookAngles" );
    JsonUtil.WriteVector( writer, cameraController.lookInput );

    writer.WriteObjectEnd();
    writer.WriteObjectEnd();

    writer.WritePropertyName( "objects" );
    writer.WriteArrayStart();
    foreach( var pair in SerializedObject.serializedObjects )
    {
      if( pair.Value.serialize )
      {
        writer.WriteObjectStart();
        pair.Value.Serialize( writer );
        writer.WriteObjectEnd(); 
      }
    }
    writer.WriteArrayEnd();

    writer.WriteObjectEnd();

    string dataFilePath = Application.persistentDataPath + "/" + WorldName + "/" + "scene.json";
    if( !Directory.Exists( Application.persistentDataPath + "/" + WorldName ) )
      Directory.CreateDirectory( Application.persistentDataPath + "/" + WorldName );
    File.WriteAllText( dataFilePath, writer.ToString() );
  }*/

  public void SaveLevelScreen()
  {
    // hide UI while taking screenshot
    bool ui = isMenuShowing;
    ShowMenu( false );

    Texture2D grab = ScreenCapture.CaptureScreenshotAsTexture();
    TextureScale.Bilinear( grab, 320, 200 );
    string grabpath = Application.persistentDataPath + "/" + WorldName + "/" + "screen.png";
    if( File.Exists( grabpath ) )
      File.Delete( grabpath );
    File.WriteAllBytes( grabpath, grab.EncodeToPNG() ); 
    Object.Destroy( grab );

    if( ui )
      ShowMenu( true );
  }


  void UpdateDebugPanel()
  {
    UPS.text = "UPS:" + ups.ToString();
    SOC.text = "SO#:" + SerializedObject.serializedObjects.Count.ToString();
    DebugChar.text = "Char:" + Character.Limit.All.Count.ToString();
    DebugDead.text = "Dead:" + Deadbody.Limit.All.Count.ToString();
    DebugSwag.text = "Swag:" + CarryObject.Limit.All.Count.ToString();
    DebugFood.text = "Food:" + Food.Limit.All.Count.ToString();
  }


  public static Vector3 SnapPos( Vector3 pos )
  {
    return new Vector3( Mathf.Floor( pos.x ) + 0.5f, 0, Mathf.Floor( pos.z ) + 0.5f );
  }

  public bool SpawnCheck( Vector3 pos, float radius = 0.4f )
  {
    return !Physics.CheckSphere( pos, radius, LayerMask.GetMask( new string[] {
      "Default",
      "CameraHide"
    } ), QueryTriggerInteraction.Ignore );
    //Vector3 extents = new Vector3( 0.4f, 4, 0.4f );
    //Collider[] colliders = Physics.OverlapBox( pos, extents, Quaternion.identity, LayerMask.GetMask(new string[]{ "Default", "CameraHide" }), QueryTriggerInteraction.Ignore );
  }

  public GameObject Spawn( string resourceName, Vector3 position, Quaternion rotation, Transform parent = null, bool limit = true, bool initialize = true )
  {
    // allow the lookup to check the name replacement table
    GameObject prefab = null;
    if( ResourceLookup.ContainsKey( resourceName ) )
    {
      prefab = ResourceLookup[ resourceName ];
    }
    else
    if( gameData.replacements.ContainsKey( resourceName ) )
    {
      if( ResourceLookup.ContainsKey( gameData.replacements[ resourceName ] ) )
        prefab = ResourceLookup[ gameData.replacements[ resourceName ] ];
    }
    if( prefab != null )
      return Spawn( prefab, position, rotation, parent, limit, initialize );
    return null;
  }

  public GameObject Spawn( GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null, bool limit = true, bool initialize = true )
  {
    if( limit )
    {
      ILimit[] limits = prefab.GetComponentsInChildren<ILimit>();
      foreach( var cmp in limits )
      {
        if( !cmp.IsUnderLimit() )
          return null;
      }
    }

    GameObject go = GameObject.Instantiate( prefab, position, rotation, parent );
    go.name = prefab.name;

    if( initialize )
    {
      SerializedComponent[] scs = go.GetComponentsInChildren<SerializedComponent>();
      foreach( var sc in scs )
        sc.AfterDeserialize();
    }
    return go;
  }

  public void SpawnRandom( GameObject prefab, int count, int maxTries = 1000 )
  {
    int tries = 0;
    while( count > 0 && tries < maxTries )
    {
      Vector3 pos = SnapPos( new Vector3( Random.value * (float)Zone.BlockSize, 0f, Random.value * (float)Zone.BlockSize ) );
      if( SpawnCheck( pos ) )
      {
        Spawn( prefab, pos, Quaternion.identity );
        count--;
      }
      tries++;
    }
  }

  float SpawnPerlinScale = 20;

  public void SpawnConway( GameObject prefab, int iterations )
  {
    FloatMap cwy = new FloatMap( Zone.BlockSize, Zone.BlockSize );
    cwy.FillPerlin( Vector2.zero, SpawnPerlinScale );
    cwy.Conway( iterations );
    float rot = 0;
    for( int x = 0; x < cwy.width; x++ )
    {
      for( int y = 0; y < cwy.height; y++ )
      {
        if( cwy.buffer[ x + cwy.width * y ] > 0.5f )
        {
          Spawn( prefab, new Vector3( 0.5f + x, 0, 0.5f + y ), Quaternion.Euler( 0, rot, 0 ) );
          rot += 90f; 
        }
      }
    }
  }

  public void CreateEmptyWorld()
  {
    if( SerializeWorld )
      Serialize();
    Destruct();
    do
    {
      int randy = Random.Range( 0, 256 );
      Random.InitState( randy );
      WorldName = "empty-" + randy;
    }
    while( WorldList.Contains( WorldName ) );
    AddToWorldList( WorldName );
    Scene activeScene = SceneManager.CreateScene( WorldName );
    // lighting settings are taken from active scene 
    SceneManager.SetActiveScene( activeScene );
    Zone zone = GameObject.Instantiate( ZonePrefab ).GetComponent<Zone>();
    zone.Initialize( WorldName );
    UpdateLightingSettings();
    CurrentTimeOfDay = 400;
    UpdateTimeOfDay();
  }

  public void GenerateWorld()
  {
    if( SerializeWorld )
      Serialize();
    Destruct();
    do
    {
      WorldName = "new-" + GenerateRandomName();
    }
    while( WorldList.Contains( WorldName ) );
    AddToWorldList( WorldName );
    Scene activeScene = SceneManager.CreateScene( WorldName );
    // lighting settings are taken from active scene 
    SceneManager.SetActiveScene( activeScene );
    Zone zone = GameObject.Instantiate( ZonePrefab ).GetComponent<Zone>();
    zone.Initialize( WorldName );
    zone.Generate();
    UpdateLightingSettings();
    CurrentTimeOfDay = 400;
    UpdateTimeOfDay();

  }

  public void AudioOneShot( AudioClip clip, Vector3 pos )
  {
    GameObject go = GameObject.Instantiate( audioOneShotPrefab, pos, Quaternion.identity );
    go.GetComponent<AudioSource>().PlayOneShot( clip );
    GameObject.Destroy( go, clip.length );
  }

  public void Pause()
  {
    IsPaused = true;
    Time.timeScale = 0;
    cameraController.LookControlEnabled = false;
  }

  public void Unpause()
  {
    IsPaused = false;
    Time.timeScale = 1;
    cameraController.LookControlEnabled = true;
  }

  public void Shake()
  {
    cameraController.Shake();
  }

  public void ShowCharacterInfo( Character cha )
  {
    if( cha == playerCharacter )
      CurrentCharacterInfo.ShowInfo( cha );
    else
      SelectedCharacterInfo.ShowInfo( cha );
  }

  public void HideCharacterInfo()
  {
    SelectedCharacterInfo.gameObject.SetActive( false );
  }


  public void AssignTeam( GameObject go, string TeamName )
  {
    Team team = Global.Instance.gameData.FindTeam( TeamName );
    if( team == null )
    {
      Debug.Log( "team does not exist: " + TeamName, go );
      return;
    }
    AssignTeam( go, team );
  }

  public void AssignTeam( GameObject go, Tag tag )
  {
    Team team = Global.Instance.gameData.FindTeam( tag );
    if( team == null )
    {
      Debug.Log( "team does not exist: " + tag.ToString(), go );
      return;
    }
    AssignTeam( go, team );
  }

  public void AssignTeam( GameObject go, Team team )
  {
    ITeam[] cmps = go.GetComponentsInChildren<ITeam>();
    foreach( var cmp in cmps )
      cmp.SetTeam( team );

    MeshRenderer[] mrs = go.GetComponentsInChildren<MeshRenderer>();
    foreach( var mr in mrs )
    {
      foreach( var mat in mr.materials )
      {
        if( mat.HasProperty( "_IndexColor" ) )
        {
          mat.SetColor( "_IndexColor", team.Color );
        }
      }
    }
  }

  public void ShowControlInfo( string key )
  {
    ControlInfo.SetActive( true );
    for( int i = 0; i < ControlInfo.transform.childCount; i++ )
      ControlInfo.transform.GetChild( i ).gameObject.SetActive( false );
    ControlInfo.transform.Find( key ).gameObject.SetActive( true );
  }

  public void HideControlInfo()
  {
    ControlInfo.SetActive( false );
  }




  public void AddZone( Zone z )
  {
    Zones.Add( z );
    ZoneLookup.Add( z.gameObject.scene.name, z );
  }

  public void RemoveZone( Zone z )
  {
    Zones.Remove( z );
    ZoneLookup.Remove( z.gameObject.scene.name );
  }

  public Zone GetZone( GameObject go )
  {
    return ZoneLookup[ go.scene.name ];
  }


  public void Speak( Character character, string text, float timeout, int priority = 0 )
  {
    // priority 0 = offhand remarks
    // priority 1 = unsolicted chat
    // priority 2 = player-engaged speech
    // priority 3 = mandatory message
    if( playerCharacter == null || character == null )
      return;
    // equal priority overrides
    if( SpeechTimer.IsActive && priority < SpeechPriority )
      return;
    float DistanceSqr = Vector3.SqrMagnitude( character.moveTransform.position - playerCharacter.moveTransform.position );
    if( DistanceSqr > SpeechRange * SpeechRange )
      return;

    SpeechCharacter = character;
    SpeechPriority = priority;
    /*string colorString = "#ffffff", 
    Color color = Color.white;
    ColorUtility.TryParseHtmlString( colorString, out color );
    SpeechText.color = color;*/

    SpeechName.gameObject.SetActive( true );
    SpeechName.text = character.CharacterName;
    SpeechText.gameObject.SetActive( true );
    SpeechText.text = text;
    SpeechText.rectTransform.localScale = Vector3.one * ( 1f - 0.5f * Mathf.Clamp( Mathf.Sqrt( DistanceSqr ) / SpeechRange, 0, 1 ) );

    SpeechTimer.Stop( false );
    SpeechTimer.Start( timeout, null, delegate()
    {
      SpeechName.gameObject.SetActive( false );
      SpeechText.gameObject.SetActive( false );
      SpeechCharacter = null;
    } );
  }

  public string GenerateRandomName()
  {
    return namegen.Generate();
  }


  [Header( "Audio" )]
  public UnityEngine.Audio.AudioMixer mixer;
  public UnityEngine.Audio.AudioMixerSnapshot snapInside;
  public UnityEngine.Audio.AudioMixerSnapshot snapOutside;
  public UnityEngine.Audio.AudioMixerSnapshot snapSleep;
  bool Sleeping = false;
  public float AudioFadeDuration = 0.1f;
  int insideRefCount = 0;

  public void AudioGoInside()
  {
    if( insideRefCount++ == 0 )
    {
      if( Sleeping )
        return;
      mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] {
        snapOutside,
        snapInside
      }, new float[] {
        0,
        1
      }, AudioFadeDuration );
    }
  }

  public void AudioGoOutside()
  {
    if( --insideRefCount == 0 )
    {
      if( Sleeping )
        return;
      mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] {
        snapOutside,
        snapInside
      }, new float[] {
        1,
        0
      }, AudioFadeDuration );
    }
  }

  public void AudioGoSleep( float fade )
  {
    Sleeping = true;
    mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] {
      snapOutside,
      snapSleep
    }, new float[] {
      0,
      1
    }, fade );
  }

  public void AudioWakeUp( float fade )
  {
    Sleeping = false;
    mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] {
      snapSleep,
      snapOutside
    }, new float[] {
      0,
      1
    }, fade );
  }
}

