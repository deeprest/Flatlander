﻿//#define ENDLESS_WORLD
using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(Billboard))]
public class BillboardEditor : Editor
{
  public override void OnInspectorGUI()
  {
    Billboard bb = target as Billboard;
    if( GUILayout.Button( "updatemesh" ) )
      bb.UpdateMesh();
    DrawDefaultInspector();
  }
}
#endif

[ExecuteInEditMode]
public class Billboard : MonoBehaviour
{
  public bool faceCamera = true;
  public bool keepUpright = false;
  public bool flip = false;
  public bool showImage = false;
  public MeshRenderer mr;
  public MeshFilter mf;
  Rect frame;
  public Sprite sprite;
  Vector2[] uvs = new Vector2[4];

  void Awake()
  {
    if( mr == null )
      mr = GetComponent<MeshRenderer>();
    if( mf == null )
      mf = GetComponent<MeshFilter>();
    if( sprite!=null && showImage )
      UpdateMesh();
    if( !faceCamera )
      enabled = false;
  }
    
	void OnEnable()
	{
		#if UNITY_EDITOR
		if( !EditorApplication.isPlaying )
			SceneView.onSceneGUIDelegate += SceneGUI;
    #endif
	}

	void OnDisable()
	{
		#if UNITY_EDITOR
		if( !EditorApplication.isPlaying )
			SceneView.onSceneGUIDelegate -= SceneGUI;
    #endif
	}

	void FaceTarget( Transform cam )
	{
    Vector3 up = cam.up;
    Vector3 forward = cam.position - transform.position;
    if( flip )
      forward = transform.position - cam.position;
    if( keepUpright )
    {
      up = Vector3.up;
      forward.y = 0;
    }
    transform.rotation = Quaternion.LookRotation( forward, up );
	}

  public void UpdateMesh()
  {
    if( sprite != null )
      frame = new Rect( sprite.rect.x, (sprite.texture.height-sprite.rect.y-sprite.rect.height), sprite.rect.width, sprite.rect.height );
    
    if( mr.sharedMaterial.mainTexture!= null )
    {
      float w = sprite.texture.width;
      float h = sprite.texture.height;

      uvs[ 0 ].x = frame.x / w;
      uvs[ 0 ].y = ( h - frame.y ) / h;
      uvs[ 1 ].x = ( frame.x + frame.width ) / w;
      uvs[ 1 ].y = ( h - frame.y ) / h;
      uvs[ 2 ].x = frame.x / w;
      uvs[ 2 ].y = ( ( h - frame.y ) - frame.height ) / h;
      uvs[ 3 ].x = ( frame.x + frame.width ) / w;
      uvs[ 3 ].y = ( ( h - frame.y ) - frame.height ) / h;

      if( Application.isPlaying )
      {
        //mr.material.mainTexture = sprite.texture;
        mf.mesh.uv = uvs;
      }
      else
      {
        // this would modify the material asset
        //mr.sharedMaterial.mainTexture = sprite.texture;
        mf.sharedMesh.uv = uvs;
      }
    }
  }

	void Update()
  {

    if( Application.isPlaying )
    {
      if( Global.Instance.cameraController.cam == null )
        return;
    
      if( faceCamera )
      {
        #if ENDLESS_WORLD
        FaceTarget( GetClosestCameraTransform() );
        #else
        FaceTarget( Global.Instance.cameraController.cam.transform );
        #endif
      }
    }
  
#if UNITY_EDITOR
    if( Application.isEditor && !Application.isPlaying )
    {
      if( showImage )
        UpdateMesh();
    }
#endif
	}

#if UNITY_EDITOR
	void SceneGUI( SceneView sv )
	{
    if( !Application.isPlaying )
    {
      if( faceCamera )
        FaceTarget( sv.camera.transform );
      if( showImage )
        UpdateMesh();
    }
	}
#endif


  public Transform GetClosestCameraTransform( )
  {
    #if ENDLESS_WORLD
    if( Global.Instance.EndlessWorld )
    {
      int camIndex = -1;
      Transform t = World.Instance.cameraController.cam;
      float closerDistance = Vector3.Distance( transform.position, t.position );
      for( int i = 0; i < 8; i++ )
      {
        Vector3 check = t.position + World.Instance.cameraController.offset[ i ];
        float distance = Vector3.Distance( transform.position, check );
        if( distance < closerDistance )
        {
          camIndex = i;
          closerDistance = distance;
        }
      }
      if( camIndex > 0 )
        t = World.Instance.cameraController.extraCameras[ camIndex ].transform;
      return t;
    }
    #endif
    return Global.Instance.cameraController.cam.transform;
  }
}
