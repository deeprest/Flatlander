﻿Shader "Custom/wiggle" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Amount ("Extrusion Amount", Range(-1,1)) = 0.5
		_Wiggle ("Wiggle", Range(0,2)) = 1
		_Speed ("Speed", Range(0,8)) = 1
		_Amounts ("Amounts", Vector) = (1,1,1,1)
		_MaxDistance ("Max Distance", Range(0,32)) = 16
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		half _MaxDistance;
		fixed4 _Amounts;
		float _Amount;
		float _Speed;
		float _Wiggle;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _Color.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
		}

		void vert (inout appdata_full v) 
		{
			if( v.vertex.z < _MaxDistance )
			{
				v.vertex.x += ((sin((v.vertex.x*_Wiggle)+_Time.y*_Speed)*0.5+1.0) * _Amounts.x * v.vertex.x );
				v.vertex.y += ((sin((v.vertex.y*_Wiggle)+_Time.y*_Speed)*0.5+1.0) * _Amounts.y * v.vertex.y );
				v.vertex.z += ((sin((v.vertex.z*_Wiggle)+_Time.y*_Speed)*0.5+1.0) * _Amounts.z * v.vertex.z );
			}
		}
		ENDCG
	}
	FallBack "Diffuse"
}
