﻿Shader "Custom/Flag" {
	Properties {
		_IndexColor ("Index 1", Color) = (1,0,0,1)
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MetallicTex ("Metallic (RGB)", 2D) = "white" {}
		_Speed ("Speed", Range(0,8)) = 1

		_Amounts ("Amounts", Vector) = (1,1,1,1)
		_Wiggle ("Wiggle", Vector) = (1,1,1,1)
		_Speed ("Speed", Vector) = (1,1,1,1)

		_MaxDistance ("Max Distance", Range(0,32)) = 16
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MetallicTex;

		struct Input {
			float2 uv_MainTex;
		};
		 
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		fixed4 _IndexColor;
		half _MaxDistance;
		fixed4 _Amounts;
		fixed4 _Wiggle;
		fixed4 _Speed;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex); //	 * _Color;
			o.Albedo = c.rgb * _Color.rgb;

			int r = (int)(c.r * 255.0);
			if( r==1 )
				o.Albedo = lerp( _IndexColor.rgb * min(1.0,c.g*2.0), float4(1,1,1,1), (c.g-0.5)*2.0 );

			o.Metallic = _Metallic * tex2D( _MetallicTex, IN.uv_MainTex );
			o.Smoothness = _Glossiness;
			o.Alpha = c.a * _Color.a;
		}

	      void vert (inout appdata_full v) 
	      {
		      if( v.vertex.z < _MaxDistance )
		      {
		      	v.vertex.x += ((sin((v.vertex.x*_Wiggle.x)+_Time.y*_Speed.x)*0.5+1.0) * _Amounts.x * v.vertex.x );
		      	v.vertex.y += ((sin((v.vertex.x*_Wiggle.y)+_Time.y*_Speed.y)*0.5+1.0) * _Amounts.y * v.vertex.y );
		      	v.vertex.z += ((sin((v.vertex.x*_Wiggle.z)+_Time.y*_Speed.z)*0.5+1.0) * _Amounts.z * v.vertex.z );
	      	}
	      }
		ENDCG
	}
	FallBack "Diffuse"
}
