#!/bin/bash

rootDir=$(pwd)
assetDir=$rootDir/Assets
outputDir=$rootDir/out
mkdir $outputDir

assets=$(echo "$(find $assetDir -type f)" | grep -E "\.png$|\.ogg$|\.json$|\.wav$")
# echo "$assets"

cp "${assets}" $outputDir


# for i in "${assets[@]}"; do
#   echo $i
#   # cp "$i" "$outputDir"
# done
