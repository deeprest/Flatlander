
# Voice Events

## Implemented
- ok (follow action)
- bye (unfollow action)
- yay (after win battle)
- attack (attack)

## Not Implemented
- greet (proximity)
- apologize (when hit character on same team)
- exclaim (when hit by same team)
- thanks (when recently dropped item picked up)
- random: laugh, nonsequitur
- fear (when see many enemies, or big monster?)



# Ideas

## Speech event priorities
high priority comments = the character *will* comment when these happen, such as spotting an enemy.
low priority comments = idle chatter or situational commentary
Have a minimum interval for both. Incessant chatter is worse than silence.

## comment-worthy scenarios
- waiting around too long (idle patience)
- being in a hurry (running patience)
- getting close and personal (proximity patience)
- npc being in the way (proximity check)
- player being in the way (raycast when aiming)
- notice dead bodies (overlap check on deadbody layer)
- notice valuable swag
- entering a tagged area, saying "wow look at that"
- player is damaged and needs to heal

## Conversations
* n = number of character identities
* direct interaction count = n+(n(n-3))/2
* unique 3-point count = n+n(n-4)
* So for 6 characters, direct interaction count is 15
* This means for 6 characters there are 15 pairs of conversation designations.

## Labeling Example
* 0 Stan
* 1 Roger
* 2 Haley
* 3 Klaus
* 4 Francine
* 5 Steve

character indices, conversation index, clip index

Example: Roger and Francine, conversation 0, clip 4 = 14-00-04
