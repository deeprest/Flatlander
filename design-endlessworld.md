#  Endless World Illusion
Notes on how to create the illusion of an endless world using a rectangular playable area.
The amount of work that goes into making the illusion flawless may require you to design your entire game with these issues in mind!
Need 3 active cameras in addition to main camera if your going to render the corners.
I use 10 cameras in total: main, one for the sky, and 8 for world bounds (but only need 3 extra active, max)
Character positions are a simple float-modulo ("repeat" in Unity) but navigation is another matter.
AI Pathfinding issues when crossing a border:
NavMesh colliders on border to allow a path to complete.
After crossing a seam, set the path again.
Projectiles might have trail renderer; if so, it will attempt to trail across the world.
Enemy awareness; use surrogates. Note: 8 surrogates per object, but 3 active maximum.
Audio sources. Need surrogate AudioSources for the single Listener.
Lights need to exist on surrogates, and with the exact offset of the original.
Physics: FixedJoint needs to be re-established when crossing.
