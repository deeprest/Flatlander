# Flatcraft
A Warcraft2-inspired battle game

# Analysis of Warcraft2
hovering top-down camera
mouse used to select objects and locations
units are created in buildings
units have very little autonomy
## Resources
wood, gold, oil, food
ubiquitous values: can be used anywhere, anytime
harvest from trees, goldmine
available food determines how many units can be created
store in town hall, etc
various buildings increase resource production: farms, town hall upgrades, lumber mill, refinery
## Building mechanic
using build menu, select a building
establish build site; deduct resources
progress meter
workers presence required to improve build progress or repair
## Scenario objectives
destroy all enemies
build certain buildings
escort specific characters to 'circle of power'

# Comparision
direct commands VS set policies. Let commands determine policy.
unit weapons/classes are intrinsic
Buildings are not single units.
single farm is all that is needed
resources: only food and wood

player establishes build site, builders carry nearest wood to site, stand next to site and play build animation.
peasant commands: chop wood, build, follow
peasant: chop tree, collect wood in town hall
assign character to lookout tower (horn when enemy spotted)
turret -> archer tower
objective: kill all enemies, kill enemy commander


# first level
peasants
footmen
town hall
objective: build farms, barracks
chop wood
mine gold


# unused
names
aging
stamina
gender, mate, birth
beds
talk/gossip
sleep
