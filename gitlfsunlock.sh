#!/bin/bash
git lfs locks | grep -oh [0-9][0-9][0-9][0-9] | while read -r line; do git lfs unlock --id="$line"; done
