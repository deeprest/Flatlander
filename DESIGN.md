For this project my goal is to enjoy game development again.  I do not know what this game is about, but I know what it is not.  I am focused on finding interesting mechanics.  I also want to avoid common game design choices we all take for granted.

# Design Overview
* AI. Characters have different sets of interests and priorities. They observe events and have an affinity for others. They converse with others to share information.
* 2D Gameplay. All interactive objects lie on a flat plane. Camera slides between third-person view and birds-eye view.
* Persistence. Changes are permanent. Environment is shaped over time.  Perma-everything.
---
* Story. No authored story to consume.
* Death. Because there is no story, every character is potentially expendable. Play as another character when you die.  
* Building. Construction in addition to destruction.
* Limited UI. Prefer a diagetic UI. A notable exception is the context menu, which is necessary for interaction with objects, and communication with characters.

# MDA
Expression, Sensation, Challenge, Discovery
Character Mechanics:
* Character Switching. Control one of many active characters.
* Age. Affects speed, health, voice pitch, ability to mate.
* Speed
* Health
* Mate. gestation timer leads to birth. Must have two adults to mate.
* Gestation. Gestating females can increase number of offspring by eating extra food.
* Food. eating affects stamina; affects offspring count. Eat produce from plants or meat from creatures.  
* Stamina...
* Death. Continue play as another character in your clan. If none exist, the game is over.
## Group Mechanics:
* Team... intrinsically shared interests.
* Talk. Characters share information with each other while conversing. They relay events they have witnessed or been told. Significant events: damaging someone, killing someone, etc..
* Rank. Must outrank to command characters. Rank is increased by accomplishing goals that the tribe cares about.
* AI commands. follow/stay, assign vocation.
* AI affinity. thresholds for: follow, heal/share items, talk/share info, attack no forgive, attack forgive timer
* AI vocation. builder, hunter/soldier, harvester/farmer, Leader?
* Trees / Wood. construction materials are not in use yet. Resource management may not be a focus of this project.

* Permanence: trails
* Permanence: destructible objects
* Carry objects, acquire items. axe, sword, bow.
* Weapons. use to hunt for food and protect your clan from enemies.
* Construction. build structures...
- Sleep. beds can heal, and will advance the time of day. Beds are owned by individuals. Beds can be built with wood.
***no implementation yet***
* Tribe status board: shows tribe goals, and high-ranking members.
* Horticulture. plants. you can harvest, plant seeds
* Storage. gather objects and store in containers.
* Day/night? Maybe certain creatures wander or hunt at night.
* Conversation. to propagate affinity


Affinity levels:
Attack. will attack on sight
Follow. will follow if asked
Share. will share / trade
Talk. can talk to

# Conversations
Source character, about character, event type (death,damage), context data.
Converse mechanic. compare all, or one at a time. average affinity or absolute max overrides other.
Broadcast intense affinity: character event.
Character witness events. Share when conversing.
Effective reputation.
Character says, "Suzy says you killed her friend. I will not follow you."



***Working list in DEV.md***

# Roadmap
* Vocation: Soldiers, Engineers, Farmers.
* Engineer "Builder"
  + Need a camp layout system.
  + Ability to repair damaged walls.
* Farmer
  - an idle farmer checks a quota for planted seeds, plants if below. If quota is reached or seeds are not available, check for ripe plants. Gather fruit of plants that are ripe, and take to storage.
  - Plant seeds. Action: Convert lemon to seed. Action: plant seed in ground.
  - Harvest crop. Gather food and take to storage. Need storage containers.
  - Livestock.
* Soldier "Fighter"
  + guard stations
  + patrol behavior
* Food: characters must eat to live (motivation to hunt/farm).
  * Creatures eat lemons
  * Creatures kill and eat other creatures and people to live.

# Feature Creep
* Marching Cubes. Doors
- combine swag into a stack at gather destination. reduce dynamic objects.
* Characters sleep at night. lie down. maybe particle zzz.
* separate home and bed. return home when idle. They go bed at night until morning.
- replace swag buttons with dispensers. Ignore dispenser item when considering items to gather.
* Unarmed Punch

## Systems
* System: use scene manager when transitioning between scenes. Make World destroy on load, create a new persistent class for UI and level transition.
* System: Unity's newer input system. Maybe have key rebinding.
* Characters can build their own home. how do they decide where to place them? Maybe designate home areas
* Allegiances. Organic formation of teams.
* System: Characters share information when they "converse". Maybe use the average affinity, or higher ranking char overwrites lesser char affinity.
- Destinations have a safety value attributed like affinity (if destination in range). When fleeing, pick a destination with an acceptable safety value.
- can claim blocks by shooting objects with a "paint gun". Objects would use an indexed color shader.
* speech. in world, on screen. VO or jibberish.
* Faces to show affinity.
* Ability to carry living characters. Wounded, children, animals.

* portal to transition to generated world. bring friends.
- can use "lookout" point of view on top of building/tower
- smooth camera transition from death to new character.

* store map of placed decals to avoid writing to the dirt layer every frame
* sidestep: consider multiple obstacles
- construction: destroy colliding objects? may need a way to know how important objects are: bush VS wall etc.
- System: Fire spreads to any combustible objects.

# Attempted and Rejected Ideas
- Seamless wrap-around world. No edge of the world, no invisible walls. (SEE: design-endlessworld.md)
- collecting lots of stuff. (Now every item has a distinct purpose)
- Junk Tornado: Hearts or random junk swirls around you forming a protective barrier.
- hearts shield player from an incoming hit when health is full and they are spinning in the inventory

# Aesthetics
* expressive face: eyes and mouth
* buildings rise out of the ground when built (magic carpet)
+ day/night cycle. directional light angle; sky color; villagers sleep at night.
+ parent characters to the block they are within.
+ visually remove second vertical layer of static objects when camera is low.
* weather cycle. rain. splash in puddles. wind. trees move. leaves fly.


# Influences
* Grand Theft Auto - top-down view
* Warcraft2 - managing a group of people. gathering resources.
* Zelda (Link to the Past) - swordplay and adventuring, bushes
* Doom - Each enemy has a unique type of attack or movement pattern that players can learn to master battle against numerous enemies.
- Super MarioKart (Battle Mode)- health is represented in-world and spinning around the avatar. This was the inspiration for the junk tornado :) Accumulate hearts or random junk that swirls around you, forming a protective barrier. You can also weaponize items from your inventory by shooting them.

## Shigeru Miyamoto
* avoid trends: force yourself to do something new
* The player needs a sense of accomplishment; to feel they have done something; satisfaction.

## Quotes
["You should always set out to make a game you enjoy playing."](https://www.theguardian.com/technology/2015/may/18/how-we-made-grand-theft-auto) - Mike Dailly, Grand Theft Auto programmer
"I learned that you can fail at what you don't love, so you might as well do what you love" - Jim Carrey

# Reference Games
art style of ["Equilinox"](https://store.steampowered.com/app/853550/Equilinox/)
["Factory Town"](https://easmussen.itch.io/factory-town)
Many elements of ["StoneHearth"](http://www.stonehearth.net/store/)
Voxel characters of ["Staxel"](https://youtu.be/WxYWweWkVEw?t=300)
Sprite characters of ["DwarfCorp"](https://www.youtube.com/watch?v=IDjPQf3dhFA)
Simulation, Building of ["Rimworld"](https://youtu.be/vkeAejMc4Ug?t=329)
Character Simulation of ["Embark"](https://embark.itch.io/embark)
["ECO"](https://www.strangeloopgames.com/eco/)
lack of character animation solution in ["Everything"](https://store.steampowered.com/app/582270/Everything/?snr=1_5_9__300)


A game is an interaction between the player and the designer. The designer declares the world, the player's place within, and waits for a response. The player considers the world as presented, and begins to ask probing questions. What are my options? What happens when I do this? Where can I go? What are the limits? What's next?

Give me your money
It's fueled with lies
drunk as a skunk, the town drunk is a skunk. skunk junk;
Go explode yourself
Wind them up and watch em go
Jack Basswards
Boon Loof Moot Bung Plop
Bubble and Squeak
Each character in your cohort plays and instrument; is a member of your traveling band: lute
Who's this?;That's nobody... a knownothing.;Prepare yourself well, Nobody! You might learn something.;
Meseeks-like character just happy to help. He is threatened at gunpoint to do assist in crime but is enthusiastic about it. The man with the gun is later arrested and our good-natured helper is charged as an accomplice. Being blindly helpful still makes you responsible for what you're contributing to (no good deed..)
Wireless Bill
Willy Nilly
We're saving you from yourselves
