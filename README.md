# Flatlander

![screenshot](./screenshot.png)


### MacOS
If you have an xbox360 controller, I recommend using [360Controller for macOS](https://github.com/360Controller/360Controller/releases)


# dependencies
[Unity's runtime navmesh components](https://github.com/Unity-Technologies/NavMeshComponents)
[Edy's blender-to-unity3d-importer](https://github.com/EdyJ/blender-to-unity3d-importer)
[UnityLitJson](https://github.com/Mervill/UnityLitJson)
## not required but I like:
[FindReferencesInPrefabs](https://wiki.unity3d.com/index.php/FindReferencesInPrefabs)
