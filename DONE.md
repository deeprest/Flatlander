
---
# 2018.12.17
+ carry should preserve initial relative rotation
+ new home prefab: beds, switchboard, lanterns, axe
+ package persistent data along with build. unzip into persistent dir
+ ui scene list needs to include zipped scenes as well as those in persistent dir
+ show stamina on HUD
+ block building inside audio
+ fix bushes block camera line of sight raycast
+ initial lookinput for cameracontroller (jumps)
+ zone generation params: deerbeds
+ replace swag with carryobject
+ loading screen show zone name
+ timers bug. newtimers.
+ sleep. character state. lie down. camera. time multiplier/time of day. sleep lower priority than attack. Allow player to cancel sleep at any time.
+ construction library hold to speed through list
+ show when hidden on HUD
+ object stacks: logs, lemons.
---
# 2018.12.06
+ serialize list of dead characters (nullref fix for gossip about dead characters). persistent character info so characters can talk about the deceased (or any character not loaded into memory at the moment)
+ serialize known character events for each character.
+ bushes destruct (shorter,gone)
+ deer behaviour: herd, run away
+ claim destinations, beds
+ ambient sounds and inside/outside triggers
+ update character health meter
+ face corners
+ delayed reaction to birth. (disabled reaction because it's weird)
+ serialize pregnancy
---
# 2018.09.03
+ interact option: switch character
+ speech text, onscreen not in world
+ can generate new name for characters
+ Animal Cannon. Weaponized creature spawn. Not sure if they should be on your team or neutral.
* Flammable objects. This is an exciting idea. Should fire be able to spread? If so, and the distance is too far, then it would be very easy to burn down the forest... which I think might be okay if I can keep the fire effects performant.
+ fix beds. Other destinations need a CommunalDestination bool set
+ faces instead of emoticons
* Animal Cannon
+ added axe. tree must be chopped down with axe.
+ Removed Fire mechanic.
+ Offspring: romance particles, assign team, civilian default, follow mother, high affinity for parents and siblings, blood explosion at birth?:) Need progress indicator. Age check; only adults can mate.
+ Romance/Breed instead of spawn point. Requires gender, gestation time, age check.
---
# 2018.08.11
New stuff:
+ Vocations. Soldier, Farmer, Undertaker. These can be assigned using the G menu. The behavior for each vocation needs much work, so not much has changed. I might rename "soldier" to "fighter" to keep consistent with the "er" theme. I want to add a "Builder" role that takes wood resources and builds the village, but I need to figure out how he would decide where to build.
+ Affinity. Low affinity causes characters to attack (less than -20). A protype emoji is shown above the character's head when affinity is changed. When a character is highlighted, a list of (non-zero) affinity is shown in the upper right hand corner of the screen.
+ Character Events. Death and damage affect characters' affinity for the instigator. I'll add more events in the future.
+ Experimental Block build mode (use G menu to access Build->Block mode). You can only add and remove blocks for now.
Fixes:
+ fixed: do not show indicator if player cannot interact with object
+ fixed: monsters and any character with no identity should have no name
+ fixed: deadbody scale
+ fixed: cannot change settings when diagetic is active.
+ fixed: deadbody lerp color (need color and factor in shader)
+ fixed: Red base spawned inside rocks
+ added: roofs for marching squares (Block structures)
+ replaced unarmed attack with melee "bite" for bats
+ attack enemy that is closer than current attack target:
  + Added an "AlwaysConsider" option to Interest so even if attacking, a character will consider a closer target for attack.
  + Added a second Physics.OverlapSphere call for lower priority layers, so the ActiveInterest queue does not fill up with insignificant objects.
---
+ simple join team context action for characters
+ pickup deadbody, identify, bury. gravestone, has dead character stats when viewed.
+ flag on top of base, can see over trees
+ added a defensive turret
+ can have characters and turret "join team"
+ destroy door with frame
+ when build compound, set team
---
+ added Construction Library
+ Build Mode buttons need to be displayed and more intuitive. Maybe use Q and W for build object cycle, and Tab for build modes + off.
+ Build Mode: the current and relevant controls need to be shown on screen.
---
+ improved wandering a bit
+ fixed: characters have random sprite each launch
+ really no raycast through walls this time
+ selected character name position
+ UI settings/prefs. look sensitivity.
---
+ more advanced home selection process; changing of homes
+ fixed: T spawns a new character, but does not switch to it. Second press switches.
+ deadbody serializes sprite, no need for identity name. This allows monsters to have no Identity.
+ 3d interact indicator
+ modify mode climbs hierarchy to find serialized object
+ longer interact raycast distance, and will not raycast through walls.
+ characters wander between known destinations and home
+ move bounce cooldown tweak
+ camera lerp improvement (for diagetic UI)
+ diagetic UIs!
+ building fixes: rotation. roof hiding.
+ build mode no longer pauses the game and does not need navmesh rebuild!
+ nav obstacle for all appropriate objects to avoid navmesh rebuild.
+ character sidestep when not following
+ followers move out of way when you walk through them
+ ideal follow distance range.
+ support blender animations. Test animation object is a Big Dog.
+ (HACK) fixed: swag falling through ground. [I put a collider below the ground to catch and reposition]
+ serialize freecam position/rot for each level.
+ show player-character name
+ fixed: if campfire is home, bed is not assigned when available
+ show name of characters if highlighted
+ can hide in bushes (character.behindcover). Characters are only hidden if not moving.
+ chars pick up weapon if none equipped. Even if they are following.
---
+ red base doors
+ when swag limit is hit, the held weapon disappears
+ enemies do not react when hit with arrows
+ UI show current level name, show list of available levels
+ move items to separate resource folder so it does not show up in construction menu
+ loading screen
+ inventory is not varied by default.
  : inventory is empty by default. Need weapon dispenser.
+ finish design for diagetic inventory. Limits, placement, cycle. Present the equipped weapon somehow.
+ battle is a meatgrinder because of friendly attack.
  + temporary: sword only has hacking motion to avoid slashing others- a single raycast forward.
+ Melee combat. Single right hand right side slash. (Would like more sword swing types)
+ When an enemy is very close, let the player attack directly without the need to aim.
+ Shield. Sits on left side, moves to front when in use. Cannot shoot while shielding; Can hit own shield with sword if not careful: consider a noise/attention mechanic for this.
+ Ranged Combat. Archers will back up if too close to target. If the enemy is not visible, they will wait a moment and then look for them.
+ Aging mechanic: scale sprite; voice pitch changes.
+ holding objects
+ Stamina: Affects movement speed
+ Rank. kills=rank?
+ Following should only happen under certain circumstances; such as: when away from home, or if the character is a soldier/guard.
+ etch the dirt layer into the permanent layer. This could use the alpha channel byte for the pixel type...
+ menu to generate new worlds.
+ need a menu to reset persistent data
+ player can call out for everyone nearby to follow them.
+ interact action opens a small menu with contextual options.
+ character: follow, unfollow, talk
+ swag: carry, drop ("drop" is not a menu options. you must drop a carried object to interact with anything else)
+ character identities: visual, interests, patience/timing/speeds, voice
+ character identity + observed event = situational speech. The only situational speech for now is post-battle comments.

## workflow / maintenance
+ break out Avatar to independent player controller script. (added PlayerControlled state to character)
+ move fighter functionality to character

+ shield
+ inventory representation, selection
+ archer combat
+ interact indicators
+ on death, play as different character
+ manage the total of characters in the scene
+ do something about all the weapons laying around from dead folks. (Added a Collector)
+ when enemy is hit, change active target to instigator
+ when item is picked up, show above the character for a second first before adding to inventory.
+ bug: when archers have sword equipped, they still use the "ranged attack" range... and swing the sword at nothing.
+ bug: when followers engage in battle, they are too quick to return to follow state. They look spastic and confused.
  : shield had an 'Attack' tag, and characters would enter 'Search' state when any shield was used.
+ bug: indicator is left on ground when player dies

+ decide when to dilute dirt images. Every minute or so?
  : dilutes when the level is constructed
+ world seams: surrogates
+ shut doors / door interaction
+ permanent ground image
+ 'dirt' ground image. snow, dust, dirt, blood
+ Test nav mesh generation performance; Also when modifying the mesh while building.
  * Generation for a World is too slow to do once per frame. Generating per-block is not an option. Therefore "destructable" objects should not alter the nav mesh.
+ Use nav mesh for all character movement.

+ Health.
  + Affects player's sight
  + leave a blood trail when severe damage is taken.

+ serialization
  + dynamic objects
  + object references
  + character state
  + Replace cell serialization with per scene serialized objects
  + dead bodies

+ building
  + ability to remove objects
  + select and rotate objects
  + multiple objects in cell (categories)
  + composite objects
  + need a way to remove dynamic objects [deleting in editor is enough to prevent serialization]
  + ghost mode to place enemies without detection (or timescale=0) [disabled characters while in build mode]
  + decal raycast will  not hit through lamp

## optimizations
  + disable far away blocks
  + turn off unused world seam cameras
