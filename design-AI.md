
## Ways to identify objects
**Tags**
A Tags script is used to replace Unity's GameObject string tags. It contains a list of tag values that are defined in an enumeration. I add a Tags script to all objects that need to be identified categorically in some way. Tags are part of the definition of an object (intrinsic) and should not be added by characters.

**Instance IDs**
Unity's component instance IDs. These are serialized so that characters can remember locations and specific characters.

## Interests
Interests associate a way of identifying an object with the name of a state.
Interests define what objects a character cares about, and what happens when detected. Objects are identified by a tag or instance ID, and the given state name is used to consider a state transition. Example: "Wooden", "Gather". The character would consider entering the "Gather" state using the context object identified by the "Wooden" Tag.

**Processing Interests**
*Sensors, Processing, Update.*
1. Sensors add to active interests.
2. Processing considers a state change for each active interest.
3. During update, current state function is run.
When an Interest is processed the associated State's Consider*() call is made, which either pushes the state on the stack or does nothing. Interests can be processed on an independent interval from when sensors detect objects. When Interests are processed, a state change can be made with contextual information from the sensor data.

## Sensors
A Sensor is anything that makes a Character aware of an object.
Sensors identify an object of interest and add it to a list for processing. This can be automatic using triggers, spatial queries, or sent from other objects.
* OverlapSphere called on interval.
* CharacterEvents. Broadcasts an event to everyone within a radius.
* Triggers (use sparingly on static objects)   Events: OnCollision, OnTriggerEnter/OverlapSphere
  Incoming events check Interest conditions (tags) and if something is worth consideration, the Interest is added to the ActiveInterest list.

## States
A State is essentially a wrapper around a group of functions. These functions determine if the state should become active, what it does each frame, and how to handle transitions.
State transitions occur when active Interests are processed, and a state has been considered. Coordinating state changes allows for the enforcement of state priorities, which can reduce the likelihood of infinite recursion and circular logic.
All states should implement a timeout as a way to prevent being 'stuck' in a mode. The only exceptions are the initial state, and the PlayerControlled state which should not automatically expire.

**Verbs**
idle
wander
investigate
pickup
gather
attack
flee
follow
die
speak
-patrol (variant of wander)
-eat
-sleep
-harvest

**Stack**
State changes are implemented with a stack. A State can push other States onto the stack freely, which effectively pauses the current state's execution and begins another. When a state updates it decides when to pop itself off the stack, ending it's own execution and resuming the execution of the previous state.
Example: if a character is following the player and needs to momentarily detour to attack a monster, an attack state is pushed on the stack, and then popped when done. The character then resumes the previous state; following the player.

**Functions**
State function names have predetermined prefixes, and end with the state name. Example: The Idle state has the functions ConsiderIdle(), PushIdle() and so on.
* **Consider*()** When entry into a new state is considered, it is the responsibility of this function to determine whether to push the new state.
* **Push*()** When the state is entered, this is called.
* **Update*()** This is regular state update call. It decides when to leave the state.
* **Pop*()** Called when the state is popped off the stack. Any state cleanup can be done here.
* **Suspend*()**
* **Resume*()**


## Identity
Identity is a character's appearance, and speech data. (potentially Interests)

## Team
Teams have collective Interests, such as attacking monsters.

## Vocation
Vocations contain interests and state priorities. Example: a Farmer is interested in gathering food, and has a lowered attack priority.

## Affinity
Affinity for a specific character is modified based on events that occur within range. Example: Sue happens to hate Joe because he was a jerk to her, not because Joe is universally hated.  Although, it would be a shortcut to use tags in a reputation system.

**Character Events**
- death
- damage
- burial
- food added to storage (storage temp is campfire)

# Thoughts

**Character daily routine example**
Guard example:
at home in bed,
wake up. Morning event? or when rest is no longer needed?
walks to work
stands at post, or patrols
---
attacks trespassers
takes damage
flees when health is too low
gets healed
---
walks home
eats
goes to bed

**Priority of Needs/Motivations**
* Character Groups: Personal, Friends, Possessions, Strangers, Enemies
1. Safety
  Attack
  Flee
  Carry/Drag others to safety
  Command others (call livestock etc)
2. Hunger
  Eat
  Hunt
  Forage
  Sleep (try again tomorrow)
3. Exhaustion
  Go Home
  or Find place to sleep
  then Sleep
4. Go About Your Day
  Farm duty
  Guard duty
  Wander


**Properties**
Hunger. Affects melee damage, affects ranged weapon accuracy. Recharged with food.
Stamina. Affects movement speed. Regenerates quickly.
Energy. Affects stamina. Lowered by movement. Recharged with rest.

**Speech IDs / categories**
hello greet
yes affirm
approve
goodbye leave
yay celebrate
sorry apologize
wow exclaim
haha laugh
scream
thankyou appreciate
nonsequitur random
insult
death
