#!/bin/bash

# unity build

# push to itch.io
# ditto is a macos anomaly
ditto -c -k --sequesterRsrc --keepParent Flatlander.app Flatlander.app.zip
butler push Flatlander.app.zip deeprest/flatlander:macos
